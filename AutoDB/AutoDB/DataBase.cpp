#include"DataBase.h"

DataBase::DataBase(){ //default constructor
	size_catalog = 0;
	catalog = nullptr;
	size_autopark = 0;
	autopark = nullptr;
	size_clients = 0;
	clients_list = nullptr;
}

DataBase::DataBase(Car *aCatalog, int aSize_catalog){ //overloaded constructor
	size_catalog = aSize_catalog;
	if (size_catalog == 0)
		catalog = nullptr;
	else
	{
		catalog = new Car[size_catalog];
		for (int i = 0; i < size_catalog; i++)
			catalog[i] = aCatalog [i];
	}
	size_autopark = 0;
	autopark = nullptr;
	size_clients = 0;
	clients_list = nullptr;
}

DataBase::DataBase(const DataBase &aDB){ //copy constructor
	size_catalog = aDB.size_catalog;
	if (size_catalog == 0 || aDB.catalog == nullptr)
	{
		catalog = nullptr;
	}
	else
	{
		catalog = new Car[size_catalog];
		for (int i = 0; i < size_catalog; i++)
		{
			catalog[i] = aDB.catalog[i];
		}
	}
	size_autopark = aDB.size_autopark;
	if (size_autopark == 0)
	{
		autopark = nullptr;
	}
	else
	{
		autopark = new test_auto[size_autopark];
		for (int i = 0; i < size_autopark; i++)
		{
			autopark[i] = aDB.autopark[i];
		}
	}
	size_clients = aDB.size_clients;
	if (size_clients == 0)
	{
		clients_list = nullptr;
	}
	else
	{
		clients_list = new client[size_clients];
		for (int i = 0; i < size_clients; i++)
		{
			clients_list[i] = aDB.clients_list[i];
		}
	}
}

DataBase::~DataBase(){ //destructor
	delete[] catalog;
	delete[] autopark;
	delete[] clients_list;
}

int DataBase::get_size_catalog() const{ //Get size of the catalog
	return size_catalog;
}

Car *DataBase::get_car_catalog()const { //Get a catalog of all cars available for order
	return  catalog;
}

int DataBase::get_size_autopark() const{ //Get size of a catalog of all cars that are in the salon 
	return size_autopark;
}

test_auto *DataBase::get_autopark() const{ //Get a catalog of all cars that are in the salon
	return autopark;
}

int DataBase::get_size_clients() const{ //Get size of a catalog of all clients salon
	return size_clients;
}

client *DataBase::get_clients_list(){	//Get a catalog of all clients salon
	return clients_list;
}

int DataBase::get_size_inquiries() const
{
	int inq_sz = 0;
	for (int i = 0; i < size_clients; i++)
		inq_sz += clients_list[i].get_list_sz();
	return inq_sz;
}

DataBase DataBase::operator= (const DataBase &aDB)
{
	size_catalog = aDB.size_catalog;
	if (size_catalog == 0)
	{
		catalog = nullptr;
	}
	else
	{
		Car *catalog = new Car[size_catalog];
		for (int i = 0; i < size_catalog; i++)
		{
			catalog[i] = aDB.catalog[i];
		}
	}
	size_autopark = aDB.size_autopark;
	if (size_autopark == 0)
	{
		autopark = nullptr;
	}
	else
	{
		test_auto *autopark = new test_auto[size_autopark];
		for (int i = 0; i < size_autopark; i++)
		{
			autopark[i] = aDB.autopark[i];
		}
	}
	size_clients = aDB.size_clients;
	if (size_clients == 0)
	{
		clients_list = nullptr;
	}
	else
	{
		client *clients_list = new client[size_clients];
		for (int i = 0; i < size_clients; i++)
		{
			clients_list[i] = aDB.clients_list[i];
		}
	}
	return *this;
}

void DataBase::print_to_file(ofstream & F) const
{
	if (!F)
		throw "DataBase: file was not opened, can't write";
	
	int buf = size_catalog;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing size_catalog
	for (int i = 0; i < size_catalog; i++)
	{
		try
		{
			catalog[i].print_to_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}

	buf = size_autopark;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing size_autopark
	for (int i = 0; i < size_autopark; i++)
	{
		try
		{
			autopark[i].print_to_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}

	buf = size_clients;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing size_clients
	for (int i = 0; i < size_clients; i++)
	{
		try
		{
			clients_list[i].print_to_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
}

void DataBase::read_from_file(ifstream & F)
{
	if (!F)
		throw "DataBase: file was not opened, can't read";

	F.read(reinterpret_cast<char*>(&size_catalog), sizeof(int));	//reading size_catalog
	delete[]catalog;
	catalog = new Car[size_catalog];
	for (int i = 0; i < size_catalog; i++)
	{
		try
		{
			catalog[i].read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}

	F.read(reinterpret_cast<char*>(&size_autopark), sizeof(int));	//reading size_autopark
	delete[]autopark;
	autopark = new test_auto[size_autopark];
	for (int i = 0; i < size_autopark; i++)
	{
		try
		{
			autopark[i].read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
	
	F.read(reinterpret_cast<char*>(&size_clients), sizeof(int));	//reading size_clients
	delete[]clients_list;
	clients_list = new client[size_clients];
	for (int i = 0; i < size_clients; i++)
	{
		try
		{
			clients_list[i].read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
}

int DataBase::get_car_index(int car_ID)
{
	int car_index = 0; //to save index of a car with pointed ID
	for (int i = 0; i < size_catalog; i++)
		if (catalog[i].get_ID() == car_ID)
		{
			car_index = i;
			break;
		}
	return car_index;
}

int DataBase::get_client_index(int clientID)	//takes client's ID, returns index
{
	int index = -1;
	for (int i = 0; i < size_clients; i++)
		if (clients_list[i].ID[1] == clientID)
		{
			index = i;
			break;
		}
	return index;
}

int DataBase::get_test_auto_index(int ID)	//retuns index of a test-auto with pointed ID
{
	int index = -1;
	for (int i = 0; i < size_autopark; i++)
		if (autopark[i].get_carID() == ID)
		{
			index = i;
			break;
		}
	return index;
}

int DataBase::get_clients_inquiry_index(int clientID, int carID)
{
	int client_index = get_client_index(clientID);
	if (client_index == -1)
		return false;
	int index = -1;
	for (int i = 0; i < clients_list[client_index].get_list_sz(); i++)
		if (clients_list[client_index].inquirys_list[i].get_car_ID() == carID)
		{
			index = i;
			break;
		}
	return index;
}



bool DataBase::check_client_ID(int clientID)
{
	//checking, if there is such a client in the list, using a flag-variable:
	bool flag = false;
	for (int i = 0; i < size_clients; i++)
		if (clients_list[i].ID[1] == clientID)
		{
			flag = true;
			break;
		}
	return flag;
}

bool DataBase::check_clients_inquiry(int clientID, int carID)
{
	int client_index = get_client_index(clientID);
	if (client_index == -1)
		return false;
	bool flag = false;
	for (int i = 0; i < clients_list[client_index].get_list_sz(); i++)
		if (clients_list[client_index].inquirys_list[i].get_car_ID() == carID)
		{
			flag = true;
			break;
		}
	return flag;
}


/*
a method to remove client from the list
takes int (client ID)
returns nothing
*/
void DataBase::remove_client(int clientID) //removing client by ID; contains exception
{
	if (size_clients == 0)
		throw "No clients in the base";
	//checking, if there is such a client in the list, using a flag-method and generating an exception if there is no such a client:
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//if client with such ID exists, deleting him from the list:
	size_clients--;
	if (size_clients == 0)
	{
		delete[] clients_list;
		clients_list = nullptr;
	}
	else
	{
		client *tmp = new client[size_clients];
		for (int i = 0, j = 0; i < size_clients + 1; i++)
			if (clients_list[i].ID[1] != clientID)
			{
				tmp[j] = clients_list[i];
				j++;
			}
		delete[] clients_list;
		clients_list = tmp;
	}
}