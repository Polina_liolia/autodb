#include "salonDB.h"

salonDB::salonDB() : DataBase() 		//default constructor
{
	clients_counter = 0;
}

salonDB::salonDB								//overloaded constructor
				(Car *aCatalog,			
				int aSize_catalog, 
				int salon_ID, 
				string salon_name, 
				string salon_city, 
				string director_surname, 
				string director_name, 
				string salon_address, //������ nullptr ���
				string salon_phone) //������ nullptr ��� - ������ � ��������������, �� � ����������
	: DataBase (aCatalog, aSize_catalog)
{
	salon_data.set_ID(salon_ID);
	salon_data.set_name(salon_name);
	salon_data.set_city(salon_city);
	salon_data.set_address(salon_address);
	salon_data.set_phone(salon_phone);
	clients_counter = 0;
	salon_personnel.set_director(salon_ID, director_surname, director_name);
}
salonDB::salonDB(const salonDB &aSalonDB) : DataBase(aSalonDB)	//copy constructor
{
	salon_data = aSalonDB.salon_data;
	clients_counter = aSalonDB.clients_counter;
	salon_personnel = aSalonDB.salon_personnel;
}

salonDB::~salonDB() 
{}


/*
a method to set or change a director of the salon
takes an instance of class director
returns nothing
*/
void salonDB::set_director(director aDir)
{
	salon_personnel.set_director(aDir);
}


/*
a method to set or change a director of the salon
takes all data about director: surname, name, patronymic, day of birth, month of birth, year of birth, phone number
returns nothing
*/
void salonDB::set_director(string aSurname, string aName, short aDay, short aMonth, short aYear, string aPhone)
{
	salon_personnel.set_director(salon_data.get_ID(), aSurname, aName, aDay, aMonth, aYear, aPhone);
}


/*
a method to hire a new manager
takes an instance of class manager
returns nothing
*/
void salonDB::add_manager(manager aMgr)
{
	salon_personnel.add_manager(aMgr);
}


/*
a method to hire a new manager
takes all data about manager: surname, name, patronymic, day of birth, month of birth, year of birth, phone number
returns nothing
*/
void salonDB::add_manager(string aSurname, string aName, short aDay, short aMonth, short aYear, string aPhone)
{
	salon_personnel.add_manager(salon_data.get_ID(), aSurname, aName, aDay, aMonth, aYear, aPhone);
}

/*
a method to fire manager with pointed ID
takes an ID of a manager to fire
returns nothing
*/
void salonDB::fire_manager(int aEmployeeID)
{
	salon_personnel.fire_manager(aEmployeeID);
}

salon &salonDB::get_salon_data()
{
	return salon_data;
}

/*
a method to get an instance of class stuff with all data about personnel of the salon
takes nothing
returns an instance of class stuff
*/
stuff & salonDB::get_personnel()
{
	return salon_personnel;
}

director salonDB::get_salon_director() const
{
	return salon_personnel.get_director();
}

int salonDB::get_managers_amount() const
{
	return salon_personnel.get_size_mgrs();
}

manager * salonDB::get_managers() const
{
	return salon_personnel.get_managers();
}

manager salonDB::get_manager_salon(int index) const
{
	return salon_personnel.get_manager(index);
}

bool salonDB::check_manager(int ID)
{
	//checking, if there is such a manager in salon, using a flag-variable:
	bool flag = false;
	for (int i = 0; i < salon_personnel.get_size_mgrs(); i++)
		if (salon_personnel.get_manager(i).get_employee_ID() == ID)
		{
			flag = true;
			break;
		}
	return flag;
}
	
/*
a method to add a new client to the list
takes personal data about client: surname, name, patronymic, day of birth, month of birth, year of birth, phone number
returns nothing
*/
void salonDB::add_client(string aSurname, string aName, string aPhone, int managerID)//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	size_clients++;
	client New_client(salon_data.get_ID(), ++clients_counter, aName, aSurname, aPhone);  	//instance of the class client, that has to be added to the clients list
	New_client.set_client_status(added);
	if (size_clients == 1)
	{
		clients_list = new client[size_clients];
		clients_list[0] = New_client;
	}
	else
	{
		client *temp = new client[size_clients];
		for (int i = 0; i < size_clients - 1; i++)
			temp[i] = clients_list[i];
		temp[size_clients - 1] = New_client;
		delete[] clients_list;
		clients_list = temp;
	}
}

void salonDB::remove_inquiry(int clientID, int carID, int managerID)	//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	//checking, if there is such a client in the list
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//checking, does such inquiry exists:
	if (!check_clients_inquiry(clientID, carID))
		throw "No such an inquiry";
	//if client and inquiry exist, checking the status of inquiry:
	int client_index = get_client_index(clientID);
	int inq_index = get_clients_inquiry_index(clientID, carID);
	if (clients_list[client_index].inquirys_list[inq_index].get_status() == added ||
		clients_list[client_index].inquirys_list[inq_index].get_status() == changed ||
		clients_list[client_index].inquirys_list[inq_index].get_status() == inq_rejected)
		clients_list[client_index].delete_inquiry((inq_index + 1));
	else
		throw "This inquiry has been already synchronized with data base of manufacturer";
}

/*
a method to remove client from the list
takes int (client ID)
returns nothing
*/
void salonDB::remove_client(int clientID, int managerID) //removing client by ID; contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	if (size_clients == 0)
		throw "No clients in the base";
	//checking, if there is such a client in the list, using a flag-method and generating an exception if there is no such a client:
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//if client with such ID exists, deleting him from the list:
	size_clients--;
	if (size_clients == 0)
	{
		delete[] clients_list;
		clients_list = nullptr;
	}
	else
	{
		client *tmp = new client[size_clients];
		for (int i = 0, j = 0; i < size_clients + 1; i++)
			if (clients_list[i].ID[1] != clientID)
			{
				tmp[j] = clients_list[i];
				j++;
			}
		delete[] clients_list;
		clients_list = tmp;
	}
}

/*
a method to edit client's surname
takes client ID and new surname
returns nothing
*/
void salonDB::edit_clients_surname(int clientID, string aSurname, int managerID)//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	if (size_clients == 0)
		throw "No clients in the base";
	//checking, if there is such a client in the list, using a flag-method and generating an exception if there is no such a client:
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//if client with such ID exists:
	int client_index = get_client_index(clientID);
	clients_list[client_index].set_client_surname(aSurname);
	clients_list[client_index].set_client_status(changed);
}

/*
a method to edit client's name
takes client ID and new name
returns nothing
*/
void salonDB::edit_clients_name(int clientID, string aName, int managerID)//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	if (size_clients == 0)
		throw "No clients in the base";
	//checking, if there is such a client in the list, using a flag-method and generating an exception if there is no such a client:
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//if client with such ID exists:
	int client_index = get_client_index(clientID);
	clients_list[client_index].set_client_name(aName);
	clients_list[client_index].set_client_status(changed);
}

/*
a method to edit client's phone
takes client ID and updated phone number
returns nothing
*/
void salonDB::edit_clients_phone(int clientID, string aPhone, int managerID)//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	if (size_clients == 0)
		throw "No clients in the base";
	//checking, if there is such a client in the list, using a flag-method and generating an exception if there is no such a client:
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//if client with such ID exists:
	int client_index = get_client_index(clientID);
	clients_list[client_index].set_client_phone(aPhone);
	clients_list[client_index].set_client_status(changed);
}

/*
a method to edit client's status
takes client ID and client's status (added, changed or synchronyzed)
returns nothing
*/
void salonDB::edit_clients_status(int clientID, client_status aStatus, int managerID)//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	if (size_clients == 0)
		throw "No clients in the base";
	//checking, if there is such a client in the list, using a flag-method and generating an exception if there is no such a client:
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//if client with such ID exists:
	int client_index = get_client_index(clientID);
	clients_list[client_index].set_client_status(aStatus);
}

client &salonDB::get_client(int ID)//contains exception
{
	if (size_clients == 0)
		throw "No clients in the base";
	//checking, if there is such a client in the list, using a flag-method and generating an exception if there is no such a client:
	if (!check_client_ID(ID))
		throw "No client with such ID";
	return clients_list[get_client_index(ID)];
}

int salonDB::get_clients_counter() const
{
	return clients_counter;
}

bool salonDB::check_carID(int car_ID)//checking, if there is such a car in the catalog
{
	bool flag = false;
	int car_index = 0; //to save index of a car with pointed ID
	for (int i = 0; i < size_catalog; i++)
		if (catalog[i].get_ID() == car_ID)
		{
			flag = true;
			break;
		}
	return flag;
}

/*
a method to add a new test auto to the autopark
takes ID of the car from the catalog, that has to be added
returns nothing
*/
void salonDB::add_test_auto(int car_ID)//contains exception
{
	if (!size_catalog)
		throw "No cars in a catalog";
	//checking, if there is such a car in the catalog, using a flag-method
	//generating an exception if there is no such a car:
	if (!check_carID(car_ID))
		throw "No car with such ID in a catalog";
	//checking if there is already such a car in salon's autopark:
	for (int i = 0; i < size_autopark; i++)
		if (autopark[i].get_carID() == car_ID)
			throw "Error: a car is already in salon's autopark";
	//if car with such ID exists:
	int car_index = get_car_index(car_ID);
	size_autopark++;
	test_auto New_car(salon_data.get_ID(), catalog[car_index]);		//instance of the class test_auto, that has to be added to the autopark
	if (size_autopark == 1)
	{
		autopark = new test_auto[size_autopark];
		autopark[0] = New_car;
	}
	else
	{
		test_auto *temp = new test_auto[size_autopark];
		for (int i = 0; i < size_autopark - 1; i++)
			temp[i] = autopark[i];
		temp[size_autopark - 1] = New_car;
		delete[] autopark;
		autopark = temp;
	}
}

/*
a method to remove test auto from the autopark
takes ID of the car from the catalog, that has to be removed
returns nothing
*/
void salonDB::remove_test_auto(int test_autoID) //contains exception
{
	if (!size_catalog)
		throw "No cars in a catalog";
	//checking, if there is such a car in the catalog, using a flag-method
	//generating an exception if there is no such a car:
	if (!check_carID(test_autoID))
		throw "No car with such ID in a catalog";
	//if tes-auto with such ID exists, deleting it from the autopark:
	size_autopark--;
	if (size_autopark == 0)
	{
		delete[] autopark;
		autopark = nullptr;
	}
	else
	{
		test_auto *tmp = new test_auto[size_autopark];
		for (int i = 0, j = 0; i < size_autopark + 1; i++)
			if (autopark[i].ID[1] != test_autoID)
			{
				tmp[j] = autopark[i];
				j++;
			}
		delete[] autopark;
		autopark = tmp;
	}
}

bool salonDB::check_test_autoID(int ID)	//checking, if there is such a test-auto in the autopark
{
	bool flag = false;
	for (int i = 0; i < size_autopark; i++)
		if (autopark[i].ID[1] == ID)
		{
			flag = true;
			break;
		}
	return flag;		
}

/*
a method to change test auto with another one
takes ID of the car from the catalog, that has to be changed, and ID of the car from the catalog to replace it
returns nothing
*/
void salonDB::change_test_auto(int test_autoID_old, int test_autoID_new) //contains exception
{
	//checking, if there is such a car in the catalog
	if (!size_catalog)
		throw "No cars in a catalog";
	//checking, if there is such a car in the catalog, using a flag-method
	//generating an exception if there is no such a car:
	if (!check_carID(test_autoID_new))
		throw "No car with such ID in a catalog";
	//if car with such ID exists, checking, if there is such a test-auto in the autopark:
	if (!size_autopark)
		throw "No test-auto in autopark";
	//generating an exception if there is no such a test-auto:
	if (!check_test_autoID(test_autoID_old))
		throw "No test-auto with such ID";
	//if tes-auto with such ID exists, changing it:
	int car_index = get_car_index(test_autoID_new);
	int test_auto_index = get_test_auto_index(test_autoID_old);
	test_auto New_test_auto(salon_data.get_ID(), catalog[car_index]); //a new instance of test_auto class with a pointed new car from the catalog
	autopark[test_auto_index] = New_test_auto;	//changing pointed test-auto
}

void salonDB::add_inquiry(int clientID, int carID, int managerID, short aDay, short aMonth, short aYear)	//contains exception
{
	// checking, if there are cars in the catalog
	if (!size_catalog)
		throw "No cars in a catalog";
	//checking, if there is such a car in the catalog
	if (!check_carID(carID))
		throw "No car with such ID in a catalog";
	//if car with such ID exists,
	//checking, if there are the same inquiries or test-cars in this salon (to avoid repeating):
	if (check_test_autoID(carID))
		throw "There is such a car in your autopark";
	bool flag = false;
	for (int i = 0; i < size_clients; i++)
		for (int j = 0; j < clients_list[i].get_list_sz(); j++)
			if (clients_list[i].inquirys_list[j].get_car_ID() == carID && clients_list[i].inquirys_list[j].get_status() != done)
			{
				flag = true;
				break;
			}
	if (flag)
		throw "Such inquiry already exist";
	//checking, if there is such a client in the list
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//if client with such ID exists:
	int client_index = get_client_index(clientID);
	clients_list[client_index].add_inquiry(salon_data.get_ID(), managerID, clientID, carID, aDay, aMonth, aYear);
}

void salonDB::change_inquiry(int clientID, int carID_old, int carID_new, int managerID) 	//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	//checking, if there is such a client in the list
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//checking, does such inquiry exist:
	if (!check_clients_inquiry(clientID, carID_old))
		throw "No such an inquiry";
	//checking a new car ID
	if (!check_carID(carID_new))
		throw "No such a car in a catalog";
	//checking, if there are the same inquiries or test-cars in this salon (to avoid repeating):
	if (check_test_autoID(carID_new))
		throw "There is such a car in your autopark";
	bool flag = false;
	for (int i = 0; i < size_clients; i++)
		for (int j = 0; j < clients_list[i].get_list_sz(); j++)
			if (clients_list[i].inquirys_list[j].get_car_ID() == carID_new && clients_list[i].inquirys_list[j].get_status() != done)
			{
				flag = true;
				break;
			}
	if (flag)
		throw "Such inquiry already exist";
	//checking the status of inquiry:
	int client_index = get_client_index(clientID);
	int inq_index = get_clients_inquiry_index(clientID, carID_old);
	if (clients_list[client_index].inquirys_list[inq_index].get_status() == added ||//�������� () �.�. �������
		clients_list[client_index].inquirys_list[inq_index].get_status() == changed ||//�������� () �.�. �������
		clients_list[client_index].inquirys_list[inq_index].get_status() == inq_rejected)//�������� () �.�. �������
		clients_list[client_index].change_inquiry(clients_list[client_index].inquirys_list[inq_index], carID_new);
	else
		throw "This inquiry has been already synchronized with data base of manufacturer";
}

void salonDB::add_inquiry_option(int clientID, int carID, string aOption, int managerID)	//contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	//checking, if there is such a client in the list
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//checking, does such inquiry exist:
	if (!check_clients_inquiry(clientID, carID))
		throw "No such an inquiry";
	// checking the status of inquiry:
	int client_index = get_client_index(clientID);
	int inq_index = get_clients_inquiry_index(clientID, carID);
	if (clients_list[client_index].inquirys_list[inq_index].get_status() == added ||//�������� () �.�. ������� ���
		clients_list[client_index].inquirys_list[inq_index].get_status() == changed ||//�������� () �.�. ����������
		clients_list[client_index].inquirys_list[inq_index].get_status() == inq_rejected)//�������� () �.�. ������� ���
		clients_list[client_index].add_option(clients_list[client_index].inquirys_list[inq_index], aOption);
	else
		throw "This inquiry has been already synchronized with data base of manufacturer";
}

void salonDB::remove_inquiry_option(int clientID, int carID, string aOption, int managerID)	//������� ����� �� ������� �������; contains exception
{
	if (!check_manager(managerID))
		throw "Access error: no manager with pointed ID in salon";
	//checking, if there is such a client in the list
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//checking, does such inquiry exist:
	if (!check_clients_inquiry(clientID, carID))
		throw "No such an inquiry";
	// checking the status of inquiry:
	int client_index = get_client_index(clientID);
	int inq_index = get_clients_inquiry_index(clientID, carID);
	if (clients_list[client_index].inquirys_list[inq_index].get_status() == added ||//�������� () �.�. ������� ���
		clients_list[client_index].inquirys_list[inq_index].get_status() == changed ||//�������� () �.�. ������� ���
		clients_list[client_index].inquirys_list[inq_index].get_status() == inq_rejected)//�������� () �.�. ������� ���
		clients_list[client_index].delete_option(clients_list[client_index].inquirys_list[inq_index], aOption);
	else
		throw "This inquiry has been already synchronized with data base of manufacturer";
}

salonDB salonDB::operator=(salonDB aSDB)
{
	salon_data = aSDB.salon_data;
	size_catalog = aSDB.size_catalog;
	if (size_catalog == 0)
		catalog = nullptr;
	else
	{
		catalog = new Car[size_catalog];
		for (int i = 0; i < size_catalog; i++)
			catalog[i] = aSDB.catalog[i];
	}
	size_autopark = aSDB.size_autopark;
	if (size_autopark == 0)
		autopark = nullptr;
	else
	{
		autopark = new test_auto[size_catalog];
		for (int i = 0; i < size_autopark; i++)
			autopark[i] = aSDB.autopark[i];
	}
	size_clients = aSDB.size_clients;
	if (size_clients == 0)
		clients_list = nullptr;
	else
	{
		clients_list = new client[size_clients];
		for (int i = 0; i < size_clients; i++)
			clients_list[i] = aSDB.clients_list[i];
	}
	clients_counter = aSDB.clients_counter;	
	salon_personnel = aSDB.salon_personnel;
	return *this;
}

void salonDB::print_to_file(ofstream & F) const
{
	if (!F)
		throw "salonDB: file was not opened, can't write";
	try
	{
		DataBase::print_to_file(F);
		salon_data.print_to_file(F);
		salon_personnel.print_to_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	int buf = clients_counter;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing clients counter
}

void salonDB::read_from_file(ifstream & F)
{
	if (!F)
		throw "salonDB: file was not opened, can't read";
	try
	{
		DataBase::read_from_file(F);
		salon_data.read_from_file(F);
		salon_personnel.read_from_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	F.read(reinterpret_cast<char*>(&clients_counter), sizeof(int));	//reading clients counter
}
