#include"Car.h"
#pragma once
/*����� ��� �������� ������� �� ����������� ��������� ��������� ������*/

class test_auto 
{
private:
	int ID [2];		//ID ������ � ID ���� (�� ��������)
	string model;

public:
	test_auto();
	test_auto(int salonID, const Car &aCar);
	test_auto(const test_auto &aTest_auto);	
	~test_auto(); 

public:
	void set_salonID(int salonID);
	void set_test_auto(const Car &aCar); 
	int get_salonID()const;
	int get_carID()const;
	string get_car_model()const;
	test_auto operator=(const test_auto &aTest_auto);

	friend class salonDB;	
	friend class director;
	friend ostream& operator<<(ostream &os, test_auto &aCar);
	friend ostream& operator << (ostream& os, salonDB aSalonDB);

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
};