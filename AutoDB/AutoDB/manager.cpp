
#include "manager.h"
#include "salonDB.h"
manager::manager() : employee::employee() // default constructor
{
	type = mgr;
}

manager::manager(int aSalonID, int aemployeeID, string aSurname, string aName, short aDay, short aMonth, short aYear, string aPhone , position aType, int aPassword)//overload constructor
{
	ID[0] = aSalonID;
	ID[1] = aemployeeID;
	type = aType;
	name = aName;
	surname = aSurname;
	phone = aPhone;
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
	password = aPassword;

}
manager::manager(const manager &amanager)//copy construktor
{
	ID[0] = amanager.ID[0];
	ID[1] = amanager.ID[1];
	surname = amanager.surname;
	name = amanager.name;
	type = amanager.type;
	phone = amanager.phone;
	birth = amanager.birth;
	password = amanager.password;
}

void manager::add_client(string aSurname, string aName, salonDB aSalon, string aPhone)// add new client; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.add_client(aSurname, aName, aPhone, ID[1]);
	else
		throw "Protected access";
}
void manager::edit_clients_surname(int clientID, string aSurname, salonDB aSalon)//edit surname; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.edit_clients_surname(clientID, aSurname, ID[1]);
	else
		throw "Protected access";
}
void manager::edit_clients_name(int clientID, string aName, salonDB aSalon)//edit name; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.edit_clients_name(clientID, aName, ID[1]);
	else
		throw "Protected access";
}
void manager::edit_clients_phone(int clientID, string aPhone, salonDB aSalon) // edit phone; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.edit_clients_phone(clientID, aPhone, ID[1]);
	else
		throw "Protected access";
}
void manager::edit_clients_status(int clientID, client_status aSt, salonDB aSalon) //contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.edit_clients_status(clientID, aSt, ID[1]);
	else
		throw "Protected access";
}
void manager::remove_client(int clientID, salonDB aSalon)//remove client; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.remove_client(clientID, ID[1]);
	else
		throw "Protected access";
}
void manager::change_clients_status(int clientID, client_status aStatus, salonDB aSalon)// change status; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.edit_clients_status(clientID, aStatus, ID[1]);
	else
		throw "Protected access";
}
void manager::add_inquiry(int clientID, int carID, salonDB aSalon, short aDay, short aMonth, short aYear)// add inquiry; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.add_inquiry(clientID, carID, ID[1], aDay, aMonth, aYear);
	else
		throw "Protected access";
}
void manager::remove_inquiry(int clientID, int carID, salonDB aSalon)//remove inquiry; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.remove_inquiry(clientID, carID, ID[1]);
	else
		throw "Protected access";
}
void manager::change_inquiry(int clientID, int carID_old, int carID_new, salonDB aSalon)//change inquiry; contains exception	
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.change_inquiry(clientID, carID_old, carID_new, ID[1]);
	else
		throw "Protected access";
}
void manager::add_inquiry_option(int clientID, int carID, string aOption, salonDB aSalon)// add option inquiry; contains exception	
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.add_inquiry_option(clientID, carID, aOption, ID[1]);
	else
		throw "Protected access";
}
void manager::remove_inquiry_option(int clientID, int carID, string aOption, salonDB aSalon)//remove inquiry option; contains exception
{
	if (aSalon.salon_data.get_ID() == ID[0])
		aSalon.remove_inquiry_option(clientID, carID, aOption, ID[1]);
	else
		throw "Protected access";
}
