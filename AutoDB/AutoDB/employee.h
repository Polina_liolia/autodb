#pragma once
#include  <string> 
using std::string;
#include "date.h"
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;

class stuff;

enum position { mgr, dir }; //enum type created for workers

class employee
{
protected:
	int ID[2]; //ID ������ � ID ���������a
	int password;
	string name;
	string surname;
	string phone;
	date	birth;
	position type;

public:
	
	employee();//default constructor

	employee(string aSurname, string aName, int IDsalon, int IDemployee, short aDay, short aMonth, short aYear, string aPhone, position aType);//overloaded constructor
	employee(const employee &a);//copy constructor
	
	void set_name(string aName);
	void set_surname(string aSurname);
	void set_phone(string aPhone);
	void set_ID_employee(int IDemployee);
	void set_ID_salon(int IDsalon);
	void set_birth_date(short aDay, short aMonth, short aYear);
	void set_position(position atype);
	string get_name()const;
	string get_surname()const;
	string get_phone()const;
	int get_password()const;
	int get_salon_ID();
	int get_employee_ID();
	position get_position()const;
	void set_password(int aPassword);
	friend ostream& operator<<(ostream& os, const employee& dt);
	friend class stuff;

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);

};