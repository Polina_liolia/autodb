#pragma once
#include "date.h"
#include <iostream>
#include <string>
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;
using std::string;


/*������ ������� (��� ������������� � �� �������������):
added - ����� ������, ��� �� ���������������
changed - ������ ������, �� ������ ���� �������
synchronyzed - ������ ������, ������ �� ���������� � ������� ��������� �������������
accepted - ������ (���������) ��������������
is_moving - � �������� �������� (���� �� ���� � �����)
rejected - �������� (���������� ���������)
*/
enum inquiry_status 
{ 
	inq_added, inq_changed, inq_synchronyzed,	//�������, ��������������� ���������� ������ ��� ��������/��������� ��� ������ �������������
	inq_accepted, inq_is_moving, inq_rejected, done	//�������, ��������������� ��������������: ������, � �������� ��������, ��������, ��������
};


/*�������� ���������� � ������� (������) �������*/
class inquiry
{
private:
	enum  index_ID //��� �������� ������� 
	{
		iSalonID = 0, 
		iManagerID = 1, 
		iClientID = 2, 
		iCarID = 3
	};
	int ID [4];					//ID �������ID ���������- ID �������� ID ����
	date date_added;
	inquiry_status status;		//������	 �������  (��.����)
	int options_sz;				//���������� ��������� ��� �����
	string *additional_options;	//������ ���.�����, ��������� ��������

public:
	inquiry();								//default constructor
	inquiry(int aSalonID, 
			int aManagerID, 
			int aClientID, 
			int aCarID, 
			int aDay, 
			int aMonth, 
			int aYear); //overloaded constructor
	inquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, date aDate); //overloaded constructor
	inquiry(const inquiry &aInquiry);		//copy constructor
	~inquiry();								//destructor

	// ���������� ID ������
	int get_salon_ID()const
	{
		return ID[iSalonID];
	}

	// ���������� ID ���������
	int get_manager_ID()const
	{
		return ID[iManagerID];
	}

	// ���������� ID �������
	int get_client_ID()const
	{
		return ID[iClientID];
	}

	// ���������� ID ����������
	int get_car_ID()const
	{
		return ID[iCarID];
	}

	// ���������� ����� date - ���� ���������� �������
	date get_date()const
	{
		return date_added;
	}

	// ���������� ������ ������
	inquiry_status get_status()const
	{
		return status;
	}

	int get_options_sz()const		
	{
		return options_sz;
	}

	//���������� ��������� �� ������ ���.����� ������
	string *get_additional_options()const
	{
		return additional_options;
	}

	
	/*set ��������� ���������� ����� ��� �������� ��� ������������ ��������*/
	void set_salon_ID(int aSalonID);
	void set_client_ID(int aClientID);
	void set_manager_ID(int aManagerID);
	void set_car_ID(int aCarID);
	void set_data(int aDay, int aMonth, int aYear);
	void set_data(date & aDate);
	void set_status(inquiry_status aStatus);	//�������� ������
	void add_option(const string &aOption);			//�������� �����
	inquiry& operator= (const inquiry& aInquiry);		// ���������� ��������� ������������
	inquiry operator+ (const string &aOption) const;			//���������� add_option, �� ����� ���������� ���������
	inquiry& operator+= (const string &aOption);		//���������� add_option, �� ����� ���������� ���������
	void remove_option(const string &aOption);			//������� ����� �� �������� (������) (���� ����� ����� ��� - exception)
	void remove_option(int option_number);		//������� ����� �� ����������� ������ (���� ����� ������� �� ������� ������� - exception)
	inquiry operator- (const string &aOption) const;			//���������� remove_option, �� ����� ���������� ���������
	inquiry& operator-= (const string &aOption);		//���������� remove_option, �� ����� ���������� ���������
	inquiry operator- (int option_number) const;		//���������� remove_option, �� ����� ���������� ���������
	inquiry& operator-= (int option_number);		//���������� remove_option, �� ����� ���������� ���������
	bool operator==(const inquiry &aInquiry)const;	//����� ��������� ���� �������� �� ID � ������� (��� �������� ��� �������������)
	friend class client; 
	friend class manager; 
	friend ostream& operator << (ostream& os, inquiry &aInquiry);
	friend class DBMS;
	friend class director;

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
};

