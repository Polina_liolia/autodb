#pragma once
#include <string> 
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;
using std::string;
using std::ostream;
/*
stores the basic (minimal) information about the salon, used to record producer salons (class manufacturerDB)
*/
class salon
{
protected:
	int ID; //id salon
	string name; //salon name
	string city; //a city in which is located salon
	string address; //salon address
	string phone; //salon phone

public:
	salon();					//default constructor
	salon(int aID, string aName, string aCity, string aAddress="", string aPhone="");	//overloaded constructor
	salon(const salon &aSalon); //copy constructor
	~salon();					//destructor

public:
	void set_ID(int aID); //sets salons id
	void set_name(string aName); //sets salons name
	void set_city(string aCity); //sets salons city
	void set_address(string aAddress); //sets salons address
	void set_phone(string aPhone); //sets salons phone
	
	int get_ID()const; //gets salons id
	string get_name()const; //gets salons name
	string get_city()const; //gets salons city
	string get_address()const; //gets salons address
	string get_phone()const; //gets salons phone

	salon operator= (const salon &aSalon);

	friend ostream & operator << (ostream& os, const salon &aSalon);
	friend class manager;	//it allows the manager to add / edit customers' requests
	friend class manufacturerDB; //manufacturer DB class is working with this class instances
	friend class DBMS; //�.�. ����� DBMS �������� � ������������ ����� ������
	friend class director;

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
};