#include <string> using std::string;
#include "DBMS.h"

#include <iostream>
using std::cout;
using std::endl;


//globaly overloaded output of string
ostream& operator << (ostream& os, const string &astring)
{
	if (astring.get_string() != nullptr)
		os << astring.str << "\n";
	return os;
}

//global outstream overload for Car
ostream& operator<<(ostream &o, Car &aCar)
{
	o << "ID: " << aCar.ID << "\n";
	o << "model: " << aCar.model << "\n";
	return o; // �������� ������ ���
}

// ���������� ��������� ������
// ��������� ������ �� ��������� ������ date � ostream
// ���������� ��������� ������ ostream
ostream& operator << (ostream& os, date &aDate)
{
	os << aDate.day << "." << aDate.month << "." << aDate.year << "\n";
	return os;
}

//global outstream overload for employee
ostream& operator<<(ostream& os, const employee& dt)
{
	os << "ID: " << dt.ID[0] << "-" << dt.ID[1] << "\n";
	os << "Name: " << dt.name ;
	os << "Surname: " << dt.surname ;
	os << "Phone: " << dt.phone ;
	os << "position: " ;
	if (dt.type == mgr)
		os << "manager" << "\n";
	else
		os << "director" << "\n";
	return os;
}

//������� ������� � ����� ��������� ������ Inquiry
//��������� ��������� ������� ostream � inquiry
//���������� �������� ������ ostream
ostream & operator<<(ostream & os, inquiry &aInquiry)
{
	os << "ID Inquiry ";
	for (int i = 0; i < 4; i++)
	{
		os << aInquiry.ID[i] << " ";
	}
	os << "\n";
	os << "Date" << aInquiry.date_added << "\n";
	switch (aInquiry.status)
	{
		case 0:
		os << "added" << "\n";
		break;
		case 1:
		os << "changed" << "\n";
		break;
		case 2:
		os << "synchronyzed" << "\n";
		break;
		case 3:
		os << "accepted" << "\n";
		break;
		case 4:
		os << "is_moving" << "\n";
		break;
		case 5:
		os << "rejected" << "\n";
		break;
	}
	if (aInquiry.options_sz != 0)
	{
		for (int i = 0; i < aInquiry.options_sz; i++)
		{
			os << aInquiry.additional_options[i];
		}
		os << "\n";
	}
	else
	{
		os << "No additional options" << "\n";
	}
	return os;
}


//friend overload of cout for client
ostream& operator<<(ostream & os, client &aclient)
{
	os << "ID client: " << aclient.ID[0] << "-" << aclient.ID[1] << "\n";
	os << "status of inquiry: ";
	switch (aclient.status)
	{
		case 0: os << "added" << "\n";	break;
		case 1: os << "changed" << "\n"; break;
		case 2: os << "synchronyzed" << "\n"; break;
	}
	os << aclient.name;
	os << aclient.surname;
	os << aclient.phone;
	os << "Number of inquries: " << aclient.list_sz << "\n";
	for (int i = 0; i < aclient.list_sz; i++)
		os << aclient.inquirys_list[i] << "\n";
	return os;
}

//������������� ����������� ������ � ����� manufacturerDB
ostream & operator<<(ostream & os, manufacturerDB &aMDB)
{
	os << "Number of size catalog" << aMDB.size_catalog << "\n";
	for (int i = 0; i < aMDB.size_catalog; i++)
	{
		os << aMDB.catalog[i];
	}

	os << "Number of size autopark" << aMDB.size_autopark << "\n";
	for (int i = 0; i < aMDB.size_autopark; i++)
	{
		os << aMDB.autopark[i];
	}

	os << "Number of clients" << aMDB.size_clients << "\n";
	for (int i = 0; i < aMDB.size_clients; i++)
	{
		os << aMDB.clients_list[i];
	}

	os << "Number of salons" << aMDB.size_salons << "\n";
	for (int i = 0; i < aMDB.size_salons; i++)
	{
		os << aMDB.all_salons[i];
	}
	return os;
}

//The overloaded operator to display the instance property class salon
ostream & operator << (ostream& os, const salon &aSalon)
{ 
	os << "ID - " << aSalon.ID << "\n";
	os << "Name - " << aSalon.name << "\n";
	os << "City - " << aSalon.city << "\n";
	os << "Address - " << aSalon.address << "\n";
	os << "Phone - " << aSalon.phone << "\n";
	return os;
}

//global overload of outstream for test_auto
ostream& operator<<(ostream &os, test_auto &aCar)// �������� ������� ���
{
	os << "ID Test Car ";
	for (int i = 0; i < 2; i++)
	{
		os << aCar.ID[i] << " ";
	}
	os << aCar.model;
	return os;
}

//global overload of outstream for stuff
ostream& operator<< (ostream& os, stuff &aStuff)
{
	os << "Personnel of salon:\n";
	os << "Director:\n" << aStuff.direk;
	os << "Managers:\n";
	for (int i = 0; i < aStuff.size_mgrs; i++)
		os << i + 1 << ". " << aStuff.mgrs[i];
	return os;
}

//global overload of outstream for salonDB
ostream& operator << (ostream& os, salonDB aSalonDB)
{
	os << "Salon ID: " << aSalonDB.salon_data.get_ID() << "\n";
	os << "Salon name: " << aSalonDB.salon_data.get_name();
	os << "Salon city: " << aSalonDB.salon_data.get_city();
	os << "Salon address: " << aSalonDB.salon_data.get_address();
	os << "Salon phone: " << aSalonDB.salon_data.get_phone() << "\n";
	os << "Autopark: \n";
	for (int i = 0; i < aSalonDB.size_autopark; i++)
		os << aSalonDB.autopark[i].ID[1] << aSalonDB.autopark[i] << "\n";
	os << "Clients: \n";
	for (int i = 0; i < aSalonDB.size_clients; i++)
		os << aSalonDB.clients_list[i].ID[1] << aSalonDB.clients_list[i] << "\n";
	return os; //�������� ������ ���
}

//global overload of outstream for DBMS
ostream& operator<<(ostream& os, DBMS& db)
{
	os << "Manufacturer:\n" << db.factory;
	os << "Salons:\n";
	for (int i = 0; i < db.size_salons; i++)
		os << db.all_salonsDB[i];
	return os;
}

void main()
{
	/*
	��������� ������:
	manufacturerDB factory;
	int salons_counter;	//to save number of salons, added to salons array (to avoid salons' IDs repeating)
	int car_catalog_counter; //to avoid repeating of cars' IDs
	int size_salons;
	salonDB *all_salonsDB;

	public:
	DBMS();							//default constructor
	DBMS(const DBMS &aDBMS);		//copy constructor
	~DBMS();						//destructor

	public:
	void set_factory(Car *aCatalog, int aSize_catalog);	//������� �� ������������� (��������� ������ manufacturerDB)
	//����� ������ ������ manufacturerDB:
	void add_car_to_catalog(string aModel);				//�������� ���������� � �������
	void remove_car_from_catalog(int aID);				//������� �� �������� ���� � ��������� ID
	void change_catalog_item(int aID, string aModel_new); //�������� ������������ ���������� � ��������

	//����� add_salon ������ ��� �� ����� �������� ����� ����� � ������� � �� ������������� (manufacturerDB factory) � ������� ������ ������ manufacturerDB
	void add_salon(Car *aCatalog, int aSize_catalog, int salon_ID, string salon_name, string salon_city, string director_surname, string director_name, string director_patronymic, string salon_address = nullptr, string salon_phone = nullptr);

	//����� remove_salon ������ ��� �� ����� ������� ����� �� �������� � �� ������������� (manufacturerDB factory) � ������� ������ ������ manufacturerDB
	void remove_salon(int salonID);
	//����� ������ ������ salon:
	void change_salon_name(int salonID, string aName);
	void change_salon_city(int salonID, string aCity);
	void change_salon_address(int salonID, string aAddress);
	void change_salon_phone(int salonID, string aPhone);

	void synchronize_cars(int salonID);	//���������������� ������� ������������ ����������� (������ ���������� �� �� ������������� � �� ������) (�� ������� ���������)
	void synchronize_test_autos(int salonID);	//���������������� ������� ����-���� � ������ (������ ���������� �� �� ������ � �� �������������) (�� ������� ���������)
	void synchronize_clients_and_inquiries(int salonID);	//���������������� ������� �������� � �� ������� (������ ���������� �� �� ������ � �� �������������) (�� ������� ���������)
	void synchronize_all(int salonID);	//���������������� ��� ����� (��� ������ ������) - ������ ��������� ���������� ��� ������ (�� ������� ���������)
	friend class manufacturerDB;
	};*/

	/*DBMS Mersedes;
	Mersedes.add_car_to_catalog("A-Class");
	Mersedes.add_car_to_catalog("B-Class");
	Mersedes.add_car_to_catalog("C-Class");
	Mersedes.remove_car_from_catalog(4);
	Mersedes.add_car_to_catalog("CLA");
	Mersedes.add_car_to_catalog("CLS");
	Mersedes.add_car_to_catalog("Maybach");
	Mersedes.add_car_to_catalog("GL-Class");
	Mersedes.add_car_to_catalog("Citan Kombi");
	Mersedes.change_catalog_item(2, "B-Class Electric Drive");
<<<<<<< HEAD
	cout << Mersedes;*/
	
	   //KAtYA tests
	//create an employee with overloaded constructor
	employee s("Ivanovich","ivan", 0, 0, 3, 4, 1985, "123456", mgr);
	s.get_employee_ID();
	s.get_password();
	s.get_salon_ID();
	cout << s;//overloaded cout for employee
	cout << "------------------------\n";

	s.set_birth_date(2, 4, 1995);
	s.set_ID_employee(6);
	s.set_ID_salon(2);
	s.set_name("Vasya");
	s.set_phone("12345678");
	s.set_surname("Pupkin");
	s.set_position(dir);
	cout << s;//output after settings of data
	cout << "------------------------\n";

	client x;//creating client using set-methods
	x.set_client_ID(11);
	x.set_client_name("Dima");
	x.set_client_surname("Sidorov");
	x.set_client_phone("066147852");
	x.set_client_status(added);
	x.set_ID(12, 3);
	x.set_salon_ID(2);
	x.add_inquiry(1, 2, 3, 4, 5, 6, 7, inq_added);
	x.add_inquiry(2, 3, 3, 4, 5, 6, 7, inq_added);
	
	inquiry ss(6, 5, 8, 9, 3, 3, 1988);//create inquiry using overloaded constructor (int aSalonID, int aManagerID,int aClientID,int aCarID,int aDay,int aMonth,int aYear)
	x.add_inquiry(ss);//add inquiry by passing inquiry
	cout << x;
	cout << "------------------------\n";
	x.delete_inquiry(ss);
	cout << "after deleating of last inquiry: \n";
	cout << x;
	cout << "------------------------\n";



	//create client using overloaded constructor
	client ww(5, 5, "Boris", "Shum", "0662585689");
	cout << ww;
	cout << "------------------------\n";
	
	DBMS test_k;
	//test_k.change_salon_address(565,"veronova");//here string= doesn't work
	//test_k.change_salon_city(0, "kyiv");the same
	//test_k.change_salon_name(12, "bnh");the same
	//test_k.change_salon_phone(11, "4566789");the same
	//test_k.synchronize_cars(123); my mistake I am going to fix it


}