#pragma once
#include "manufacturerDB.h"
#include "salonDB.h"
/*Data base management system - ����� ��� ����������������� ���������� �� ������������� � ������� */
class DBMS
{
private:
	manufacturerDB factory;
	int salons_counter;	//to save number of salons, added to salons array (to avoid salons' IDs repeating)
	int car_catalog_counter; //to avoid repeating of cars' IDs
	int size_salons; 
	salonDB *all_salonsDB;

public:
	DBMS();							//default constructor
	DBMS(const DBMS &aDBMS);		//copy constructor
	~DBMS();						//destructor

public:
	void set_factory(Car *aCatalog, int aSize_catalog);	//������� �� ������������� (��������� ������ manufacturerDB)
	manufacturerDB& get_factory();//������ ������ �� �������� ������ manufacturerDB //�������� ���
	salonDB &get_salon(int ID);//returns reffery on salonDB instance with pointed index
	int& get_car_catalog_counter(); //����������� �������� ����� � �������� ��� ID ����� ����� ������, ����� ����� ���� ������ //�������� ���
	int& get_salons_counter(); // ����������� �������� ������� ��� ID ����� ������, ����� ����� ���� ������
	//����� ������ ������ manufacturerDB:
	void add_car_to_catalog(string aModel);				//�������� ���������� � �������
	void remove_car_from_catalog(int aID);				//������� �� �������� ���� � ��������� ID
	void change_catalog_item(int aID, string aModel_new); //�������� ������������ ���������� � ��������
	
	//����� add_salon ������ ��� �� ����� �������� ����� ����� � ������� � �� ������������� (manufacturerDB factory) � ������� ������ ������ manufacturerDB
	void add_salon(string salon_name, string salon_city, string director_surname, string director_name, string salon_address = "", string salon_phone = "");
	int get_size_salons()const;
	int get_salon_index(int salonID);
	salonDB *get_all_salonsDB();

	//����� remove_salon ������ ��� �� ����� ������� ����� �� �������� � �� ������������� (manufacturerDB factory) � ������� ������ ������ manufacturerDB
	void remove_salon(int salonID);
	//����� ������ ������ salon:
	void change_salon_name(int salonID, string aName);
	void change_salon_city(int salonID, string aCity);
	void change_salon_address(int salonID, string aAddress);
	void change_salon_phone(int salonID, string aPhone);

	void synchronize_cars(int salonID);	//���������������� ������� ������������ ����������� (������ ���������� �� �� ������������� � �� ������) (�� ������� ���������)
	void synchronize_test_autos(int salonID);	//���������������� ������� ����-���� � ������ (������ ���������� �� �� ������ � �� �������������) (�� ������� ���������)
	void synchronize_clients_and_inquiries(int salonID);	//���������������� ������� �������� � �� ������� (������ ���������� �� �� ������ � �� �������������) (�� ������� ���������)
	void synchronize_all(int salonID);	//���������������� ��� ����� (��� ������ ������) - ������ ��������� ���������� ��� ������ (�� ������� ���������)
	DBMS operator=(DBMS aDBMS);
	
	void print_to_file(char* file_name);
	void read_from_file(char* file_name);
	
	friend class manufacturerDB;
	friend class salonDB;
	friend ostream& operator<<(ostream& os, DBMS& db);
};