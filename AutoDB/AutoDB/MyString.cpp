#include <string> using std::string;
#include <string.h>
#include <stdlib.h>

string::string()									//1
{
	sz = 0;
	str = nullptr;
}

string::string(char *aStr)							//2.1
{
	if (aStr != nullptr)
	{
		sz = (int)strlen(aStr) + 1;
		str = new char[sz];
		strcpy_s(str, sz, aStr);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

string::string(const string &astring)				//2.2
{
	if (astring.str && astring.sz)
	{
		sz = astring.sz;
		str = new char[sz];
		strcpy_s(str, sz, astring.str);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

string::~string()
{
	delete[] str;
}

void string::set_string(char *aStr)
{
	if (str)
		delete[] str;
	if (aStr, strlen(aStr))
	{
		sz = (int)strlen(aStr) + 1;
		str = new char[sz];
		strcpy_s(str, sz, aStr);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
}

char *string::get_string()const 
{
	return str;
}

int string::get_stringlen()const
{
	return sz;
}

string string::operator=(const string &astring)		//3
{
	if (str)
		delete[] str;
	if (astring.str && astring.sz)
	{
		sz = astring.sz;
		str = new char[sz];
		strcpy_s(str, sz, astring.str);
	}
	else
	{
		sz = 0;
		str = nullptr;
	}
	return *this;
}

string string::operator+=(const string &astring)	//4
{
	if (astring.str && astring.sz)
	{
		if (str && sz)
		{
			char *temp = new char[sz + astring.sz - 1];
			strcpy_s(temp, sz, str);
			sz += astring.sz - 1;
			strcat_s(temp, sz, astring.str);
			delete[] str;
			str = new char[sz];
			str = temp;
		}
		else
		{
			sz = astring.sz;
			str = new char[sz];
			strcpy_s(str, sz, astring.str);
		}
	}
	return *this;
}

string string::operator+=(char a)					//6
{
	if (str)
	{
		sz++;
		char *temp = new char[sz];
		strcpy_s(temp, sz, str);
		temp[sz - 2] = a;
		temp[sz - 1] = '\0';
		delete[] str;
		str = new char[sz];
		str = temp;
	}
	else
	{
		sz = 2;
		str = new char[sz];
		str[0] = a;
		str[1] = '\0';
	}
	return *this;
}

string string::operator*=(const string &astring)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
		for (int i = 0; i < astring.sz; i++)
			if (str[i] == astring.str[i])
			{
				char *temp_new = new char [sz_temp + 1];
				for (int i = 0; i < sz_temp; i++)
					temp_new[i] = temp[i];
				temp_new[sz_temp] = str[i];
				delete[] temp;
				sz_temp++;
				temp = temp_new;
				break;
			}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

string string::operator*=(char *aStr)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
		for (int i = 0; i < strlen(aStr); i++)
			if (str[i] == aStr[i])
			{
				char *temp_new = new char[sz_temp + 1];
				for (int i = 0; i < sz_temp; i++)
					temp_new[i] = temp[i];
				temp_new[sz_temp] = str[i];
				delete[] temp;
				sz_temp++;
				temp = temp_new;
				break;
			}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

string string::operator/=(const string &astring)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j < astring.sz; j++)
			if (str[i] == astring.str[j])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second string
		{
			char *temp_new = new char[sz_temp + 1];
			for (int i = 0; i < sz_temp; i++)
				temp_new[i] = temp[i];
			temp_new[sz_temp] = str[i];
			delete[] temp;
			sz_temp++;
			temp = temp_new;
		}
	}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

string string::operator/=(char *aStr)
{
	char *temp = nullptr;
	int sz_temp = 0;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j <= strlen(aStr); j++)
			if (str[i] == aStr[i])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second string
		{
			char *temp_new = new char[sz_temp + 1];
			for (int i = 0; i < sz_temp; i++)
				temp_new[i] = temp[i];
			temp_new[sz_temp] = str[i];
			delete[] temp;
			sz_temp++;
			temp = temp_new;
		}
	}
	delete[] str;
	str = temp;
	sz = sz_temp;
	return *this;
}

string string::operator+(const string &astring)		//5.1
{
	if (astring.str && astring.sz)
	{
		if (str && sz)
		{
			char *temp = new char[sz + astring.sz - 1];
			strcpy_s(temp, sz, str);
			int sz_tmp = sz + astring.sz - 1;
			strcat_s(temp, sz_tmp, astring.str);
			string tmp(temp);
			return tmp;
		}
		else
			return astring;
	}
	else
		return *this;
}

string string::operator+(char *aStr)				//5.2
{
	if (aStr)
	{
		if (str && sz)
		{
			int sz_tmp = sz + (int)strlen(aStr);
			char *temp = new char[sz_tmp];
			strcpy_s(temp, sz, str);
			strcat_s(temp, sz_tmp, aStr);
			string tmp(temp);
			return tmp;
		}
		else
		{
			string tmp(aStr);
			return tmp;
		}
	}
	else
		return *this;
}


string string::operator*(const string &astring)
{
	string temp;
	for (int i = 0; i < sz; i++)
		for (int j = 0; j < astring.sz; j++)
			if (str[i] == astring.str[j])
			{
				temp += str[i];
				break;
			}
	return temp;
}

string string::operator*(char *aStr)
{
	string temp;
	for (int i = 0; i < sz; i++)
		for (int j = 0; j <= strlen (aStr); j++)
			if (str[i] == aStr[j])
			{
				temp += str[i];
				break;
			}
	return temp;
}

string string::operator/(const string &astring)
{
	string temp;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j < astring.sz; j++)
			if (str[i] == astring.str[j])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second string
			temp += str[i];
	}
	return temp;
}

string string::operator/(char *aStr)
{
	string temp;
	for (int i = 0; i < sz; i++)
	{
		bool flag = false;
		for (int j = 0; j <= strlen(aStr); j++)
			if (str[i] == aStr[j])
			{
				flag = true;
				break;
			}
		if (!flag)		//if current symbol was not found in second string
			temp += str[i];
	}
	return temp;
}

bool string::operator==(const string &astring)		//7.1
{
	if (!str && !astring.str)
		return false;
	if (strcmp(str, astring.str) == 0)
		return true;
	return false;
}

bool string::operator==(char *aStr)					//8.1
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) == 0)
		return true;
	return false;
}

bool string::operator!=(const string &astring)		//7.2
{
	if (!str && !astring.str)
		return false;
	if (strcmp(str, astring.str) != 0)
		return true;
	return false;
}

bool string::operator!=(char *aStr)					//8.2
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) != 0)
		return true;
	return false;
}

bool string::operator>(const string &astring)		//9.1
{
	if (!str && !astring.str)
		return false;
	if (strcmp(str, astring.str) > 0)
		return true;
	return false;
}

bool string::operator>(char *aStr)					//9.2
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) > 0)
		return true;
	return false;
}

bool string::operator>=(const string &astring)
{
	if (!str && !astring.str)
		return false;
	if (strcmp(str, astring.str) > 0 || strcmp(str, astring.str) == 0)
		return true;
	return false;
}

bool string::operator>=(char *aStr)
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) > 0 || strcmp(str, aStr) == 0)
		return true;
	return false;
}

bool string::operator<(const string &astring)		//9.3	
{
	if (!str && !astring.str)
		return false;
	if (strcmp(str, astring.str) < 0)
		return true;
	return false;
}

bool string::operator<(char *aStr)					//9.4
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) < 0)
		return true;
	return false;
}

bool string::operator<=(const string &astring)
{
	if (!str && !astring.str)
		return false;
	if (strcmp(str, astring.str) < 0 || strcmp(str, astring.str) == 0)
		return true;
	return false;
}

bool string::operator<=(char *aStr)
{
	if (!str && !aStr)
		return false;
	if (strcmp(str, aStr) < 0 || strcmp(str, aStr) == 0)
		return true;
	return false;
}

void string::print()
{
	//std::cout << str << "\n";
}

char& string::operator[](int aIndex)
{
	return str[aIndex];
}

string::operator int()const		//converting string to int
{
	return atoi(str);
}

string::operator double()const		//converting string to double
{
	return atof(str);
}