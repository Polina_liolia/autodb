#include "manufacturerDB.h"
#include "salon.h"
using std::cout;
using std::endl;

//����������� �� ���������
manufacturerDB::manufacturerDB() : DataBase::DataBase()
{
	size_salons = 0;
	all_salons = nullptr;
	password = 1111;
}

//������������� �����������
manufacturerDB::manufacturerDB(Car * aCatalog, int aSize_catalog) 
	: DataBase::DataBase(aCatalog, aSize_catalog)
{
	size_salons = 0;
	all_salons = nullptr;
	password = 1111;
}

//������������� ����������� 2� �������
manufacturerDB::manufacturerDB(Car * aCatalog, int aSize_catalog, salon * aAll_salons, int aSize_salons)
	: DataBase::DataBase(aCatalog, aSize_catalog)
{
	size_salons = aSize_salons;
	if (size_salons == 0)
	{
		all_salons = nullptr;
	}
	else
	{
		all_salons = new salon [size_salons];
		for (int i = 0; i < size_salons; i++)
		{
			all_salons[i] = aAll_salons[i]; 
		}
	}
	password = 1111;
}

//����������� �����������
manufacturerDB::manufacturerDB(const manufacturerDB & aMDB): 
	DataBase(aMDB)	
{
	size_salons = aMDB.size_salons;
	if (size_salons == 0)
	{
		all_salons = nullptr;
	}
	else
	{
		all_salons = new salon[size_salons];
		for (int i = 0; i < size_salons; i++)
		{
			all_salons[i] = aMDB.all_salons[i]; 
		}
	}
	password = aMDB.password;
}

//����������	
manufacturerDB::~manufacturerDB()	
{
	delete[] all_salons;
}

int manufacturerDB::get_password()const
{
	return password;
}

void manufacturerDB::set_password(int pas)
{
	password = pas;
}

//������� ���������� ���������� ������� 
int manufacturerDB::get_size_salons() const
{
	return size_salons;
}

//������� ���������� ������ �������
salon * manufacturerDB::get_all_salons() const
{
	return all_salons;
}

//������� ��������� ������� � ������ ��������
// �������� ������  �� ��������� ������ client
void manufacturerDB::add_client(const client & aClient)
{
	size_clients++;
	if (size_clients == 1)
	{
		clients_list = new client[size_clients];
		clients_list[0] = aClient;
	}
	else
	{
		client *buf = new client[size_clients];
		for (int i = 0; i < size_clients - 1; i++)
		{
			buf[i] = clients_list[i];
		}
		delete[]clients_list;
		clients_list = buf;
		clients_list[size_clients - 1] = aClient;
	}
}

//������� ��������� ������ � ������ �������� ������� �� ������� �������
//�������� ������ �������, ������ ��� ����������
//���� ������ ������� ��� - ����������
void manufacturerDB::add_inquiry(int client_index, const inquiry aInquiry) //contains exception
{
	if (client_index >= 0 && client_index < size_clients)
	{
		clients_list[client_index].add_inquiry(aInquiry);
	}
	else
	{
		throw "BAD CLIENT INDEX";
	}
}

void manufacturerDB::add_inquiry(const inquiry aInquiry)//contains exception
{
	if (size_clients != 0)
	{
		int result = exist_client_ID(aInquiry.get_client_ID());
		if (result != -1)
		{
			clients_list[result].add_inquiry(aInquiry);
		}
		else
		{
			throw "BAD CLIENT ID";
		}
	}
	else
	{
		throw "EMPTY ARRAY CLIENT";
	}
}
//������� ��������� ����� � ������ �������
// �������� ��������� ������ salon
void manufacturerDB::add_salon(const salon & aSalon)
{
	size_salons++;
	if (size_salons == 1)
	{
		all_salons = new salon[size_salons];
		all_salons[0] = aSalon;
	}
	else 
	{
		salon *buf = new salon[size_salons];
		for (int i = 0; i < size_salons - 1; i++)
		{
			buf[i] = all_salons[i];
		}
		delete[]all_salons;
		all_salons = buf;
		all_salons[size_salons - 1] = aSalon;
	}
}

//������� ������� ������ ������ �� ID
// �������� ID ������
//���������� ������ ������, ���� ������ � ����� ID, � -1 ���� �� ������
int manufacturerDB::find_index_salon(int salonID)
{
	int result = -1;
	for (int i = 0; i < size_salons; i++)
	{
		if (all_salons[i].ID == (salonID))
		{
			result = i;
			break;
		}
	}
	return result;
}

//������� ������� ����� �� ������� ������� �� ID ������
// �������� ID ������
//���� ������ ��� - ������ ����������
void manufacturerDB::remove_salon_by_ID(int salonID)//contains exception
{
	int result = find_index_salon(salonID);
	if (result != -1)
	{
		remove_salon_by_index(result);
	}
	else
	{
		throw "BAD ID SALON";
	}
}

//������� ������� ����� �� ������� ������� �� ID ������
// �������� ������ ������
//���� ������� ������ ��� - ������ ����������

void manufacturerDB::remove_salon_by_index(int salon_index)//contains exception
{
	if (salon_index >= 0 && salon_index < size_salons)
	{
		size_salons--;
		if (size_salons == 0)
		{
			delete[]all_salons;
			all_salons = nullptr;
		}
		else {
			salon *buf = new salon[size_salons];
			for (int i = 0; i < size_salons; i++)
			{
				if (i < salon_index)
				{
					buf[i] = all_salons[i];
				}
				else
				{
					buf[i] = all_salons[i + 1];
				}
			}
			delete[]all_salons;
			all_salons = buf;
		}
	}
	else
	{
		throw "BAD INDEX SALON NUMBER";
	}
}

//������� ��������������� ����� �� ������� ������� �� ID ������
// �������� ������ ������ � ����� ��� (������)
// ���� �� ������� ������ � ������ ID - ����������
void manufacturerDB::change_salon_name(int salonID, string aName)//contains exception
{
	int result = find_index_salon(salonID);
	if (result != -1)
	{
		all_salons[result].set_name(aName);
	}
	else
	{
		throw "BAD ID SALON";
	}
}

//������� �������� ����� ������ �� ������� ������� �� ID ������
// �������� ������ ������ � ����� ��� (������)
// ���� �� ������� ������ � ������ ID - ����������
void manufacturerDB::change_salon_city(int salonID, string aCity)//contains exception
{
	int result = find_index_salon(salonID);
	if (result != -1)
	{
		all_salons[result].set_city(aCity);
	}
	else
	{
		throw "BAD ID SALON";
	}
}

//������� �������� ������ ������ �� ������� ������� �� ID ������
// �������� ������ ������ � ����� ��� (������)
// ���� �� ������� ������ � ������ ID - ����������
void manufacturerDB::change_salon_address(int salonID, string aAddress)//contains exception
{
	int result = find_index_salon(salonID);
	if (result != -1)
	{
		all_salons[result].set_address(aAddress);
	}
	else
	{
		throw "BAD ID SALON";
	}
}

//������� �������� ������� ������ �� ������� ������� �� ID ������
// �������� ������ ������ � ����� ��� (������)
// ���� �� ������� ������ � ������ ID - ����������
void manufacturerDB::change_salon_phone(int salonID, string aPhone)//contains exception
{
	int result = find_index_salon(salonID);
	if (result != -1)
	{
		all_salons[result].set_phone(aPhone);
	}
	else
	{
		throw "BAD ID SALON";
	}
}

//������� ������� ������ ������ � ������� �� ID 
// �������� ID ������
// ��������� ������ ������, ���� �� �������, � -1 ���� ������ � ������ ID �� �������
int manufacturerDB::find_index_car_catolog(int carID)
{
	int result = -1;
	for (int i = 0; i < size_catalog; i++)
	{
		if (catalog[i].get_ID() == (carID))	//���� ��� ������� ������, ��� ������������ catalog[i].get_ID()
		{
			result = i;
			break;
		}
	}
	return result;
}

//������� ��������� ������ � ������ �����������
// �������� ��������� ������ �ar
void manufacturerDB::add_car(const Car & aCar)
{
	size_catalog++;
	if (size_catalog == 1)
	{
		catalog = new Car [size_catalog];
		catalog[0] = aCar; // ����������� �������� = ����� Car		//�� ����������� ����������� - ��� ��� ������������ �������
		                                                             // ����, ��� ���� �����, ��� ������������
	}
	else
	{
		Car *buf = new Car[size_catalog];
		for (int i = 0; i < size_catalog - 1; i++)
		{
			buf[i] = catalog[i];
		}
		delete[]catalog;
		catalog = buf;
		catalog[size_catalog - 1] = aCar;
	}
}


//������� ��������� ������ � ������ �����������
// �������� ID ���������� � ������ (������)
void manufacturerDB::add_car(int aID, string aModel)
{
	add_car(Car (aID, aModel));
}

//������� ������� ������ �� ������� ����������� �� ID
// �������� ID ���������� ��� �������� �� ����
// � ������ ������������ ���������� � ������ ID - ����������
void manufacturerDB::remove_car_by_ID(int carID)//contains exception
{
	int result = find_index_car_catolog(carID);
	if (result != -1)
	{
		remove_car_by_index(result);
	}
	else
	{
		throw "BAD ID CAR";
	}
}

//������� ������� ������ �� ������� ����������� �� ������� � �������
// �������� ������ ���������� ��� �������� �� ����
// � ������ ������������ ���������� � ������ �������� - ����������
void manufacturerDB::remove_car_by_index(int car_index)	//contains exception
{
	if (car_index >= 0 && car_index < size_catalog)
	{
		size_catalog--;
		if (size_catalog == 0)
		{
			delete[] catalog;
			catalog = nullptr;
		}
		else {
			Car *buf = new Car[size_catalog];
			for (int i = 0; i < size_catalog; i++)
			{
				if (i < car_index)
				{
					buf[i] = catalog[i];
				}
				else
				{
					buf[i] = catalog[i + 1];
				}
			}
			delete[]catalog;
			catalog = buf;
		}
	}
	else
	{
		throw "BAD INDEX NUMBER";
	}
}

//������� ������ ������ ���������� �� ID
// �������� ID ���������� � ������ - ����� ������ ����������
// � ������ ������������ ���������� � ������ ID - ����������
void manufacturerDB::change_car(int carID, string aModel_new)//contains exception
{
	int result = find_index_car_catolog(carID);
	if (result != -1)
	{
		catalog[result].set_model(aModel_new);
	}
	else
	{
		throw "BAD ID CAR";
	}
}

//������� ������ ������ ������� 
// ��������� ������ �� ��������� ������ inquiry � �������� ������� �������
void manufacturerDB::change_inquiry_status(inquiry & aInquiry, inquiry_status aStatus)
{
	aInquiry.set_status(aStatus);
}


//������� ���������� ������ �������� ���� ��������, ���� �������
inquiry * manufacturerDB::get_all_inquiries()
{
	int size_all_inquiries = get_size_all_inquiries();
	inquiry *All_inquiries;
	if (size_all_inquiries == 0)
	{
		All_inquiries = nullptr;
	}
	else
	{
		All_inquiries = new inquiry[size_all_inquiries]; 
		int index = 0;
		for (int j = 0; j < size_clients; j++)
		{
			for (int k = 0; k < clients_list[j].list_sz; k++)	
			{
				All_inquiries[index] = clients_list[j].inquirys_list[k];
				index++;
			}
		}
	}
	return All_inquiries;
}

//������� ������� � ���������� ���������� �������� ���� �������� ���� ������� (�����)
int manufacturerDB::get_size_all_inquiries()
{
	int size_all_inquiries = 0;
	for (int j = 0; j < size_clients; j++)
	{
		size_all_inquiries += clients_list[j].list_sz;
	}
	return size_all_inquiries;
}

//������� ���������� �������, ������� ������� ������ �� ������� ������ �� ID ������
// ��������� ID ������
//�������� ID ������. ���� ������ ��� - ��������� ����������
inquiry * manufacturerDB::get_all_salon_inquiries(int aSalonID)//contains exception
{
	if (size_salons != 0)
	{
		int result = find_index_salon(aSalonID);
		if (result != -1)
		{
			int size_all_salon_inquiries = get_size_all_salon_inquiries(aSalonID);
			inquiry *All_salon_inquiries;
			if (size_all_salon_inquiries == 0)
			{
				All_salon_inquiries = nullptr;
			}
			else
			{
				All_salon_inquiries = new inquiry[size_all_salon_inquiries]; // ��������� ������ ����� ���, ���� �� ��� ������.
				int index = 0;

				//������� ��� ������ � ����� ��� ���� � ������ ID
				int size_all_inquiries = get_size_all_inquiries();
				inquiry *All_inquiries = get_all_inquiries();

				for (int i = 0; i < size_all_inquiries; i++)
				{
					if (All_inquiries[i].get_salon_ID() == aSalonID)
					{
						All_salon_inquiries[index] = All_inquiries[i];
						index++;
					}
				}
			}
			return All_salon_inquiries;
		}
		else
		{
			throw "BAD ID SALON";
		}
	}
	else
	{
		throw "EMPTY ARRAY SALONS";
	}
}

//������� ������� � ���������� ���������� �������� �� ������ ������ �� ID ������
//��������� ID ������
//�������� ID ������. ���� ������ ��� - ��������� ����������
int manufacturerDB::get_size_all_salon_inquiries(int aSalonID)//contains exception
{
	if (size_salons != 0)
	{
		int result = find_index_salon(aSalonID);
		if (result != -1)
		{
			int size_all_salon_inquiries = 0;
			int size_all_inquiries = get_size_all_inquiries();

			inquiry *All_inquiries = get_all_inquiries();

			for (int i = 0; i < size_all_inquiries; i++)
			{
				if (All_inquiries[i].get_salon_ID() == aSalonID)
				{
					size_all_salon_inquiries++;
				}
			}
			return size_all_salon_inquiries;
		}
		else
		{
			throw "BAD ID SALON";
		}
	}
	else
	{
		throw "EMPTY ARRAY SALONS";
	}
	return 0;
}



//������� ���������� �������, ������� ������� ������ �� ������� ��������� �� ID ���������
// ��������� ID ���������
inquiry * manufacturerDB::get_all_manager_inquiries(int aManagerID)
{
	int size_all_manager_inquiries = get_size_all_manager_inquiries(aManagerID);
	inquiry *All_manager_inquiries;
	if (size_all_manager_inquiries == 0)
	{
		All_manager_inquiries = nullptr;
	}
	else
	{
		All_manager_inquiries = new inquiry[size_all_manager_inquiries]; 
		int index = 0;

		//������� ��� ������ � ����� ��� ���� � ������ ID
		int size_all_inquiries = get_size_all_inquiries();
		inquiry *All_inquiries = get_all_inquiries();

		for (int i = 0; i < size_all_inquiries; i++)
		{
			if (All_inquiries[i].get_manager_ID() == aManagerID)
			{
				All_manager_inquiries[index] = All_inquiries[i];
				index++;
			}
		}
	}

	return All_manager_inquiries;
}

//������� ������� � ���������� ���������� �������� �� ������ ��������� �� ID ���������
//��������� ID ���������
int manufacturerDB::get_size_all_manager_inquiries(int aManagerID)
{
	int size_all_manager_inquiries = 0;
	int size_all_inquiries = get_size_all_inquiries();

	inquiry *All_inquiries = get_all_inquiries();

	for (int i = 0; i < size_all_inquiries; i++)
	{
		if (All_inquiries[i].get_manager_ID() == aManagerID)
		{
			size_all_manager_inquiries++;
		}
	}
	return size_all_manager_inquiries;
}

//������� ���������� �������, ������� ������� ������ �� ������� ������� �� ID �������
// ��������� ID �������
//�������� ID �������. ���� ������ ��� - ��������� ����������
inquiry * manufacturerDB::get_all_client_inquiries(int aClientID)//contains exception
{
	if (size_clients != 0)
	{
		int result = exist_client_ID(aClientID);
		if (result != -1)
		{
			int size_all_client_inquiries = get_size_all_client_inquiries(aClientID);
			inquiry *All_client_inquiries;
			if (size_all_client_inquiries == 0)
			{
				All_client_inquiries = nullptr;
			}
			else
			{
				All_client_inquiries = new inquiry[size_all_client_inquiries];
				int index = 0;

				//������� ��� ������ � ����� ��� ���� � ������ ID
				int size_all_inquiries = get_size_all_inquiries();
				inquiry *All_inquiries = get_all_inquiries();

				for (int i = 0; i < size_all_inquiries; i++)
				{
					if (All_inquiries[i].get_client_ID() == aClientID)
					{
						All_client_inquiries[index] = All_inquiries[i];
						index++;
					}
				}
			}
			return All_client_inquiries;
		}
		else
		{
			throw "BAD ID CLIENT";
		}
	}
	else
	{
		throw "EMPTY ARRAY CLIENT";
	}
}


//������� ������� � ���������� ���������� �������� �� ������ ������� �� ID �������
//��������� ID �������
//�������� ID �������. ���� ������ ��� - ��������� ����������
int manufacturerDB::get_size_all_client_inquiries(int aClientID)//contains exception
{
	if (size_clients != 0)
	{
		int result = exist_client_ID(aClientID);
		if (result != -1)
		{
			int size_all_client_inquiries = 0;

			int size_all_inquiries = get_size_all_inquiries();
			inquiry *All_inquiries = get_all_inquiries();

			for (int i = 0; i < size_all_inquiries; i++)
			{
				if (All_inquiries[i].get_client_ID() == aClientID)
				{
					size_all_client_inquiries++;
				}
			}
			return size_all_client_inquiries;
		}
		else
		{
			throw "BAD ID CLIENT";
		}
	}
	else
	{
		throw "EMPTY ARRAY CLIENT";
	}
}

//������� ���������� ������� �� ���� ��� ����������� �� ID �����������
// ��������� ID ����������
//�������� ID ����. ���� ������ ��� - ��������� ����������
inquiry * manufacturerDB::get_all_car_inquiries(int aCarID)//contains exception
{
	if (size_catalog != 0)
	{
		int result = find_index_car_catolog(aCarID);
		if (result != -1)
		{
			int size_all_car_inquiries = get_size_all_car_inquiries(aCarID);
			inquiry *All_car_inquiries;
			if (size_all_car_inquiries == 0)
			{
				All_car_inquiries = nullptr;
			}
			else
			{
				All_car_inquiries = new inquiry[size_all_car_inquiries];
				int index = 0;

				//������� ��� ������ � ����� ��� ���� � ������ ID
				int size_all_inquiries = get_size_all_inquiries();
				inquiry *All_inquiries = get_all_inquiries();

				for (int i = 0; i < size_all_inquiries; i++)
				{
					if (All_inquiries[i].get_car_ID() == aCarID)
					{
						All_car_inquiries[index] = All_inquiries[i];
						index++;
					}
				}
			}
			return All_car_inquiries;
		}
		else
		{
			throw "BAD ID CAR";
		}
	}
	else
	{
		throw "EMPTY CATALOG CAR";
	}
}

//������� ������� � ���������� ���������� �������� �� ������ �� ID ������
//��������� ID ������
//�������� ID ����. ���� ������ ��� - ��������� ����������
int manufacturerDB::get_size_all_car_inquiries(int aCarID)//contains exception
{
	if (size_catalog != 0)
	{
		int result = find_index_car_catolog(aCarID);
		if (result != -1)
		{
			int size_all_car_inquiries = 0;

			int size_all_inquiries = get_size_all_inquiries();
			inquiry *All_inquiries = get_all_inquiries();

			for (int i = 0; i < size_all_inquiries; i++)
			{
				if (All_inquiries[i].get_car_ID() == aCarID)
				{
					size_all_car_inquiries++;
				}
			}
			return size_all_car_inquiries;
		}
		else
		{
			throw "BAD ID CAR";
		}
	}
	else
	{
		throw "EMPTY CATALOG CAR";
	}
	return 0;
}

// ������� ���������� ��������� �������
//��������� ID �������
//���� �� ������� ������ � ������ ID - ����������
inquiry manufacturerDB::get_inquiry(int *aInquiryID)//contains exception
{
	int size_all_inquiries = get_size_all_inquiries();
	inquiry *All_inquiries = get_all_inquiries();
	
	for (int i = 0; i < size_all_inquiries; i++)
	{
		if (All_inquiries[i].get_salon_ID() == aInquiryID[0] &&
			All_inquiries[i].get_manager_ID() == aInquiryID[1] &&
			All_inquiries[i].get_client_ID() == aInquiryID[2] &&
			All_inquiries[i].get_car_ID() == aInquiryID[3])
		{
			return All_inquiries[i];
		}
	}
		throw "BAD ID INQUIRY";
}

//���������� true, ���� ������ � ����� ID ��������� � false, ���� ���
// ��������� ID �������
int manufacturerDB::exist_client_ID(int aClientID)
{
	int result = -1;
	for (int i = 0; i < size_clients; i++)
	{
		if (clients_list[i].ID[1] == aClientID)
		{
			result = i;
			break;
		}
	}
	return result;
}

manufacturerDB manufacturerDB::operator=(manufacturerDB aMDB)
{
	size_catalog = aMDB.size_catalog;
	if (size_catalog == 0)
	{
		catalog = nullptr;
	}
	else
	{
		delete[] catalog;
		catalog = new Car[size_catalog];
		for (int i = 0; i < size_catalog; i++)
		{
			catalog[i] = aMDB.catalog[i];
		}
	}
	size_autopark = aMDB.size_autopark;
	if (size_autopark == 0)
	{
		autopark = nullptr;
	}
	else
	{
		delete[] autopark;
		autopark = new test_auto[size_autopark];
		for (int i = 0; i < size_autopark; i++)
		{
			autopark[i] = aMDB.autopark[i];
		}
	}
	size_clients = aMDB.size_clients;
	if (size_clients == 0)
	{
		clients_list = nullptr;
	}
	else
	{
		delete[] clients_list;
		clients_list = new client[size_clients];
		for (int i = 0; i < size_clients; i++)
		{
			clients_list[i] = aMDB.clients_list[i];
		}
	}
	size_salons = aMDB.size_salons;
	if (size_salons == 0)
	{
		all_salons = nullptr;
	}
	else
	{
		all_salons = new salon[size_salons];
		for (int i = 0; i < size_salons; i++)
		{
			all_salons[i] = aMDB.all_salons[i];
		}
	}
	return *this;
}

void manufacturerDB::print_to_file(ofstream & F) const
{
	if (!F)
		throw "manufacturerDB: file was not opened, can't write";
	try
	{
		DataBase::print_to_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	int buf = size_salons;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing size_salons
	for (int i = 0; i < size_salons; i++)
	{
		try
		{
			all_salons[i].print_to_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
	buf = password;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing password
}

void manufacturerDB::read_from_file(ifstream & F)
{
	if (!F)
		throw "manufacturerDB: file was not opened, can't read";
	try
	{
		DataBase::read_from_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	F.read(reinterpret_cast<char*>(&size_salons), sizeof(int));	//reading size_salons
	
	delete[] all_salons;
	all_salons = new salon[size_salons];
	for (int i = 0; i < size_salons; i++)
	{
		try
		{
			all_salons[i].read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
	F.read(reinterpret_cast<char*>(&password), sizeof(int));	//reading password
}


void manufacturerDB::remove_inquiry(int clientID, int carID)	//contains exception
{
	//checking, if there is such a client in the list
	if (!check_client_ID(clientID))
		throw "No client with such ID";
	//checking, does such inquiry exists:
	if (!check_clients_inquiry(clientID, carID))
		throw "No such an inquiry";
	//if client and inquiry exist, checking the status of inquiry:
	int client_index = get_client_index(clientID);
	int inq_index = get_clients_inquiry_index(clientID, carID);
	clients_list[client_index].delete_inquiry((inq_index + 1));
}