#pragma once
#include<iostream>
#include <string> 
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;
using std::string;
using std::ostream;

/*
�������� ���������� �� �������� ������ ����������
� ������� ����������� ������ Car �������� ������� ���� ������������ �����������, ��������� ��� ������
*/
class Car
{
private:
	int ID; //ID ����(�� ��������) ��������� �� ������� 1, 2...
	string model;

public:
	Car();		//default constructor
	Car(int aID, string aModel);	//overloaded constructor
	Car(const Car &aCar);			//copy constructor
	~Car();				//destructor

	/*set ��������� ���������� ����� ��� �������� ��� ������������ ��������*/
	void set_ID(int aID);				//to set or change ID of a car
	void set_model(string aModel);		//to set or change model
	int get_ID()const;			
	string get_model()const;
	friend ostream& operator<<(ostream &o, Car &aCar);

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
};



