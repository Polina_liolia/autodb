#pragma once
#include<iostream>
using std::ostream;
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;

class date
{
private:
	short day;
	short month;
	short year;

	friend class inquiry;
	friend class employee;
	friend class stuff;
	friend class director;
	friend class manager;
	
	// friend ostream & operator << (ostream& os, const string &astring); //����� ������ string - ����� � ����� ������ string
	friend ostream& operator << (ostream& os, date &aDate);

public:
	
	date()	//default constructor
	{
		day = 0;
		month = 0;
		year = 0;
	}

	date(short aDay, short aMonth, short aYear)	//overloaded constructor
	{
		day = aDay;
		month = aMonth;
		year = aYear;
	}

	date(const date &BD)	//copy constructor
	{
		day = BD.day;
		month = BD.month;
		year = BD.year;
	}

	~date() {}		//destructor

	// ���������� ��������� ������������
	// ��������� ������ �� ��������� ������ date
	// ���������� ������� ��������� ������ date
	date& operator= (const date &aDate)	
	{
		day = aDate.day;
		month = aDate.month;
		year = aDate.year;
		return *this;
	}

	void print_to_file(ofstream &F)const
	{
		if (!F)
			throw "Date: file was not opened, can't write";
		short buf = day;
		F.write(reinterpret_cast<char*>(&buf), sizeof(short));	//writing day
		buf = month;
		F.write(reinterpret_cast<char*>(&buf), sizeof(short));	//writing month
		buf = year;
		F.write(reinterpret_cast<char*>(&buf), sizeof(short));	//writing year
	}

	void read_from_file(ifstream &F)
	{
		if (!F)
			throw "Time: file was not opened, can't write";
		F.read(reinterpret_cast<char*>(&day), sizeof(short));	//reading day
		F.read(reinterpret_cast<char*>(&month), sizeof(short));	//reading month
		F.read(reinterpret_cast<char*>(&year), sizeof(short));	//reading year
	}
};

