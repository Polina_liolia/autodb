
#include "DBMS.h"
#include "infs.h"
#include <stdexcept>

#include <iostream>
#include <string> 
using std::string;
using std::cout;
using std::endl;


//global outstream overload for Car
ostream& operator<<(ostream &o, Car &aCar)
{
	o << "ID:\t" << aCar.ID << "\n";
	o << "model:\t" << aCar.model << "\n";
	return o; // �������� ������ ���
}

// ���������� ��������� ������
// ��������� ������ �� ��������� ������ date � ostream
// ���������� ��������� ������ ostream
ostream& operator << (ostream& os, date &aDate)
{
	os << aDate.day << "." << aDate.month << "." << aDate.year << "\n";
	return os;
}

//global outstream overload for employee
ostream& operator<<(ostream& os, const employee& dt)
{
	os << "ID:\t" << dt.ID[0] << "-" << dt.ID[1] << "\n";
	os << "Name:\t" << dt.name << "\n";
	os << "Surname:\t" << dt.surname << "\n";
	os << "Phone:\t" << dt.phone << "\n";
	os << "position:\t";
	if (dt.type == mgr)
		os << "manager" << "\n";
	else
		os << "director" << "\n";
	return os;
}

//������� ������� � ����� ��������� ������ Inquiry
//��������� ��������� ������� ostream � inquiry
//���������� �������� ������ ostream
ostream & operator<<(ostream & os, inquiry &aInquiry)
{
	os << "ID:\t";
	for (int i = 0; i < 4; i++)
	{
		os << aInquiry.ID[i] << " ";
	}
	os << "\n";
	os << "Date\t" << aInquiry.date_added;
	switch (aInquiry.status)
	{
	case 0:
		os << "added" << "\n";
		break;
	case 1:
		os << "changed" << "\n";
		break;
	case 2:
		os << "synchronyzed" << "\n";
		break;
	case 3:
		os << "accepted" << "\n";
		break;
	case 4:
		os << "is_moving" << "\n";
		break;
	case 5:
		os << "rejected" << "\n";
	case 6:
		os << "done" << "\n";
		break;
	}
	if (aInquiry.options_sz != 0)
	{
		for (int i = 0; i < aInquiry.options_sz; i++)
		{
			os << aInquiry.additional_options[i] << "\n";
		}
	}
	else
	{
		os << "No additional options" << "\n";
	}
	return os;
}


//friend overload of cout for client
ostream& operator<<(ostream & os, client &aclient)
{
	os << "ID:\t" << aclient.ID[0] << "-" << aclient.ID[1] << "\n";
	os << "status:\t";
	switch (aclient.status)
	{
	case 0: os << "added" << "\n";	break;
	case 1: os << "changed" << "\n"; break;
	case 2: os << "synchronyzed" << "\n"; break;
	}
	os << aclient.name << " " << aclient.surname << "\n";
	os << aclient.phone << "\n";
	os << "Number of inquries: " << aclient.list_sz << "\n";
	for (int i = 0; i < aclient.list_sz; i++)
		os << aclient.inquirys_list[i];
	return os;
}

//������������� ����������� ������ � ����� manufacturerDB
ostream & operator<<(ostream & os, manufacturerDB &aMDB)
{
	os << "Cars in catalog:\t" << aMDB.size_catalog << "\n";
	for (int i = 0; i < aMDB.size_catalog; i++)
	{
		os << aMDB.catalog[i];
	}
	os << "\n";
	os << "Cars in autopark:\t" << aMDB.size_autopark << "\n";
	for (int i = 0; i < aMDB.size_autopark; i++)
	{
		os << aMDB.autopark[i];
	}
	os << "\n";
	os << "Number of clients:\t" << aMDB.size_clients << "\n";
	for (int i = 0; i < aMDB.size_clients; i++)
	{
		os << aMDB.clients_list[i];
	}
	os << "\n";
	os << "Number of salons:\t" << aMDB.size_salons << "\n";
	for (int i = 0; i < aMDB.size_salons; i++)
	{
		os << aMDB.all_salons[i];
	}
	os << "\n";
	return os;
}

//The overloaded operator to display the instance property class salon
ostream & operator << (ostream& os, const salon &aSalon)
{
	os << "ID:\t" << aSalon.ID << "\n";
	os << "Name:\t" << aSalon.name << "\n";
	os << "City:\t" << aSalon.city << "\n";
	os << "Address:\t" << aSalon.address << "\n";
	os << "Phone:\t" << aSalon.phone << "\n";
	return os;
}

//global overload of outstream for test_auto
ostream& operator<<(ostream &os, test_auto &aCar)
{
	os << "ID\t";
	for (int i = 0; i < 2; i++)
	{
		os << aCar.ID[i] << " ";
	}
	os << aCar.model << "\n";
	return os;
}

//global overload of outstream for stuff
ostream& operator<< (ostream& os, stuff &aStuff)
{
	os << "Personnel of salon:\n";
	os << "Director:\n" << aStuff.direk;
	os << "\nManagers:\n";
	for (int i = 0; i < aStuff.size_mgrs; i++)
		os << i + 1 << ". " << aStuff.mgrs[i] << "\n";
	return os;
}

//global overload of outstream for salonDB
ostream& operator << (ostream& os, salonDB aSalonDB)
{
	os << "Salon ID:\t" << aSalonDB.salon_data.get_ID() << "\n";
	os << "Salon name:\t" << aSalonDB.salon_data.get_name() << "\n";
	os << "Salon city:\t" << aSalonDB.salon_data.get_city() << "\n";
	os << "Salon address:\t" << aSalonDB.salon_data.get_address() << "\n";
	os << "Salon phone:\t" << aSalonDB.salon_data.get_phone() << "\n";
	os << aSalonDB.salon_personnel;
	os << "\n";
	os << "Autopark:\n";
	for (int i = 0; i < aSalonDB.size_autopark; i++)
		os << aSalonDB.autopark[i].ID[1] << aSalonDB.autopark[i];
	os << "\n";
	os << "Clients:\n";
	for (int i = 0; i < aSalonDB.size_clients; i++)
		os << aSalonDB.clients_list[i].ID[1] << aSalonDB.clients_list[i];
	os << "\n";
	return os; //�������� ������ ���
}

//global overload of outstream for DBMS
ostream& operator<<(ostream& os, DBMS& db)
{
	os << "Manufacturer:\n" << db.factory;
	os << "Salons:\n";
	for (int i = 0; i < db.size_salons; i++)
		os << db.all_salonsDB[i];
	return os;
}

void main()
{
	//creating an instance of DBMS:
	DBMS Mersedes;
	infs test;
	test.login(Mersedes);

}