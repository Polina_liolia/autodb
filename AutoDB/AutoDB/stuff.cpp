#include "stuff.h"
#include "director.h"

stuff::stuff() // default constructor
{
	size_mgrs = 0;
	counter = 2;
	mgrs = nullptr;
}
stuff::stuff(director aDir)
{
	direk = aDir;
	size_mgrs = 0;
	counter = 0;
	mgrs = nullptr;
}

stuff::stuff(const stuff &aStuff) //copy constructor
{
	direk = aStuff.direk;	
	size_mgrs = aStuff.size_mgrs;
	if (size_mgrs == 0)
		mgrs = nullptr;
	else
	{
		mgrs = new manager[size_mgrs];
		for (int i = 0; i < size_mgrs; i++)
			mgrs[i] = aStuff.mgrs[i];
	}
}
stuff::~stuff()
{
	delete[] mgrs;
}
stuff::stuff(string director_surname, string director_name, short director_bDay, short director_bMonth, short director_bYear, string director_phone)//overload constructor
{
	direk.name = director_name;
	direk.surname = director_surname;
	direk.phone = director_phone;
	direk.birth.day = director_bDay;
	direk.birth.month = director_bMonth;
	direk.birth.year = director_bYear;
}
void  stuff::set_director(director aDir)// metod add new derector exemp class aDir
{
	direk = aDir;
	direk.ID[1] = 1;
}
void stuff::set_director(int aSalonID, string aSurname, string aName, short aDay, short aMonth, short aYear, string aPhone)//metod set_dir 
{
		direk.ID[0] = aSalonID;
		direk.ID[1] = 1;
		direk.name = aName;
		direk.surname = aSurname;
		direk.phone = aPhone;
		direk.birth.day = aDay;
		direk.birth.month = aMonth;
		direk.birth.year = aYear;
}
void  stuff::add_manager(manager aMgr) //metod add_mgrs with exemp aMgr
{
		size_mgrs++;
		mgrs = new manager[size_mgrs];
		if (size_mgrs > 0)
		{
			manager *buf = new manager[size_mgrs];
			for (int q = 0; q < size_mgrs - 1; q++)
				buf[q] = mgrs[q];
			delete[]mgrs;
			mgrs = buf;
		}
		mgrs[size_mgrs - 1] = aMgr;
}
void stuff::add_manager(int aSalonID, string aSurname, string aName, short aDay, short aMonth, short aYear, string aPhone)// metod add_mgr
{
		size_mgrs++;
		if (size_mgrs > 1)
		{
			manager *buf = new manager[size_mgrs];
			for (int q = 0; q < size_mgrs - 1; q++)
				buf[q] = mgrs[q];
			delete[]mgrs;
			mgrs = buf;
		}
		else
		{
			mgrs = new manager[size_mgrs];
		}
			mgrs[size_mgrs - 1].ID[0] = aSalonID;
			mgrs[size_mgrs - 1].ID[1] = counter++;
			mgrs[size_mgrs - 1].name = aName;
			mgrs[size_mgrs - 1].surname = aSurname;
			mgrs[size_mgrs - 1].phone = aPhone;
			mgrs[size_mgrs - 1].birth.day = aDay;
			mgrs[size_mgrs - 1].birth.month = aMonth;
			mgrs[size_mgrs - 1].birth.year = aYear;
}
void stuff::fire_director(char aCh, int aEmployeeID) //metod fire dictor (for user interface) //1 ������ ��� �������� =1 ������ ��� ��������������, �� � ����������
{
	if (aCh == 'y' || aCh == 'Y')
	{
		int ch = 0;
		//cout << "1. Add new director\n2. Promote your menedger";
		//cin >> ch;
		if (ch == 2)
		{
			for (int i = 0; i < size_mgrs; i++)
			{
				/*cout << i + 1 << ". "
					<< mgrs[i].name
					<< mgrs[i].surname
					<< "\n";
			}
			cout << "write your choose: ";
			cin >> ch;*/
			}
			direk.name = mgrs[ch - 1].name;
			direk.surname = mgrs[ch - 1].surname;
			fire_manager(ch-1);
		}
		if (ch == 1)
		{
			direk.name = nullptr;
			direk.surname = nullptr;
			//cout <<"Director was deleted";
			set_director(direk);
		}
	}
}
director stuff::get_director() const
{
	return direk;
}

int stuff::get_size_mgrs() const
{
	return size_mgrs;
}
manager * stuff::get_managers() const
{
	return mgrs;
}
manager stuff::get_manager(int index) const //(contains exception)
{
	if (index >= size_mgrs)
		throw "A wrong index of manager";
	return mgrs[index];
}

manager stuff::get_manager_by_ID(int ID) const //(contains exception)
{
	int o = 0;
	int index = -1;
	for (int i = 0; i < size_mgrs; i++)
	{
		if (mgrs[i].get_employee_ID() != ID)
			o++;
		else
			index = i;
		if (o == size_mgrs)
		{
			throw "Erorr: Wrong ID";
		}
	}
	return mgrs[index];
}

void stuff::print_to_file(ofstream & F) const
{
	if (!F)
		throw "stuff: file was not opened, can't write";
	try
	{
		direk.print_to_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	int buf = size_mgrs;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing size of managers array
	buf = counter;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing counter of managers
	for (int i = 0; i < size_mgrs; i++)
	{
		try
		{
			mgrs[i].print_to_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
}

void stuff::read_from_file(ifstream & F)
{
	if (!F)
		throw "stuff: file was not opened, can't read";
	try
	{
		direk.read_from_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	F.read(reinterpret_cast<char*>(&size_mgrs), sizeof(int));	//reading size of managers array
	F.read(reinterpret_cast<char*>(&counter), sizeof(int));		//reading counter of managers
	
	delete[] mgrs;
	mgrs = new manager[size_mgrs];
	for (int i = 0; i < size_mgrs; i++)
	{
		try
		{
			mgrs[i].read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
}

void stuff::fire_manager(int aEmployeeID) //metod to fire manager (contains exception)
{
	int o = 0;
	for (int i = 0; i < size_mgrs; i++)
	{
		if (mgrs[i].get_employee_ID() != aEmployeeID) 
			o++;
		if (o == size_mgrs)
		{
			throw "Erorr: Wrong ID";
		}
	}
	size_mgrs--;
	manager *buf = new manager[size_mgrs];
	for (int q = 0, i=0; q < size_mgrs + 1; q++)
		if (mgrs[q].ID[1] != aEmployeeID)
		{
			buf[i] = mgrs[q];
			i++;
		}
	delete[] mgrs;
	mgrs = buf;
}

stuff stuff::operator=(stuff aStuff) //operator = overload
{
	direk = aStuff.direk;
	size_mgrs = aStuff.size_mgrs;
	delete[] mgrs;
	if (size_mgrs == 0)
		mgrs = nullptr;
	else
	{
		mgrs = new manager[size_mgrs];
		for (int i = 0; i < size_mgrs; i++)
			mgrs[i] = aStuff.mgrs[i];
	}
	return *this;
}