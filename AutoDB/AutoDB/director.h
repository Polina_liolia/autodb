#pragma once
#include "employee.h"
#include "manufacturerDB.h"
class salonDB;
class director : public employee
{
public:
	director();	//default constructor устанавливает type = dir;
	director(int aSalonID, int aemployeeID, position aType = dir);	//overloaded constructor устанавливает type = dir;
	director(int aSalonID, int aemployeeID, string aSurname, string aName, short aDay = 0, short aMonth = 0, short aYear = 0, string aPhone = "", position aType = dir, int aPasword = 1111);	//overloaded constructor устанавливает type = dir;
	director(const director &adirector);//copy constructor принудительно устанавливает type = dir;

public:
	void synchronize_cars(manufacturerDB &aFactory, salonDB *aSalonDB);
	void synchronize_test_autos(manufacturerDB &aFactory, salonDB *aSalonDB);
	void synchronize_clients_and_inquiries(manufacturerDB &aFactory, salonDB *aSalonDB);
	void synchronize_all(manufacturerDB &aFactory, salonDB *aSalonDB);
};

