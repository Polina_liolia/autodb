//#include <string> 
//using std::string;
#include "DBMS.h"
#include "infs.h"

#include <iostream>
using std::cout;
using std::endl;
#define a

//global outstream overload for Car
ostream& operator<<(ostream &o, Car &aCar)
{
	o << "ID: " << aCar.ID << "\n";
	o << "model: " << aCar.model;
	return o; // �������� ������ ���
}

// ���������� ��������� ������
// ��������� ������ �� ��������� ������ date � ostream
// ���������� ��������� ������ ostream
ostream& operator << (ostream& os, date &aDate)
{
	os << aDate.day << "." << aDate.month << "." << aDate.year << "\n";
	return os;
}

//global outstream overload for employee
ostream& operator<<(ostream& os, const employee& dt)
{
	os << "ID: " << dt.ID[0] << "-" << dt.ID[1] << "\n";
	os << "Name: " << dt.name;
	os << "Surname: " << dt.surname;
	os << "Phone: " << dt.phone;
	os << "position: ";
	if (dt.type == mgr)
		os << "manager" << "\n";
	else
		os << "director" << "\n";
	return os;
}

//������� ������� � ����� ��������� ������ Inquiry
//��������� ��������� ������� ostream � inquiry
//���������� �������� ������ ostream
ostream & operator<<(ostream & os, inquiry &aInquiry)
{
	os << "ID Inquiry ";
	for (int i = 0; i < 4; i++)
	{
		os << aInquiry.ID[i] << " ";
	}
	os << "\n";
	os << "Date" << aInquiry.date_added;
	switch (aInquiry.status)
	{
	case 0:
		os << "added" << "\n";
		break;
	case 1:
		os << "changed" << "\n";
		break;
	case 2:
		os << "synchronyzed" << "\n";
		break;
	case 3:
		os << "accepted" << "\n";
		break;
	case 4:
		os << "is_moving" << "\n";
		break;
	case 5:
		os << "rejected" << "\n";
		break;
	}
	if (aInquiry.options_sz != 0)
	{
		for (int i = 0; i < aInquiry.options_sz; i++)
		{
			os << aInquiry.additional_options[i];
		}
		os << "\n";
	}
	else
	{
		os << "No additional options" << "\n";
	}
	return os;
}


//friend overload of cout for client
ostream& operator<<(ostream & os, client &aclient)
{
	os << "ID client: " << aclient.ID[0] << "-" << aclient.ID[1] << "\n";
	os << "status: ";
	switch (aclient.status)
	{
	case 0: os << "added" << "\n";	break;
	case 1: os << "changed" << "\n"; break;
	case 2: os << "synchronyzed" << "\n"; break;
	}
	os << aclient.name;
	os << aclient.surname;
	os << aclient.phone;
	os << "Number of inquries: " << aclient.list_sz << "\n";
	for (int i = 0; i < aclient.list_sz; i++)
		os << aclient.inquirys_list[i] << "\n";
	return os;
}

//������������� ����������� ������ � ����� manufacturerDB
ostream & operator<<(ostream & os, manufacturerDB &aMDB)
{
	os << "Cars in catalog: " << aMDB.size_catalog << "\n";
	for (int i = 0; i < aMDB.size_catalog; i++)
	{
		os << aMDB.catalog[i];
	}
	os << "\n";
	os << "Cars in autopark: " << aMDB.size_autopark << "\n";
	for (int i = 0; i < aMDB.size_autopark; i++)
	{
		os << aMDB.autopark[i];
	}
	os << "\n";
	os << "Number of clients: " << aMDB.size_clients << "\n";
	for (int i = 0; i < aMDB.size_clients; i++)
	{
		os << aMDB.clients_list[i];
	}
	os << "\n";
	os << "Number of salons: " << aMDB.size_salons << "\n";
	for (int i = 0; i < aMDB.size_salons; i++)
	{
		os << aMDB.all_salons[i];
	}
	os << "\n";
	return os;
}

//The overloaded operator to display the instance property class salon
ostream & operator << (ostream& os, const salon &aSalon)
{
	os << "ID - " << aSalon.ID << "\n";
	os << "Name - " << aSalon.name;
	os << "City - " << aSalon.city;
	os << "Address - " << aSalon.address;
	os << "Phone - " << aSalon.phone << "\n";
	return os;
}

//global overload of outstream for test_auto
ostream& operator<<(ostream &os, test_auto &aCar)// �������� ������� ���
{
	os << "ID Test Car ";
	for (int i = 0; i < 2; i++)
	{
		os << aCar.ID[i] << " ";
	}
	os << aCar.model;
	return os;
}

//global overload of outstream for stuff
ostream& operator<< (ostream& os, stuff &aStuff)
{
	os << "Personnel of salon:\n";
	os << "Director:\n" << aStuff.direk;
	os << "Managers:\n";
	for (int i = 0; i < aStuff.size_mgrs; i++)
		os << i + 1 << ". " << aStuff.mgrs[i];
	return os;
}

//global overload of outstream for salonDB
ostream& operator << (ostream& os, salonDB aSalonDB)
{
	os << "Salon ID: " << aSalonDB.salon_data.get_ID() << "\n";
	os << "Salon name: " << aSalonDB.salon_data.get_name();
	os << "Salon city: " << aSalonDB.salon_data.get_city();
	os << "Salon address: " << aSalonDB.salon_data.get_address();
	os << "Salon phone: " << aSalonDB.salon_data.get_phone();
	os << aSalonDB.salon_personnel;
	os << "\n";
	os << "Autopark: \n";
	for (int i = 0; i < aSalonDB.size_autopark; i++)
		os << aSalonDB.autopark[i].ID[1] << aSalonDB.autopark[i];
	os << "\n";
	os << "Clients: \n";
	for (int i = 0; i < aSalonDB.size_clients; i++)
		os << aSalonDB.clients_list[i].ID[1] << aSalonDB.clients_list[i];
	os << "\n";
	return os; //�������� ������ ���
}

//global overload of outstream for DBMS
ostream& operator<<(ostream& os, DBMS& db)
{
	os << "Manufacturer:\n" << db.factory;
	os << "Salons:\n";
	for (int i = 0; i < db.size_salons; i++)
		os << db.all_salonsDB[i];
	return os;
}

void main()
{
	

	DBMS Mersedes;
	
	//void set_factory(Car *aCatalog, int aSize_catalog);	//������� �� ������������� (��������� ������ manufacturerDB)
	cout << "\nAfter setting a new factory (with new catalog):\n";
	Car *catalog = new Car[2];
	catalog[0].set_ID(1);
	catalog[0].set_model("Model1_new");
	catalog[1].set_ID(2);
	catalog[1].set_model("Model2_new");
	Mersedes.set_factory(catalog, 2);
	cout << Mersedes;

#ifdef a
	system("pause");
#endif
	system("cls");


	Mersedes.add_car_to_catalog("A-Class");
	Mersedes.add_car_to_catalog("B-Class");
	Mersedes.add_car_to_catalog("C-Class");
	cout << "Trying to remove from catalog a car with wrong ID:\n";
	try
	{
		Mersedes.remove_car_from_catalog(4);	//with a wrong car ID to remove
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	Mersedes.add_car_to_catalog("CLA");
	Mersedes.add_car_to_catalog("CLS");
	try
	{
		Mersedes.remove_car_from_catalog(4);	//with a correct car ID to remove
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	Mersedes.add_car_to_catalog("GL-Class");
	Mersedes.add_car_to_catalog("Citan Kombi");
	try
	{
		Mersedes.change_catalog_item(2, "B-Class Electric Drive");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}


	//void DBMS::change_catalog_item(int aID, string aModel_new) //�������� ������������ ���������� � ��������
	//������� �������������� ID
	cout << "Trying to change a car in catalog with wrong ID:\n";
	try
	{
		Mersedes.change_catalog_item(31, "B-Class Electric Drive");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "\nAfter adding new cars to catalog and removing car with ID 4:\n";
	cout << Mersedes;

#ifdef a
	system("pause");
#endif
	system("cls");

	//void add_salon(string salon_name, string salon_city, string director_surname, string director_name, string salon_address = nullptr, string salon_phone = nullptr);
	//Creating two new salons:
	Mersedes.add_salon("Salon1", "Kharkiv", "Ivanov", "Ivan", "Plechanovskaya str, 25", "057 716-85-93");
	Mersedes.add_salon("Salon2", "Kyiv", "Levchenko", "Leonid", "Kreshatik str.,1", "044 369-75-22");
	Mersedes.add_salon("Salon3", "Dnepr", "Kuzin", "Petr", "Cehova str.,15", "054 389-75-22");
	try
	{
		Mersedes.get_salon(1).add_test_auto(2);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).add_test_auto(3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try 
	{ 
		Mersedes.get_salon(2).add_test_auto(2); 
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(3).add_test_auto(7);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
		
	cout << "\nTrying to add the same car twice to one salon:\n";
	try
	{
		Mersedes.get_salon(3).add_test_auto(7);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "\nTrying to add test-auto with wrong ID to salon:\n";
	try
	{
		Mersedes.get_salon(3).add_test_auto(88);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "\nTrying to remove the test-auto with wrong ID from salon's autopark:\n";
	try
	{
		Mersedes.get_salon(2).remove_test_auto(777);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "\nTrying to remove the test-auto from unexisting salon's autopark:\n";
	try
	{
		Mersedes.get_salon(555).remove_test_auto(7);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "Removing the test-auto ID 3 from salon's ID 2 autopark:\n";
	try
	{	
		Mersedes.get_salon(2).remove_test_auto(3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nTrying to change the test-auto with wrong ID from salon's autopark:\n";
	try
	{
		Mersedes.get_salon(2).change_test_auto(6, 777);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).change_test_auto(3, 777);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "\nTrying to change the test-auto from unexisting salon's autopark:\n";
	try
	{
		Mersedes.get_salon(555).change_test_auto(7, 2);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nTrying to add client to salon with wrong ID:\n";
	try
	{
		Mersedes.get_salon(8).add_client("Kolobov", "Andrey", "050-952-57-71", 1);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nChanging the test-auto (ID-7) from salon's (ID-3) autopark:\n";
	try
	{
		Mersedes.get_salon(3).change_test_auto(7, 8);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to remove salon with wrong ID:\n";
	try
	{
		Mersedes.remove_salon(54);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.remove_salon(1);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	Mersedes.add_salon("Salon4", "Merefa", "Kozlov", "Ivan", "Mira str, 22", "057 345-85-77");
	
	try
	{
		Mersedes.change_salon_name(2, "Salon2_new");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.change_salon_city(2, "Kukuevka");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.change_salon_address(2, "Zeletnaya str., 1");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.change_salon_phone(2, "222-33-22");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "Trying to change data of unexisting salon (with ID 325):\n";
	try
	{
		Mersedes.change_salon_name(325, "Unexisting");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nDBMS after one salon remove and one more added;\nData of salon2 changed:\n" << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to set another director in unexisting salon (with ID 125):\n";
	try
	{
		Mersedes.get_salon(125).set_director("Litvinov", "Andrey");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "Setting another director in Salon2:\n";
	try
	{
		Mersedes.get_salon(2).set_director("Litvinov", "Andrey", 5, 12, 1966, "068-932-74-55");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to add manager to unexisting salon (with ID 125):\n";
	try
	{
		Mersedes.get_salon(125).add_manager("Vasilev", "Vasiliy");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Adding a managers to salon2 and salon3:\n";
	try
	{
		Mersedes.get_salon(2).add_manager("Vasilev", "Vasiliy");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).add_manager("Illyin", "Sergey");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(3).add_manager("Kozlov", "Aleksey");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(3).add_manager("Mironov", "Artem");
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to fire manager from unexisting salon (with ID 125):\n";
	try
	{
		Mersedes.get_salon(125).fire_manager(1);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "Trying to fire unexisting manager from salon2 (with ID 34):\n";
	try
	{
		Mersedes.get_salon(2).fire_manager(34);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Firing a managers wirh ID 2 from salon2:\n";
	try
	{
		Mersedes.get_salon(2).fire_manager(2);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to get some information about unexisting salon(785):\n";
	
	try
	{
		cout << "Director:\n" << Mersedes.get_salon(785).get_salon_director();
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "Managers:\n";
	try
	{
		for (int i = 0; i < Mersedes.get_salon(785).get_managers_amount(); i++)
			cout << Mersedes.get_salon(785).get_manager_salon(i);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "\nGetting information about second salon:\n";
	try
	{
		cout << "Director:\n" << Mersedes.get_salon(2).get_salon_director();
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		cout << "Managers:\n";
		for (int i = 0; i < Mersedes.get_salon(2).get_managers_amount(); i++)
			cout << Mersedes.get_salon(2).get_manager_salon(i);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to add client to salon using unexicsting manager's accaunt (with ID 76):\n";
	try
	{
		Mersedes.get_salon(2).add_client("Chernov", "Sergey", "067-932-54-78", 76);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "Trying to add client to unexisting salon (with ID 15):\n";
	try
	{
		Mersedes.get_salon(15).add_client("Chernov", "Sergey", "067-932-54-78", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nAdding clients to salons:\n";
	try
	{
		Mersedes.get_salon(2).add_client("Chernov", "Sergey", "067-932-54-78", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).add_client("Lubimov", "Valentin", "065-932-54-71", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(3).add_client("Belov", "Eugen", "068-932-54-72", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(3).add_client("Kolobov", "Andrey", "050-952-57-71", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to change data of unexisting client:\n";
	try
	{
		Mersedes.get_salon(2).edit_clients_name(55, "Petr", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_surname(55, "Petrov", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_phone(55, "+3(8066)-985-66-36", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_status(55, changed, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "\nTrying to change data of unexisting salon's client:\n";
	try
	{
		Mersedes.get_salon(15).edit_clients_name(1, "Petr", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(15).edit_clients_surname(1, "Petrov", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(15).edit_clients_phone(1, "+3(8066)-985-66-36", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(15).edit_clients_status(1, changed, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nTrying to change data using unexicsting manager's accaunt (with ID 76):\n";
	try
	{
		Mersedes.get_salon(2).edit_clients_name(1, "Petr", 76);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_surname(1, "Petrov", 76);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_phone(1, "+3(8066)-985-66-36", 76);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_status(1, changed, 76);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nChanging data of salon's (ID 2) client (ID 1):\n";
	try
	{
		Mersedes.get_salon(2).edit_clients_name(1, "Petr", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_surname(1, "Petrov", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_phone(1, "+3(8066)-985-66-36", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).edit_clients_status(1, changed, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to remove unexisting client:\n";
	try
	{
		Mersedes.get_salon(2).remove_client(36, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to remove unexisting salon's client:\n";
	try
	{
		Mersedes.get_salon(17).remove_client(2, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to remove client using unexicsting manager's accaunt (with ID 76):\n";
	try
	{
		Mersedes.get_salon(2).remove_client(2, 76);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "\nRemoving salon's (ID 2) client (ID 2):\n";
	try
	{
		Mersedes.get_salon(2).remove_client(2, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to add inquiry of existing car (already in autopark):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry(1, 2, 3, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to add inquiry of unexisting client (ID 52):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry(52, 7, 3, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to add inquiry of unexisting salon's client (ID 15):\n";
	try
	{
		Mersedes.get_salon(15).add_inquiry(1, 2, 3, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to add inquiry on unexisting car (ID 255):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry(1, 255, 3, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to add inquiry using unexisting manager's accaunt (ID 54):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry(1, 255, 54, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "\nAdding inquiries of salon's (ID 2) client (ID 1):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry(1, 7, 3, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).add_inquiry(1, 3, 3, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).add_inquiry(1, 5, 3, 25, 12, 2016);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to remove inquiry of unexisting client (ID 52):\n";
	try
	{
		Mersedes.get_salon(2).remove_inquiry(52, 2, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to remove inquiry of unexisting salon's client (ID 15):\n";
	try
	{
		Mersedes.get_salon(15).remove_inquiry(1, 2, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to remove inquiry using unexisting manager's accaunt (ID 54):\n";
	try
	{
		Mersedes.get_salon(2).remove_inquiry(1, 2, 54);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nRemoving inquirie(car ID 3) of salon's (ID 2) client (ID 1):\n";
	try
	{
		Mersedes.get_salon(2).remove_inquiry(1, 3, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to change inquiry of unexisting client (ID 52):\n";
	try
	{
		Mersedes.get_salon(2).change_inquiry(52, 7, 2, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to change inquiry of unexisting salon's client (ID 15):\n";
	try
	{
		Mersedes.get_salon(15).change_inquiry(1, 7, 2, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to change inquiry using unexisting manager's accaunt (ID 54):\n";
	try
	{
		Mersedes.get_salon(2).change_inquiry(1, 7, 2, 54);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nChanging inquiry(car ID 7) of salon's (ID 2) client (ID 1):\n";
	try
	{
		Mersedes.get_salon(2).change_inquiry(1, 7, 6, 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to add inquiry option to unexisting client (ID 52):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry_option(52, 2, "color: red", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to add inquiry option to unexisting salon's client (ID 15):\n";
	try
	{
		Mersedes.get_salon(15).add_inquiry_option(1, 2, "color: red", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to add inquiry option using unexisting manager's accaunt (ID 54):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry_option(1, 2, "color: red", 54);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	cout << "\nAdding inquiry options(car ID 6) to salon's (ID 2) client (ID 1):\n";
	try
	{
		Mersedes.get_salon(2).add_inquiry_option(1, 6, "color: red", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).add_inquiry_option(1, 6, "engine 3.0l", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.get_salon(2).add_inquiry_option(1, 6, "automatic transmission", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");

	cout << "Trying to remove inquiry option to unexisting client (ID 52):\n";
	try
	{
		Mersedes.get_salon(2).remove_inquiry_option(52, 2, "color: red", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to remove inquiry option to unexisting salon's client (ID 15):\n";
	try
	{
		Mersedes.get_salon(15).remove_inquiry_option(1, 2, "color: red", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << "Trying to remove inquiry option using unexisting manager's accaunt (ID 54):\n";
	try
	{
		Mersedes.get_salon(2).remove_inquiry_option(1, 2, "color: red", 54);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	
	cout << "\nRemoving inquiry option \"color: red\" (car ID 6) of salon's (ID 2) client (ID 1):\n";
	try
	{
		Mersedes.get_salon(2).remove_inquiry_option(1, 6, "color: red", 3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	cout << Mersedes;
#ifdef a
	system("pause");
#endif
	system("cls");
	
	cout << "Synchronization check. First changing car catalog (to test cars synchronisation). Adding one more car (SYNCH-TEST CAR)\n";
	cout << "Then synchronizing all data for salon(ID 2).\n";
	Mersedes.add_car_to_catalog("SYNCH-TEST CAR");
	try
	{
		Mersedes.synchronize_all(2);	
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}
	try
	{
		Mersedes.synchronize_all(3);
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}	
	cout << Mersedes;
}