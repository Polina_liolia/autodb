#include "employee.h"
employee::employee()
{
	ID[0] = 0;
	ID[1] = 0;
	birth.day = 0;
	birth.month = 0;
	birth.year = 0;
	type = mgr;
	password = 1111;
}
employee::employee(string aSurname, string aName, int IDsalon, int IDemployee, short aDay, short aMonth, short aYear, string aPhone, position aType)
{
	ID[0] = IDsalon;
	ID[1] = IDemployee;
	name = aName;
	surname = aSurname;
	phone = aPhone;
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
	type = aType;
	password = 1111;
}
employee::employee(const employee &a)
{
	ID[0] = a.ID[0];
	ID[1] = a.ID[1];
	name = a.name;
	surname = a.surname;
	phone = a.phone;
	birth.day = a.birth.day;
	birth.month = a.birth.month;
	birth.year = a.birth.year;
	type = a.type;
	password = a.password;
}
void employee::set_name(string aName)
{
	name = aName;
}
void employee::set_surname(string aSurname)
{
	surname = aSurname;
}
void employee::set_phone(string aPhone)
{
	phone = aPhone;
}
void employee::set_ID_employee(int IDemployee)
{
	ID[1] = IDemployee;

}
void employee::set_ID_salon(int IDsalon)
{
	ID[0] = IDsalon;
}
void employee::set_position(position aType)
{
	type = aType;
}
string employee::get_name() const
{
	return name;
}
string employee::get_surname() const
{
	return surname;
}
string employee::get_phone() const
{
	return phone;
}
void employee::set_birth_date(short aDay, short aMonth, short aYear)
{
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
}

int employee::get_password()const
{
	return password;
}
int employee::get_salon_ID()
{
	return ID[0];
}
int employee::get_employee_ID()
{
	return ID[1];
}
position employee::get_position()const
{
	return type;
}

void employee::set_password(int aPassword)
{
	password = aPassword;
}

void employee::print_to_file(ofstream & F) const
{
	if (!F)
		throw "employee: file was not opened, can't write";
	int buf = ID[0];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing salon ID
	buf = ID[1];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing employee ID

	string tmp = name;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//name

	tmp = surname;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//surname

	tmp = phone;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//phone

	try
	{
		birth.print_to_file(F);								//writing date of birth
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	
	buf = sizeof(type);
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	position p = type;
	F.write(reinterpret_cast<char*>(&p), buf);		//position type writing
}

void employee::read_from_file(ifstream & F)
{
	if (!F)
		throw "employee: file was not opened, can't read";
	F.read(reinterpret_cast<char*>(&ID[0]), sizeof(int));	//reading salon ID
	F.read(reinterpret_cast<char*>(&ID[1]), sizeof(int));	//reading employee ID

	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	name.resize(buf);
	F.read(reinterpret_cast<char*>(&name[0]), buf);			//name

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	surname.resize(buf);
	F.read(reinterpret_cast<char*>(&surname[0]), buf);		//surname

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	phone.resize(buf);
	F.read(reinterpret_cast<char*>(&phone[0]), buf);		//phone

	try
	{
		birth.read_from_file(F);							//reading date of birth
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	F.read(reinterpret_cast<char*>(&type), buf);		//position type reading
}
