#pragma once
#include "stuff.h"
#include "salon.h"

class salonDB : public DataBase
{
	salon salon_data;
	int clients_counter;	//to count all clients, added to list (to avoid repeating of clients' IDs)
	stuff salon_personnel;
	
	friend class manager;
	friend class DBMS;
public:
	salonDB();
	salonDB(Car *aCatalog, int aSize_catalog, int salon_ID, string salon_name, string salon_city, string director_surname, string director_name, string salon_address, string salon_phone = "");
	salonDB(const salonDB &aSalonDB);
	~salonDB();

public:
	void set_director(director aDir);
	void set_director(string aSurname, string aName, short aDay = 0, short aMonth = 0, short aYear = 0, string aPhone = "");
	void add_manager(manager aMgr);
	void add_manager(string aSurname, string aName, short aDay = 0, short aMonth = 0, short aYear = 0, string aPhone = "");
	void fire_manager(int aEmployeeID);
	salon &get_salon_data();
	stuff & get_personnel();
	director get_salon_director()const;
	int get_managers_amount()const;
	manager *get_managers()const;
	manager get_manager_salon(int index)const;
	bool check_manager(int ID);
	void edit_clients_surname(int clientID, string aSurname, int managerID);
	void edit_clients_name(int clientID, string aName, int managerID);
	void edit_clients_phone(int clientID, string aPhone, int managerID);
	void edit_clients_status(int clientID, client_status aStatus, int managerID);
	client &get_client(int ID);
	int get_clients_counter()const;
	bool check_carID(int car_ID);
	bool check_test_autoID(int car_ID);
	void add_client(string aSurname, string aName, string aPhone, int managerID);
	void remove_client(int clientID, int managerID);
	void add_test_auto(int car_ID);
	void remove_test_auto(int test_autoID);
	void change_test_auto(int test_autoID_old, int test_autoID_new);
	void add_inquiry(int clientID, int carID, int managerID, short aDay, short aMonth, short aYear);
	void remove_inquiry(int clientID, int carID, int managerID);
	void change_inquiry(int clientID, int carID_old, int carID_new, int managerID);
	void add_inquiry_option(int clientID, int carID, string aOption, int managerID);
	void remove_inquiry_option(int clientID, int carID, string aOption, int managerID);
	salonDB operator=(salonDB aSDB);
	
	friend ostream& operator << (ostream& os, salonDB aSalonDB);
	friend class DBMS;
	friend class director;

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
};