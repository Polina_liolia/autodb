#pragma once
#include <iostream>

using std::ostream;
using std::istream;

class string
{
private:
	int sz;
	char* str;

public:
	string();									
	string(char *aStr);							
	string(const string &astring);				
	~string();

	

public:
	void set_string(char *aStr);
	char *get_string()const;
	int get_stringlen()const;
	string operator=(const string &astring);	
	string operator+=(const string &astring);	
	string operator+=(char a);	
	string operator*=(const string &astring);	
	string operator*=(char *aStr);
	string operator+(const string &astring);	
	string operator+(char *aStr);				
	string operator*(const string &astring);	
	string operator*(char *aStr);
	string operator/(const string &astring);	
	string operator/(char *aStr);
	string operator/=(const string &astring);	
	string operator/=(char *aStr);
	bool operator==(const string &astring);		
	bool operator==(char *aStr);				
	bool operator!=(const string &astring);		
	bool operator!=(char *aStr);				
	bool operator>(const string &astring);		
	bool operator>(char *aStr);					
	bool operator>=(const string &astring);		
	bool operator>=(char *aStr);
	bool operator<(const string &astring);			
	bool operator<(char *aStr);	
	bool operator<=(const string &astring);		
	bool operator<=(char *aStr);	
	void print();
	char& operator[](int aIndex);
	operator int ()const;
	operator double()const;
	
	friend ostream & operator << (ostream& os, const string &astring);
	friend istream & operator >> (istream& os, string &astring);
};
