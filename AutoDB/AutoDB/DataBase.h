#pragma once
#include "Car.h"
#include "test_auto.h"
#include "client.h"

/*The base class (parent) class manufacturerDB & salon DB*/
class DataBase 
{
protected:
	int size_catalog;		//size of the catalog
	Car *catalog;			//all cars available for order
	int size_autopark;		//size of a catalog of all cars that are in the salon
	test_auto *autopark;	//all cars that are in the salon
	int size_clients;		//size of a catalog of all clients salon
	client *clients_list;	//all clients salon

public:
	DataBase();							//default constructor
	DataBase(Car *aCatalog, int aSize_catalog);		//overloaded constructor
	DataBase(const DataBase &aDB);		//copy constructor
	~DataBase();						//destructor

public:
	int get_size_catalog() const;		//Get size of the catalog
	Car *get_car_catalog() const;		//Get a catalog of all cars available for order
	int get_size_autopark() const;		//Get size of a catalog of all cars that are in the salon 
	test_auto *get_autopark() const;	//Get a catalog of all cars that are in the salon
	int get_size_clients() const;		//Get size of a catalog of all clients salon
	client *get_clients_list();		//Get a catalog of all clients salon
	int get_size_inquiries()const;
	DataBase operator=(const DataBase &aDB);

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);

	int get_car_index(int car_ID);

	int get_client_index(int clientID);

	int get_test_auto_index(int ID);

	int get_clients_inquiry_index(int clientID, int carID);

	void remove_inquiry(int clientID, int carID);

	bool check_client_ID(int clientID);

	bool check_clients_inquiry(int clientID, int carID);

	void remove_client(int clientID);

};