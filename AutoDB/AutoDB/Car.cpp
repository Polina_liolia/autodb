#include "Car.h"

Car::Car()		//default constructor
{
	ID = 0;
	model;
}
Car::Car(int aID, string aModel)//overloaded constructor
{
	ID = aID;
	model = aModel;
}
Car::Car(const Car &aCar)//copy constructor
{
	ID = aCar.ID;
	model = aCar.model;
}
Car::~Car()				//destructor
{

}
//set ��������� ���������� ����� ��� �������� ��� ������������ ��������*/
void Car::set_ID(int aID)				//to set or change ID of a car
{
	ID = aID;

}
void Car::set_model(string aModel)	//to set or change model
{
	model = aModel;
}
int Car::get_ID()const			
{
	return ID;
}
string Car::get_model()const
{
	return model;
}


void Car::print_to_file(ofstream & F) const
{
	if (!F)
		throw "Car: file was not opened, can't write";
	int buf = ID;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing ID
	string tmp = model;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//model
}


void Car::read_from_file(ifstream & F)
{
	if (!F)
		throw "Car: file was not opened, can't read";
	F.read(reinterpret_cast<char*>(&ID), sizeof(int));	//reading ID
	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	model.resize(buf);
	F.read(reinterpret_cast<char*>(&model[0]), buf);	//model
}
