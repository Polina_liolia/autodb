#pragma once
#include "DataBase.h"
#include "salon.h"

class manufacturerDB : public DataBase
{
	int size_salons;
	salon *all_salons;
	int password;

public:
	manufacturerDB();	// ����������� �� ��������� ����������
	manufacturerDB(Car *aCatalog, int aSize_catalog);	// ������������� ����������� ���������� 
	manufacturerDB(Car * aCatalog, int aSize_catalog, salon * aAll_salons, int aSize_salons);
	manufacturerDB(const manufacturerDB &aMDB);	// ����������� ����������� ����������
	virtual ~manufacturerDB();

public:
	int get_size_salons() const;	//�������� ���-�� �������
	salon *get_all_salons() const;		//�������� ������ �� �������

	void add_client(const client &aClient); //��������� ������� � ����
	void add_inquiry(int client_index, const inquiry aInquiry); //���������� ������ ������� � ����

	void add_inquiry(const inquiry aInquiry); //���������� ������ ������� �� ID�������

	void add_salon(const salon &aSalon); //��������� ����� � ������, ���������� � �� �������������
	int find_index_salon(int salonID); // ������� ������ ������ � �������
	
	void remove_salon_by_ID(int salonID);			//������� ����� � ��������� ID �� �������� �� �������������
	void remove_salon_by_index(int salon_index);		//������� ����� � ��������� �������� �� �������� �� �������������
	
	void change_salon_name(int salonID, string aName);	//������ �������� ������ � ��������� ID
	void change_salon_city(int salonID, string aCity);	//������ ����� ������ � ��������� ID
	void change_salon_address(int salonID, string aAddress);	//������ ����� ������ � ��������� ID
	void change_salon_phone(int salonID, string aPhone);	//������ ����� �������� ������ � ��������� ID
	
	int find_index_car_catolog(int carID); // ������� ������ ������ � �������
	void add_car(const Car &aCar);			//�������� ���������� � �������
	void add_car(int aID, string aModel);	//�������� ���������� � �������
	void remove_car_by_ID(int carID);		//������� �� �������� ���� � ��������� ID
	void remove_car_by_index (int car_index);	//������� �� �������� ���� � ��������� ��������
	void change_car(int carID, string aModel_new); //�������� ������������ ���������� � ��������

	/*�������� ������ ������������ ������ (��������: 
	accepted - ������ (���������) ��������������
	is_moving - � �������� ��������(���� �� ���� � �����)
	rejected - ��������(���������� ���������)):*/
	void change_inquiry_status(inquiry &aInquiry, inquiry_status aStatus); 

	inquiry *get_all_inquiries(); //���������� ������ ���� �������� ���� ��������
	int get_size_all_inquiries(); //���������� ������ ������� ���� �������� ���� ��������
	inquiry *get_all_salon_inquiries(int aSalonID); //���������� ������ ���� �������� �������� ���������� ������
	int get_size_all_salon_inquiries(int aSalonID);//����������  ������ ������� ���� �������� �������� ���������� ������
	inquiry *get_all_manager_inquiries(int aManagerID); //���������� ������ ���� �������� �������� ���������� ���������
	int get_size_all_manager_inquiries(int aManagerID); //���������� ������ ������� ���� �������� �������� ���������� ���������
	inquiry *get_all_client_inquiries(int aClientID); //���������� ������ ���� �������� ���������� �������
	int get_size_all_client_inquiries(int aClientID); //���������� ������ ������� ���� �������� ���������� �������
	inquiry *get_all_car_inquiries(int aCarID); //���������� ������ ���� �������� �� ��������� ����������
	int get_size_all_car_inquiries(int aCarID); //���������� ������ ���� �������� �� ��������� ����������
	inquiry get_inquiry(int* aInquiryID); //���������� ��������� ������ �������
	
	int get_password()const;
	void set_password(int pas);
	int exist_client_ID(int aClientID);
	manufacturerDB operator=(manufacturerDB to_copy);
	//���������� ������, ���� ������ � ����� ID ��������� � -1, ���� ���
	
	
	friend class DBMS; //�.�. ����� DBMS �������� � ������������ ����� ������
	friend ostream& operator << (ostream& os, manufacturerDB &aMDB);
	friend class director;

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);

	void remove_inquiry(int clientID, int carID);

};


