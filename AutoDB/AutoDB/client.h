#pragma once
#include<iostream>
#include <string> 
using std::string;
#include "inquiry.h"
#include<fstream>
using std::ios;
using std::ofstream;
using std::ifstream;


/*client status for synchronization*/
enum client_status { added, changed, synchronyzed };

/*this class will be used in manufacturerDB and salonDB,it stores information about client and inquiries*/
class client
{
	int ID[2];					//ID salon � ID client
	client_status status;		//client's status
	string name;
	string surname;
	string phone;
	int list_sz;				//number of inquiries for definite client
	inquiry *inquirys_list;		//client's array of inquiries
	int password;

	friend class manager;
	friend class DataBase;

public:
	client();//default constructor

	client(int aSalon_ID, int aClient_ID, string aName, string aSurname, string aPhone="");//overloaded constructor

	client(const client &aClient);//copy constructor

	~client();//destructor

	int get_salon_ID();//show and return salon ID

	int get_client_ID();//show and return client ID

	client_status get_status();//return client status

	int get_list_sz();//returns number of inquries for this client

	inquiry *get_inquiry_list()//returns the pointer to array of inquries
	{
		return inquirys_list;
	}
	string get_name();//return client name

	string get_surname();//return client surname

	string get_phone();//return client phone

	int get_password()const;	//returns client's password

					   //these methods set or change existing volumes
	void set_ID(int aSalon_ID, int aClient_ID);

	void set_salon_ID(int aSalon_ID);

	void set_client_ID(int aClient_ID);

	void set_client_data(string aSurname, string aName, string aPhone = "");

	void set_client_name(string aName);

	void set_client_surname(string aSurname);

	void set_client_phone(string aPhone);

	void set_client_status(client_status aStstus);
	void set_password(int aPassword);

	/*this method creates inquriy using set_methods of inquiry class,takes ID salon,ID car,ID manager
	,day,month,year,status of inquiry and option, returns exampel inquiry*/
	inquiry fill_in_iquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, int aDay, int aMonth, int aYear, inquiry_status aStatus, const string &aOption="");

	/*this method takes  aSalonID, aManagerID,  aClientID, aCarID, aInquiryID,  aDay,  aMonth,  aYear, inquiry_status , aOption,
	create new inquiry using overloaded constructor, and adds it to array*/
	void add_inquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, int aDay, int aMonth, int aYear);

	/*this method takes  exiting inquiry and adds it to array*/
	void add_inquiry(const inquiry &aInquiry);

	void delete_inquiry(int number_inquary);//deleating an inquiry by number in array+1 (fo users counting from 1)

	void delete_inquiry(inquiry &aInquiry);		//delete passed inquiry

	client operator= (const client &aClient);//fill in client with same data of passed client

	client operator+ (inquiry aInquairy);	//add inquiry to client

	client operator+=(inquiry aInquairy);	//add inquiry to client

	void add_option(inquiry &aInquiry, string aOption);	//add inquiry using methods of inquiry class

	bool operator== (const client &aClient)const;//compare two clients by ID and status

	void change_inquiry(inquiry &aInquiry, int car_ID);	//change car in inquriy

	void delete_option(inquiry &aInquiry, string aOption);//delete option from passed inquriy

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);

	friend class director;
	friend class salonDB;
	friend class manufacturerDB;
	friend ostream& operator << (ostream& os, salonDB aSalonDB);
	friend ostream& operator << (ostream& os, client &aInquiry);
};
