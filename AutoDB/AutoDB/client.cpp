#include "client.h"
client::client()//default constructor
{
	ID[0] = 0;
	ID[1] = 0;
	status = added;
	name;
	surname;
	phone;
	list_sz = 0;
	inquirys_list = nullptr;
	password = 1111;
}
client::client(int aSalon_ID, int aClient_ID, string aName, string aSurname, string aPhone)//overloaded constructor
{
	ID[0] = aSalon_ID;
	ID[1] = aClient_ID;
	status = added;
	name = aName;
	surname = aSurname;
	phone = aPhone;
	list_sz = 0;
	inquirys_list = nullptr;
	password = 1111;
}
client::client(const client &aClient)//copy constructor
{
	ID[0] = aClient.ID[0];
	ID[1] = aClient.ID[1];
	status = aClient.status;
	name = aClient.name;
	surname = aClient.surname;
	phone = aClient.phone;
	list_sz = aClient.list_sz;
	inquirys_list = new inquiry[aClient.list_sz];
	for (int i = 0; i < aClient.list_sz; i++)
	{
		inquirys_list[i] = aClient.inquirys_list[i];
	}
	password = aClient.password;
}
client::~client()//destructor
{
	delete[]inquirys_list;
}

int client::get_password()const
{
	return password;
}

int client::get_salon_ID()//show and return salon ID
{
	return  ID[0];
}
int client::get_client_ID()//show and return client ID
{
	return  ID[1];
}
client_status client::get_status()//return client status
{
	return status;
}
int client::get_list_sz()//returns number of inquries for this client
{
	return list_sz;
}

string client::get_name()//return client name
{
	return name;
}
string client::get_surname()//return client surname
{
	return surname;
}
string client::get_phone()//return client phone
{
	return phone;
}
//these methods set or change existing volumes
void client::set_ID(int aSalon_ID, int aClient_ID)
{
	if (status != added)
		status = changed;
	ID[0] = aSalon_ID;
	ID[1] = aClient_ID;
}

void client::set_salon_ID(int aSalon_ID)
{
	if (status != added)
		status = changed;
	ID[0] = aSalon_ID;
}
void client::set_client_ID(int aClient_ID)
{
	if (status != added)
		status = changed;
	ID[1] = aClient_ID;
}

void client::set_client_data(string aSurname, string aName, string aPhone)
{
	if (status != added)
		status = changed;
	name = aName;
	surname = aSurname;
	phone = aPhone;

}
void client::set_client_name(string aName)
{
	if (status != added)
		status = changed;
	name = aName;
}
void client::set_client_surname(string aSurname)
{
	if (status != added)
		status = changed;
	surname = aSurname;
}
void client::set_client_phone(string aPhone)
{
	if (status != added)
		status = changed;
	phone = aPhone;
}
void client::set_client_status(client_status aStstus)
{
	status = aStstus;
}

void client::set_password(int aPassword)
{
	password = aPassword;
}

/*this method creates inquriy using set_methods of inquiry class,takes ID salon,ID car,ID manager,
day,month,year,status of inquiry and option, returns exampel inquiry*/
inquiry client::fill_in_iquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, int aDay, int aMonth, int aYear, inquiry_status aStatus, const string &aOption)
{
	status = changed;
	inquiry x;
	x.set_salon_ID(aSalonID);
	x.set_client_ID(aClientID);
	x.set_manager_ID(aManagerID);
	x.set_car_ID(aCarID);
	x.set_data(aDay, aMonth, aYear);
	x.set_status(aStatus);
	x.add_option(aOption);
	return x;
}

/*this method takes  aSalonID, aManagerID,  aClientID, aCarID, aInquiryID,  aDay,  aMonth,  aYear, inquiry_status , aOption,
create new inquiry using overloaded constructor, and adds it to array*/
void client::add_inquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, int aDay, int aMonth, int aYear)
{
	status = changed;
	inquiry *inquirys_list_new = new inquiry[list_sz + 1];
	for (int i = 0; i < list_sz; i++)
	{
		inquirys_list_new[i] = inquirys_list[i];
	}
	inquirys_list_new[list_sz] = inquiry(aSalonID, aManagerID, aClientID, aCarID, aDay, aMonth, aYear);
	list_sz++;
	delete[]inquirys_list;
	inquirys_list = inquirys_list_new;

}

/*this method takes  existing inquiry and adds it to array*/
void client::add_inquiry(const inquiry &aInquiry)
{
	status = changed;
	inquiry *inquirys_list_new = new inquiry[list_sz + 1];
	for (int i = 0; i < list_sz; i++)
	{
		inquirys_list_new[i] = inquirys_list[i];
	}
	inquirys_list_new[list_sz] = aInquiry;
	inquirys_list_new[list_sz].set_status(inq_added);
	list_sz++;
	delete[]inquirys_list;
	inquirys_list = inquirys_list_new;
}

//deleating an inquiry by number in array+1 (fo users counting from 1); contains exception
void client::delete_inquiry(int number_inquary)
{
	status = changed;
	if ((number_inquary - 1) >= 0 && (number_inquary - 1) < list_sz)
	{
		list_sz--;
		if (list_sz == 0)
		{
			inquirys_list = nullptr;
		}
		else {
			inquiry *inquirys_list_new = new inquiry[list_sz];
			for (int i = 0; i < list_sz; i++)
			{
				if (i < number_inquary - 1)
				{
					inquirys_list_new[i] = inquirys_list[i];
				}
				else
				{
					inquirys_list_new[i] = inquirys_list[i + 1];
				}
			}
			delete[]inquirys_list;
			inquirys_list = inquirys_list_new;
		}
	}
	else
	{
		throw "wrong inquiry number";
	}
}

//delete passed inquiry; contains exception
void client::delete_inquiry(inquiry &aInquiry)
{
	status = changed;
	if (list_sz == 0)
		throw "Error number: no inquiries in the base";
	//checking, if there is such a inquiry in the list, using a flag-variable:
	bool flag = false;
	for (int i = 0; i <list_sz; i++)
		if (inquirys_list[i].ID[4] == aInquiry.ID[4])
			flag = true;
	//generating an exception if there is no such a client:
	if (!flag)
		throw "No inquiry with such ID";
	//if inquiry with such ID exists, deleting him from the list:
	list_sz--;
	if (list_sz == 0)
	{
		delete[] inquirys_list;
		inquirys_list = nullptr;
	}
	else
	{
		inquiry *tmp = new inquiry[list_sz];
		for (int i = 0, j = 0; i < list_sz + 1; i++)
			if (inquirys_list[i].ID[4] != aInquiry.ID[4])
			{
				tmp[j] = inquirys_list[i];
				j++;
			}
		delete[] inquirys_list;
		inquirys_list = tmp;
	}
}

//fill in client with same data of passed client
client client::operator= (const client &aClient)
{
	ID[0] = aClient.ID[0];
	ID[1] = aClient.ID[1];
	status = aClient.status;
	name = aClient.name;
	surname = aClient.surname;
	phone = aClient.phone;
	list_sz = aClient.list_sz;
	delete[] inquirys_list;
	inquirys_list = new inquiry[aClient.list_sz];
	for (int i = 0; i < aClient.list_sz; i++)
	{
		inquirys_list[i] = aClient.inquirys_list[i];
	}
	return *this;
}

//add inquiry to client
client client::operator+ (inquiry aInquairy)
{
	status = changed;
	aInquairy.set_status(inq_added);
	inquiry *inquirys_list_new = new inquiry[list_sz + 1];
	for (int i = 0; i < list_sz; i++)
	{
		inquirys_list_new[i] = inquirys_list[i];
	}
	inquirys_list_new[list_sz] = aInquairy;
	list_sz++;
	delete[]inquirys_list;
	inquirys_list = inquirys_list_new;
	return *this;
}

//add inquiry to client
client client::operator+=(inquiry aInquairy)
{
	status = changed;
	aInquairy.set_status(inq_added);
	inquiry *inquirys_list_new = new inquiry[list_sz + 1];
	for (int i = 0; i < list_sz; i++)
	{
		inquirys_list_new[i] = inquirys_list[i];
	}
	inquirys_list_new[list_sz] = aInquairy;
	list_sz++;
	delete[]inquirys_list;
	inquirys_list = inquirys_list_new;
	return *this;
}

//add inquiry using methods of inquiry class
void client::add_option(inquiry &aInquiry, string aOption)
{
	status = changed;
	aInquiry.add_option(aOption);
	aInquiry.set_status(inq_changed);
}

//compare two clients by ID
bool client::operator== (const client &aClient)const
{
	return ((ID[0] == aClient.ID[0] &&
		ID[1] == aClient.ID[1]));
}

//change car in inquriy
void client::change_inquiry(inquiry &aInquiry, int car_ID)
{
	status = changed;
	aInquiry.set_car_ID(car_ID);
	aInquiry.set_status(inq_changed);
}

//delete option from passed inquriy
void client::delete_option(inquiry &aInquiry, string aOption)
{
	status = changed;
	aInquiry.remove_option(aOption);
	aInquiry.set_status(inq_changed);
}

void client::print_to_file(ofstream & F) const
{
	if (!F)
		throw "client: file was not opened, can't write";
	int buf = ID[0];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing salon ID
	buf = ID[1];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing client ID

	buf = sizeof(status);
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	client_status st = status;
	F.write(reinterpret_cast<char*>(&st), buf);			//client_status writing

	string tmp = name;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//name

	tmp = surname;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//surname

	tmp = phone;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//phone

	buf = list_sz;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing size of inquiries list

	for (int i = 0; i < list_sz; i++)
	{
		try
		{
			inquirys_list[i].print_to_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}

	buf = password;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing client's password
}

void client::read_from_file(ifstream & F)
{
	if (!F)
		throw "client: file was not opened, can't read";
	F.read(reinterpret_cast<char*>(&ID[0]), sizeof(int));	//reading salon ID
	F.read(reinterpret_cast<char*>(&ID[1]), sizeof(int));	//reading client ID

	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	F.read(reinterpret_cast<char*>(&status), buf);			//client_status reading

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	name.resize(buf);
	F.read(reinterpret_cast<char*>(&name[0]), buf);			//name

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	surname.resize(buf);
	F.read(reinterpret_cast<char*>(&surname[0]), buf);		//surname

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	phone.resize(buf);
	F.read(reinterpret_cast<char*>(&phone[0]), buf);		//phone

	F.read(reinterpret_cast<char*>(&list_sz), sizeof(int));	//reading size of inquiries list
	delete[]inquirys_list;
	inquirys_list = new inquiry[list_sz];
	for (int i = 0; i < list_sz; i++)
	{
		try
		{
			inquirys_list[i].read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}

	F.read(reinterpret_cast<char*>(&password), sizeof(int));	//reading client's password
}
