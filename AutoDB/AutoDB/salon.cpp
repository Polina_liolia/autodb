#include "salon.h"

salon::salon(){ //default constructor
	ID = 0;
	name;
	city;
	address;
	phone;
}

salon::salon(int aID, string aName, string aCity, string aAddress, string aPhone){ //overloaded constructor
	ID = aID;
	name = aName;
	city = aCity;
	address = aAddress;
	phone = aPhone;
}

salon::salon(const salon &aSalon){ //copy constructor
	ID = aSalon.ID;
	name = aSalon.name;
	city = aSalon.city;
	address = aSalon.address;
	phone = aSalon.phone;
}

salon::~salon(){ //destructor
}

void salon::set_ID(int aID){ //The method allows to set the ID
	ID = aID;
}

void salon::set_name(string aName){ //The method allows to set the name
	name = aName;
}

void salon::set_city(string aCity){ //The method allows to set the city
	city = aCity;
}

void salon::set_address(string aAddress){ //The method allows to set the address
	address = aAddress;
}

void salon::set_phone(string aPhone){ //The method allows to set the phone
	phone = aPhone;
}

int salon::get_ID()const{ //The method allows to get the ID
	return ID;
}

string salon::get_name()const{ //The method allows to get the name
	return name;
}

string salon::get_city()const{ //The method allows to get the city
	return city;
}

string salon::get_address()const{ //The method allows to get the address
	return address;
}

string salon::get_phone()const{ //The method allows to get the phone
	return phone;
}

salon salon::operator= (const salon &aSalon){
	ID = aSalon.ID;
	name = aSalon.name;
	city = aSalon.city;
	address = aSalon.address;
	phone = aSalon.phone;
	return *this;
}

void salon::print_to_file(ofstream & F) const
{
	if (!F)
		throw "salon: file was not opened, can't write";
	int buf = ID;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing salon ID

	string tmp = name;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//name

	tmp = city;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//city

	tmp = address;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//address

	tmp = phone;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//phone
}
 
void salon::read_from_file(ifstream & F)
{
	if (!F)
		throw "salon: file was not opened, can't read";

	F.read(reinterpret_cast<char*>(&ID), sizeof(int));		//reading salon ID

	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	name.resize(buf);
	F.read(reinterpret_cast<char*>(&name[0]), buf);			//name

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	city.resize(buf);
	F.read(reinterpret_cast<char*>(&city[0]), buf);			//city

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	address.resize(buf);
	F.read(reinterpret_cast<char*>(&address[0]), buf);		//address

	F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
	phone.resize(buf);
	F.read(reinterpret_cast<char*>(&phone[0]), buf);		//phone
}

