#include "director.h"
#include "salonDB.h"
director::director() : employee::employee()// default constructor
{
	type = dir;
}

director::director(int aSalonID, int aemployeeID, position aType)//overload constructor
{
	ID[0] = aSalonID;
	ID[1] = aemployeeID;
	type = aType;
}

director::director(int aSalonID, int aemployeeID, string aSurname, string aName, short aDay, short aMonth, short aYear, string aPhone, position aType, int aPasword)//overload constructor
{
	ID[0] = aSalonID;
	ID[1] = aemployeeID;
	surname = aSurname;
	name = aName; 
	type = aType;
	phone = aPhone;
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
	password = aPasword;
}
director::director(const director &adirector)//copy constructor
{
	ID[0] = adirector.ID[0];
	ID[1] = adirector.ID[1];
	surname = adirector.surname;
	name = adirector.name;
	type = adirector.type;
	phone = adirector.phone;
	birth = adirector.birth;
	password = adirector.password;

}

//���� 
void director::synchronize_cars(manufacturerDB &aFactory, salonDB *aSalonDB) //contains exception
{
	int sz = aSalonDB->get_size_catalog();//size of catalog from salonDB
	int sz_f = aFactory.get_size_catalog();//size of catalog from  manufacturerDB
	Car *ptr = aSalonDB->get_car_catalog();//catalog from  salonDB
	Car *ptr_f = aFactory.get_car_catalog();//catalog from manufacturerDB
	if (ptr_f == nullptr)
		throw "No valid catalog from manufacturer"; 
	delete[] ptr;//delete old catalog in salon
	ptr = new Car[sz_f];//create new catalog size of manufectorer's catalog
	for (int i = 0; i < sz_f; i++)
	{
		ptr[i] = ptr_f[i];//writing date into salon's catalog from manufectorer's catalog
	}

}

//��� 
void director::synchronize_test_autos(manufacturerDB &aFactory, salonDB *aSalonDB)
{
	//������ ������� ���� ����-���� ������ � ����� ID
	int size_test_auto_old = 0;
	for (int i = 0; i < aFactory.get_size_autopark(); i++)
	{
		if (aFactory.autopark[i].ID[0] == aSalonDB->salon_data.get_ID())
		{
			size_test_auto_old++;
		}
	}
	//���� ���������� ���� ���� � ������������� (������ � ��� �� ������ �� ID) �� ����� ������ ���-�� ���� ���� ������
	//����� ������������ ������
	if (size_test_auto_old != aSalonDB->size_autopark)
	{
		//�������� ���� ������ ������ ����-���� ������, ��������� ���� �����
		int size_new_item = aFactory.size_autopark - size_test_auto_old + aSalonDB->size_autopark;
		if (size_new_item != 0)
		{
			//���� ����� ������ ��������� �� ����� 0
			test_auto *New_Item = new test_auto[size_new_item];
			int index = 0;
			//���������� �� ������� ������� ������������� ��� ����-���� �� ������ �������
			for (int i = 0; i < aFactory.size_autopark; i++)
			{
				if (aFactory.autopark[i].ID[0] != aSalonDB->salon_data.get_ID())
				{
					New_Item[index] = aFactory.autopark[i];
					index++;
				}
			}
			//���������� � ����� ������ ������ ����� ������ �� ����������
			for (int i = index, j = 0; i < size_new_item; i++, j++)
			{
				New_Item[i] = aSalonDB->autopark[j];
			}
			//������� ������ ������ �������������, �������������� �� �����
			aFactory.size_autopark = size_new_item;	//�������� ������ ������� ����-���� ������������� �� �����
			delete[] aFactory.autopark;
			aFactory.autopark = New_Item;
		}
		else
		{
			delete[] aFactory.autopark;
			aFactory.autopark = nullptr;
		}
	}
	// ���� ������ ������� ���� �� �������, �� ������� ��� ����-���� ������ ������(�� ID) � ������� �������������
	// �� ����� ����-���� ���� �� ������
	//������ �� ������������
	else
	{
		for (int i = 0, j = 0; i < aFactory.size_autopark; i++)
		{
			if (aFactory.autopark[i].ID[0] == aSalonDB->salon_data.get_ID())
			{
				aFactory.autopark[i] = aSalonDB->autopark[j];
				j++;
			}
		}
	}
}

//������
void director::synchronize_clients_and_inquiries(manufacturerDB &aFactory, salonDB *aSalonDB)
{
	for (int i = 0; i < aSalonDB->get_size_clients(); i++)
		switch (aSalonDB->clients_list[i].get_status())
		{
		case added:
		{
			aSalonDB->clients_list[i].set_client_status(synchronyzed);
			aFactory.add_client(aSalonDB->clients_list[i]);
			break;
		}
		case synchronyzed:
		{
			//if client's data is synchronized, there is nothing to change
			break;
		}
		case changed:
		{
			bool flag = false; //variable to indicate if client was found in Factory base
							   //searching for such a client in manufacturer's DB:
			for (int j = 0; j < aFactory.get_size_clients(); j++)
				if (aFactory.clients_list[j] == aSalonDB->clients_list[i])
				{
					flag = true; //client was found in Factory base
								 //changing client's data in manufacturer's DB:
					aFactory.clients_list[j].set_client_status(aSalonDB->clients_list[i].get_status());
					aFactory.clients_list[j].set_client_name(aSalonDB->clients_list[i].get_name());
					aFactory.clients_list[j].set_client_surname(aSalonDB->clients_list[i].get_surname());
					aFactory.clients_list[j].set_client_phone(aSalonDB->clients_list[i].get_phone());
					//changing client's status after his data synchronization:
					aSalonDB->clients_list[i].set_client_status(synchronyzed);
					//checking client's inquiries:
					for (int k = 0; k < aSalonDB->clients_list[i].get_list_sz(); k++)
					{
						//if this is a new inquiry, adding it to manufacturer's DB:
						if (aSalonDB->clients_list[i].inquirys_list[k].get_status() == inq_added)
						{
							aFactory.add_inquiry(j, aSalonDB->clients_list[i].inquirys_list[k]);	//add method to class manufacturerDB add_inquiry(int client_index, const inquiry aInquiry);
							aSalonDB->clients_list[i].inquirys_list[k].set_status(inq_synchronyzed);	//changing inquiry status in salon DB
							continue;
						}
						for (int l = 0; l < aFactory.clients_list[j].get_list_sz(); l++)
							if (aSalonDB->clients_list[i].inquirys_list[k].get_car_ID() == aFactory.clients_list[j].inquirys_list[l].get_car_ID())
							{
								switch (aSalonDB->clients_list[i].inquirys_list[k].get_status())
								{
								case inq_changed:
								{
									if (aFactory.clients_list[j].inquirys_list[l].get_status() == inq_accepted ||
										aFactory.clients_list[j].inquirys_list[l].get_status() == inq_changed ||
										aFactory.clients_list[j].inquirys_list[l].get_status() == inq_synchronyzed ||
										aFactory.clients_list[j].inquirys_list[l].get_status() == inq_rejected)
									{
										aFactory.clients_list[j].inquirys_list[l] = aSalonDB->clients_list[i].inquirys_list[k];
										aSalonDB->clients_list[i].inquirys_list[k].set_status(inq_synchronyzed);
									}
									else
									{
										//if this inquiry has already been accepted or moving, rejecting this inquiry 
										//and creating one more inquiry in salon DB - with data from manufacturer's DB
										aSalonDB->clients_list[i].inquirys_list[k].set_status(inq_rejected);
										aSalonDB->clients_list[i].add_inquiry(aFactory.clients_list[j].inquirys_list[l]);
									}
									break;
								}
								case inq_synchronyzed:
								{
									//synchronising status of inquiry in two bases:
									if (aFactory.clients_list[j].inquirys_list[l].get_status() != inq_synchronyzed)
										aSalonDB->clients_list[i].inquirys_list[k].set_status(aFactory.clients_list[j].inquirys_list[l].get_status());
									break;
								}
								case inq_accepted:
								{
									//synchronising status of inquiry in two bases:
									if (aFactory.clients_list[j].inquirys_list[l].get_status() != inq_accepted)
										aSalonDB->clients_list[i].inquirys_list[k].set_status(aFactory.clients_list[j].inquirys_list[l].get_status());
									break;
								}
								case inq_is_moving:
								{
									//synchronising status of inquiry in two bases:
									if (aFactory.clients_list[j].inquirys_list[l].get_status() != inq_is_moving)
										aSalonDB->clients_list[i].inquirys_list[k].set_status(aFactory.clients_list[j].inquirys_list[l].get_status());
									break;
								}
								case inq_rejected:
								{
									//synchronising status of inquiry in two bases:
									if (aFactory.clients_list[j].inquirys_list[l].get_status() != inq_rejected)
										aSalonDB->clients_list[i].inquirys_list[k].set_status(aFactory.clients_list[j].inquirys_list[l].get_status());
									break;
								}
								}
							}
					}
				}
			if (flag == false)	//if client was not found in Factory base
			{
				aSalonDB->clients_list[i].set_client_status(synchronyzed);
				aFactory.add_client(aSalonDB->clients_list[i]);
			}
		}
		}
}


//������ 
void director::synchronize_all(manufacturerDB &aFactory, salonDB *aSalonDB)
{
	synchronize_cars(aFactory, aSalonDB);
	synchronize_test_autos(aFactory, aSalonDB);
	synchronize_clients_and_inquiries(aFactory, aSalonDB);
}
