#include "inquiry.h"
using std::cout;
using std::endl;
//����������� �� ���������
inquiry::inquiry()
{
	for (int i = 0; i < 4; i++)
	{
		ID[i] = 0;
	}
	date_added.day = 0;
	date_added.month = 0;
	date_added.year = 0;
	status = inq_added;
	options_sz = 0;
	additional_options = nullptr;
}

//������������� ����������� - 1� �������
inquiry::inquiry
(	int aSalonID, 
	int aManagerID, 
	int aClientID, 
	int aCarID, 
	int aDay, 
	int aMonth, 
	int aYear)
{
	ID[iSalonID] = aSalonID; 
	ID[iManagerID] = aManagerID;
	ID[iClientID] = aClientID;
	ID[iCarID] = aCarID;
	date_added.day = aDay;
	date_added.month = aMonth;
	date_added.year = aYear;
	status = inq_added;
	options_sz = 0;
	additional_options = nullptr;
}

//������������� ����������� - 2� �������
inquiry::inquiry(
	int aSalonID,
	int aManagerID,
	int aClientID,
	int aCarID, 
	date aDate)
{
	ID[iSalonID] = aSalonID;
	ID[iManagerID] = aManagerID;
	ID[iClientID] = aClientID;
	ID[iCarID] = aCarID;
	date_added = aDate;
	status = inq_added;
	options_sz = 0;
	additional_options = nullptr;
}

//����������� �����������
inquiry::inquiry(const inquiry & aInquiry)
{
	for (int i = 0; i < 4; i++)
	{
		ID[i] = aInquiry.ID[i];
	}
	date_added = aInquiry.date_added;
	status = aInquiry.status;
	options_sz = aInquiry.options_sz;
	if (options_sz != 0)
	{
		additional_options = new string[options_sz];
		for (int i = 0; i < options_sz; i++)
		{
			additional_options[i] = aInquiry.additional_options[i];
		}
	}
	else
	{
		additional_options = nullptr;
	}
}

//����������
inquiry::~inquiry() 
{
	delete [] additional_options;
}

//������� ��������� � ������������� �������� ID ������
void inquiry::set_salon_ID(int aSalonID)
{
	ID[iSalonID] = aSalonID;
}

//������� ��������� � ������������� �������� ID �������
void inquiry::set_client_ID(int aClientID)
{
	ID[iClientID] = aClientID;
}

//������� ��������� � ������������� �������� ID ���������
void inquiry::set_manager_ID(int aManagerID)
{
	ID[iManagerID] = aManagerID;
}

//������� ��������� � ������������� �������� ID ������
void inquiry::set_car_ID(int aCarID) 
{
	ID[iCarID] = aCarID;
}

//������� ��������� � ������������� �������� ��� ����
void inquiry::set_data(int aDay, int aMonth, int aYear) 
{
	date_added.day = aDay;
	date_added.month = aMonth;
	date_added.year = aYear;
}

//������� ��������� � ������������� �������� ��� ����
void inquiry::set_data(date &aDate) 
{
	date_added = aDate;
}

//������� ��������� � ������������� �������� ������� �������
void inquiry::set_status(inquiry_status aStatus)
{
	status = aStatus;
}

//������� ��������� ������ � ������������� �������� �������������� �����
void inquiry::add_option(const string &aOption)
{
	options_sz++;
	if (options_sz == 1)		
	{
		additional_options = new string[options_sz];
		additional_options[0] = aOption;
	}
	else {
		string *buf = new string[options_sz];
		for (int i = 0; i < options_sz - 1; i++)
		{
			buf[i] = additional_options[i];
		}
		delete[]additional_options;
		additional_options = buf;
		additional_options[options_sz - 1] = aOption;
	}
}

//������������� �������� ������������
//��������� ������ �� ������ ������ inquiry
inquiry & inquiry::operator=(const inquiry & aInquiry)
{
	for (int i = 0; i < 4; i++)
	{
		ID[i] = aInquiry.ID[i];
	}
	date_added = aInquiry.date_added;
	status = aInquiry.status;
	if (options_sz != 0)
	{
		delete[] additional_options;
	}
	options_sz = aInquiry.options_sz;
	if (options_sz != 0)
	{
		additional_options = new string[options_sz];
		for (int i = 0; i < options_sz; i++)
		{
			additional_options[i] = aInquiry.additional_options[i];
		}
	}
	else
	{
		additional_options = nullptr;
	}
	return *this;
}

//������������� �������� ��������
//��������� ������ �� ������ ������ string - ������
//���������� ����� ��������� ������
inquiry inquiry::operator+(const string &aOption) const
{
	return inquiry(*this) += aOption;
}

//������������� �������� +=
//��������� ������ �� ������ ������ string - ������
//���������� ������� ��������� ������
inquiry& inquiry::operator+=(const string &aOption)
{
	add_option(aOption);
	return *this;
}

//������� ������� �������� �������������� �����
//��������� ������ �� ������ ������ string - ������
// ���������� exceptions
void inquiry::remove_option(const string &aOption) //contains exception
{
	int result = -1;
	for (int i = 0; i < options_sz; i++)
	{
		if (additional_options[i]==(aOption))
		{
			result = i;
			break;
		}
	}
	if (result != -1)
	{
		remove_option(result);
	}
	else
	{
		throw "BAD OPTION";
	}
}

//������� ������� �������� �������������� �����
//��������� ������ �������������� �����
// ���������� exceptions
void inquiry::remove_option(int option_number)	//contains exception
{
	if (option_number >= 0 && option_number < options_sz)
	{
		options_sz--;
		if (options_sz == 0)
		{
			delete[]additional_options;
			additional_options = nullptr;
		}
		else {
			string *buf = new string[options_sz];
			for (int i = 0; i < options_sz; i++)
			{
				if (i < option_number)
				{
					buf[i] = additional_options[i];
				}
				else
				{
					buf[i] = additional_options[i + 1];
				}
			}
			delete[]additional_options;
			additional_options = buf;
		}
	}
	else
	{
		throw "BAD OPTION NUMBER";
	}
}

//������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �� ������ ������ string - ������
// ���������� ����� ������ �����  inquiry
inquiry inquiry::operator-(const string &aOption) const
{
	return inquiry(*this) -= aOption;
}

//������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �� ������ ������ string - ������
// ���������� ������� ������ �����  inquiry
inquiry& inquiry::operator-=(const string &aOption)
{
	remove_option(aOption);
	return *this;
}

//������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �������������� �����
// ���������� ����� ������ �����  inquiry
inquiry inquiry::operator-(int option_number) const
{
	return inquiry(*this) -= option_number;
}

// ������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �������������� �����
// ���������� ������� ������ �����  inquiry
inquiry& inquiry::operator-=(int option_number)
{
	remove_option(option_number);
	return *this;
}

// ������� �������� true ���� ����� ������ � �������, �.�. ����� ID �����
// ������� ��������� ������ �� �������� ������ inquiry
bool inquiry::operator==(const inquiry & aInquiry) const
{
	return (ID[iCarID] == aInquiry.ID[iCarID]);
}

void inquiry::print_to_file(ofstream & F) const
{
	if (!F)
		throw "inquiry: file was not opened, can't write";
	int buf = ID[0];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing salon ID
	buf = ID[1];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing manager ID
	buf = ID[2];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing client ID
	buf = ID[3];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing car ID

	try
	{
		date_added.print_to_file(F);	//writing date of inquiry
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	buf = sizeof(inquiry_status);
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));
	inquiry_status tmp = status;
	F.write(reinterpret_cast<char*>(&tmp), buf);	//inquiry_status writing

	buf = options_sz;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing options array size

	//writing oadditional_options:
	for (int i = 0; i < options_sz; i++)
	{
		string tmp = additional_options[i];
		buf = tmp.size();
		F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
		F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//additional option
	}
}

void inquiry::read_from_file(ifstream & F)
{
	if (!F)
		throw "inquiry: file was not opened, can't read";
	F.read(reinterpret_cast<char*>(&ID[0]), sizeof(int));		//reading salon ID
	F.read(reinterpret_cast<char*>(&ID[1]), sizeof(int));		//reading manager ID
	F.read(reinterpret_cast<char*>(&ID[2]), sizeof(int));		//reading client ID
	F.read(reinterpret_cast<char*>(&ID[3]), sizeof(int));		//reading car ID

	try
	{
		date_added.read_from_file(F);							//reading date of inquiry
	}
	catch (char *msg)
	{
		cout << msg << "\n";
	}

	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));
	F.read(reinterpret_cast<char*>(&status), buf);				//inquiry_status reading

	F.read(reinterpret_cast<char*>(&options_sz), sizeof(int));	//reading options array size

	delete[]additional_options;	
	additional_options = new string[options_sz];
	//reading additional_options:
	for (int i = 0; i < options_sz; i++)
	{
		F.read(reinterpret_cast<char*>(&buf), sizeof(int));		//string size
		additional_options[i].resize(buf);
		F.read(reinterpret_cast<char*>(&additional_options[i][0]), buf);	//additional option
	}
}
