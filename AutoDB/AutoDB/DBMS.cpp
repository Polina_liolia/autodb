#include "DBMS.h"
using std::cout;
using std::endl;

//������
DBMS::DBMS() //default constructor
{
	salons_counter = 0;	
	car_catalog_counter = 0;
	size_salons = 0;
	all_salonsDB = nullptr;
}

//������
DBMS::DBMS(const DBMS &aDBMS) //copy constructor
{
	factory = aDBMS.factory;
	salons_counter = aDBMS.salons_counter;
	car_catalog_counter = aDBMS.car_catalog_counter;
	size_salons = aDBMS.size_salons;
	if (size_salons == 0)
	{
		all_salonsDB = nullptr;
	}
	else
	{
		all_salonsDB = new salonDB[size_salons];
		for (int i = 0; i < size_salons; i++)
		{
			all_salonsDB[i] = aDBMS.all_salonsDB[i];
		}
	}
}

//������
DBMS::~DBMS() //destructor
{
	delete[] all_salonsDB;
}

int  DBMS::get_size_salons()const
{
	return size_salons;
}

//������
void DBMS::set_factory(Car *aCatalog, int aSize_catalog)	//������� �� ������������� (��������� ������ manufacturerDB)
{
	manufacturerDB new_factory(aCatalog, aSize_catalog);
	factory = new_factory;
	car_catalog_counter = aSize_catalog;
}

manufacturerDB & DBMS::get_factory()
{
	return factory;
}

salonDB & DBMS::get_salon(int ID) //contains exception
{
	if (!size_salons)	//if salonsDB array is empty
		throw "No salons found"; //generaiting exception
	if (get_salon_index(ID) == -1)	//if salon with pointed ID wasn't found
		throw "Wrong salon ID"; //generaiting exception
	int salon_index = get_salon_index(ID);
	return all_salonsDB[salon_index];	
}

int & DBMS::get_salons_counter()
{
	return salons_counter;
}

int & DBMS::get_car_catalog_counter()
{
	return car_catalog_counter;
}

//���
void DBMS::add_car_to_catalog(string aModel)		//�������� ���������� � �������
{
	car_catalog_counter++;
	factory.add_car(car_catalog_counter, aModel);
}

//���
void DBMS::remove_car_from_catalog(int aID)		//������� �� �������� ���� � ��������� ID; contains exception
{
	if (factory.get_size_catalog() != 0)
	{
		if (factory.find_index_car_catolog(aID) != -1)
		{
			factory.remove_car_by_ID(aID);
		}
		else
		{
			throw "BAD CAR ID";
		}
	}
	else
	{
		throw "EMPTY ARRAY CAR CATALOG";
	}
}

//���
void DBMS::change_catalog_item(int aID, string aModel_new) //�������� ������������ ���������� � ��������; contains exception
{
	if (factory.get_size_catalog() != 0)
	{
		if (factory.find_index_car_catolog(aID) != -1)
		{
			factory.change_car(aID, aModel_new);
		}
		else
		{
			throw "BAD CAR ID";
		}
	}
	else
	{
		throw "EMPTY ARRAY CAR CATALOG";
	}
}

//salonID - �������� ��� ++salons_counter
//����� add_salon ������ ��� �� ����� �������� ����� ����� � ������� � �� ������������� (manufacturerDB factory) � ������� ������ ������ manufacturerDB
//������
void DBMS::add_salon(string salon_name, string salon_city, string director_surname, string director_name, string salon_address, string salon_phone)
{
	salonDB salon_new(factory.get_car_catalog(), factory.get_size_catalog(), ++salons_counter, salon_name, salon_city, director_surname, director_name, salon_address, salon_phone);
	size_salons++;
	if (size_salons == 1)
	{
		all_salonsDB = new salonDB[size_salons];
		all_salonsDB[0] = salon_new;
	}
	else
	{
		salonDB *tmp = new salonDB[size_salons];
		for (int i = 0; i < size_salons - 1; i++)
		{
			tmp[i] = all_salonsDB[i];
		}
		tmp[size_salons - 1] = salon_new;
		delete[] all_salonsDB;
		all_salonsDB = tmp;
	}
	salon salon_data(salons_counter, salon_name, salon_city, salon_address, salon_phone);
	factory.add_salon(salon_data);	//adding a new salon to the manufacturer's list 
}

int DBMS::get_salon_index(int salonID)
{
	int salon_index = -1;
	for (int i = 0; i < size_salons; i++)
		if (all_salonsDB[i].salon_data.get_ID() == salonID)
		{
			salon_index = i;
			break;
		}
	return salon_index;
}

salonDB * DBMS::get_all_salonsDB()
{
	return all_salonsDB;
}

//����� remove_salon ������ ��� �� ����� ������� ����� �� �������� � �� ������������� (manufacturerDB factory) � ������� ������ ������ manufacturerDB
//������
//contains exception
void DBMS::remove_salon(int salonID)
{
	//searching for salonDB with pointed ID
	if (!size_salons)
		throw "No salons found"; //generaiting exception
	if (get_salon_index (salonID) == -1)	//if salon with pointed ID wasn't found
		throw "Wrong salon ID"; //generaiting exception
	//if salon index was successfully defined:
	int salon_index = get_salon_index(salonID);
	size_salons--;
	salonDB *tmp = new salonDB[size_salons];
	for (int i = 0, j = 0; i < size_salons + 1; i++)
		if (i != salon_index)
		{
			tmp[j] = all_salonsDB[i];
			j++;
		}
	delete[] all_salonsDB;
	all_salonsDB = tmp;

	factory.remove_salon_by_ID(salonID);	//removing salon from the manufacturer's list 
}

//����
void DBMS::change_salon_name(int salonID, string aName) //contains exception
{
	//searching for salonDB with pointed ID
	if (!size_salons)
		throw "No salons found"; //generaiting exception
	if (get_salon_index(salonID) == -1)	//if salon with pointed ID wasn't found
		throw "Wrong salon ID"; //generaiting exception
	//if salon index was successfully defined:
	int salon_index = get_salon_index(salonID);
	all_salonsDB[salon_index].salon_data.set_name(aName);
	factory.change_salon_name(salonID, aName);
}

//����
void DBMS::change_salon_city(int salonID, string aCity) //contains exception
{
	//searching for salonDB with pointed ID
	if (!size_salons)
		throw "No salons found"; //generaiting exception
	if (get_salon_index(salonID) == -1)	//if salon with pointed ID wasn't found
		throw "Wrong salon ID"; //generaiting exception
	//if salon index was successfully defined:
	int salon_index = get_salon_index(salonID);
	all_salonsDB[salon_index].salon_data.set_city(aCity);
	factory.change_salon_city(salonID, aCity);
}

//����
void DBMS::change_salon_address(int salonID, string aAddress) //contains exception
{
	//searching for salonDB with pointed ID
	if (!size_salons)
		throw "No salons found"; //generaiting exception
	if (get_salon_index(salonID) == -1)	//if salon with pointed ID wasn't found
		throw "Wrong salon ID"; //generaiting exception
	//if salon index was successfully defined:
	int salon_index = get_salon_index(salonID);
	all_salonsDB[salon_index].salon_data.set_address(aAddress);
	factory.change_salon_address(salonID, aAddress);
}

//����
void DBMS::change_salon_phone(int salonID, string aPhone)//contains exception
{
	//searching for salonDB with pointed ID
	if (!size_salons)
		throw "No salons found"; //generaiting exception
	if (get_salon_index(salonID) == -1)	//if salon with pointed ID wasn't found
		throw "Wrong salon ID"; //generaiting exception
	//if salon index was successfully defined:
	int salon_index = get_salon_index(salonID);
	all_salonsDB[salon_index].salon_data.set_phone(aPhone);
	factory.change_salon_phone(salonID, aPhone);
}

//���������������� ������� ������������ ����������� (������ ���������� �� �� ������������� � �� ������) (�� ������� ��������� - �.�. ����� ����� ������ director)
// ����
void DBMS::synchronize_cars(int salonID)	//contains exception
{
	//searching for salonDB with pointed ID
	if (!size_salons)
		throw "No salons found"; //generaiting exception
	if (get_salon_index(salonID) == -1)	//if salon with pointed ID wasn't found
		throw "Wrong salon ID"; //generaiting exception
	//if salon index was successfully defined:
	int salon_index = get_salon_index(salonID);
	delete[] all_salonsDB[salon_index].catalog;//deleting the old catalog in salon
	all_salonsDB[salon_index].size_catalog = factory.size_catalog;
	all_salonsDB[salon_index].catalog = new Car[factory.size_catalog];//creating new dynamic array (catalog)
	for (int i = 0; i < factory.size_catalog; i++)
	{
		all_salonsDB[salon_index].catalog[i] = factory.catalog[i];//writing date into salon's catalog from manufectorer's catalog
	}
}

//���
//���������������� ������� ����-���� � ������ (������ ���������� �� �� ������ � �� �������������) (�� ������� ��������� - �.�. ����� ����� ������ director)
void DBMS::synchronize_test_autos(int salonID) //contains exception
{
	if (factory.get_size_salons() != 0)
	{
		int result = factory.find_index_salon(salonID);
		if (result != -1)
		{
			all_salonsDB[result].salon_personnel.direk.synchronize_test_autos(factory, &all_salonsDB[result]);
		}
		else
		{
			throw "BAD SALON ID";
		}
	}
	else
	{
		throw "EMPTY ARRAY OF SALONS";
	}
}

//���������������� ������� �������� � �� ������� (������ ���������� � ��� �������) (�� ������� ��������� - �.�. ����� ����� ������ director)
//������
void DBMS::synchronize_clients_and_inquiries(int salonID)	//contains exception
{
	//searching for salonDB with pointed ID and saving its index
	int salon_index = -1;
	for (int i = 0; i < size_salons; i++)
		if (all_salonsDB[i].salon_data.get_ID() == salonID)
		{
			salon_index = i;
			break;
		}
	if (salon_index == -1)	//if salon with pointed ID wasn't found
			throw "Wrong salon ID"; //generaiting exception
	//if salon index was successfully defined:	
	all_salonsDB[salon_index].salon_personnel.direk.synchronize_clients_and_inquiries(factory, &all_salonsDB[salon_index]);
}

//���������������� ��� ����� (��� ������ ������) - ������ ��������� ���������� ��� ������ (�� ������� ��������� - �.�. ����� ����� ������ director)
//������
void DBMS::synchronize_all(int salonID)
{
	synchronize_cars(salonID);
	synchronize_test_autos(salonID);
	synchronize_clients_and_inquiries(salonID);
}

DBMS DBMS::operator=(DBMS aDBMS) //operator = overload
{
	factory = aDBMS.factory;
	salons_counter = aDBMS.salons_counter;
	car_catalog_counter = aDBMS.car_catalog_counter;
	size_salons = aDBMS.size_salons;
	delete[] all_salonsDB;
	if (size_salons == 0)
		all_salonsDB = nullptr;
	else
	{
		all_salonsDB = new salonDB[size_salons];
		for (int i = 0; i < size_salons; i++)
			all_salonsDB[i] = aDBMS.all_salonsDB[i];
	}
	return *this;
}

/*
manufacturerDB factory;
int salons_counter;	//to save number of salons, added to salons array (to avoid salons' IDs repeating)
int car_catalog_counter; //to avoid repeating of cars' IDs
int size_salons;
salonDB *all_salonsDB;
*/
void DBMS::print_to_file(char * file_name)
{
	ofstream F(file_name, ios::binary);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "DBMS: Can't open file for writing";
	try
	{
		factory.print_to_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	int buf = salons_counter;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//salons_counter writing

	buf = car_catalog_counter;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//car_catalog_counter writing

	buf = size_salons;
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//size_salons writing

	for (int i = 0; i < size_salons; i++)
	{
		try
		{
			all_salonsDB[i].print_to_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
}

void DBMS::read_from_file(char * file_name)
{
	ifstream F(file_name, ios::binary);
	// checking if file was successfully opened, othervice generating exception:
	if (!F)
		throw "DBMS: Can't open file for reading";
	try
	{
		factory.read_from_file(F);
	}
	catch (char *msg)
	{
		std::cout << msg << "\n";
	}
	F.read(reinterpret_cast<char*>(&salons_counter), sizeof(int));	//salons_counter reading
	F.read(reinterpret_cast<char*>(&car_catalog_counter), sizeof(int));	//car_catalog_counter reading
	F.read(reinterpret_cast<char*>(&size_salons), sizeof(int));	//size_salons reading

	delete[]all_salonsDB;
	all_salonsDB = new salonDB[size_salons];
	for (int i = 0; i < size_salons; i++)
	{
		try
		{
			all_salonsDB[i].read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
	}
}
