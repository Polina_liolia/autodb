#include "infs.h"
using std::cout;
using std::cin;

infs::infs()
{
	ID[0] = 0;
	ID[1] = 0;
}

//method to output some message to console (answer not required)
void infs::show_message(string message)
{
	system("cls");
	system("color 9F");	//to set the console window color
	pos.X = 10;
	pos.Y = 3;
	SetConsoleCursorPosition(hStdOut, pos);
	cout << message << "\n";
	pos.X = 0;
	pos.Y = 1;
	SetConsoleCursorPosition(hStdOut, pos);
	system("pause");
	system("cls");
}

//universal method to show menu (consists from array elements); msg - some message before menu, not mandatory argument
int infs::show_menu(string * arr, int sz, string msg)
{
	system("cls");
	system("color 9F");	//to set the console window color
	SetConsoleCursorPosition(hStdOut, pos);
	SetConsoleTextAttribute(hStdOut, defaultColor);
	int activeItem = 0;	//index of menu item, that is currently in focus
	while (true)
	{
		if (msg != "")
		{
			pos.X = 10;	//coursor position (x-coordinate - columns)
			pos.Y = 4;	//coursor position (y-coordinate - rows)
			SetConsoleCursorPosition(hStdOut, pos);
			cout << msg << "\n";
		}
		
		for (int i = 0; i < sz; i++)
		{
			if (i == activeItem)
				SetConsoleTextAttribute(hStdOut, activeColor);	//settings for text in focus
			else
				SetConsoleTextAttribute(hStdOut, defaultColor);	//settings for other text
			pos.X = 10;	//coursor position (x-coordinate - columns)
			pos.Y = 5 + i;	//coursor position (y-coordinate - rows)
			SetConsoleCursorPosition(hStdOut, pos);
			std::cout << arr[i];
		}

		//getting a command
		int code = 0;
		code = _getch();

		//interpretating a command
		switch (code)
		{
		case 72:	//up 
			if (activeItem != 0)	//to aviod missing active item
				activeItem--;
			else
				activeItem = sz - 1;	//moving to the last item
			break;
		case 80:	//down 
			if (activeItem < sz - 1)	//to aviod missing active item 
				activeItem++;
			else
				activeItem = 0;	//moving to the forst item
			break;
		case 13: 	//Enter 
			return activeItem;
		case 27:	//Esc - exit
			return -1;
		}
	}
	return 0;
}


//method to ask user some question; returns answer (string)
string infs::show_question_get_str(string question)
{
	system("cls");
	system("color 9F");	//to set the console window color
	pos.X = 10;
	pos.Y = 3;
	SetConsoleCursorPosition(hStdOut, pos);
	cout << question << "\n";
	pos.X = 10;
	pos.Y++;
	SetConsoleCursorPosition(hStdOut, pos);
	string answer;
	cin.ignore(cin.rdbuf()->in_avail());
	getline(cin, answer);
	cin.clear();
	system("cls");
	return answer;
}

void infs::login(DBMS &aManufacturer)
{
	system("cls");
	system("color 9F");	//to set the console window color
	try
	{
		aManufacturer.read_from_file("DB.bin");
	}
	catch (char *msg)
	{
		show_message(msg);
	}
	int accaunt_type = login_scr1(aManufacturer);	//user has to choose accaunt type, then method returns it's index:
										//0 - client, 1 - manager, 2 - director, 3 - manufacturer, 4 - exit
	if (accaunt_type != 3 && accaunt_type != 4)	//if client/manarer/director accaunt was choosen
		ID[0] = login_scr2_other(aManufacturer);	//user chooses salon, method returns its index
	switch (accaunt_type)
	{
	case 0:  case 1: case 2: //client/manager/director menu
		ID[1] = login_scr3_other(aManufacturer, accaunt_type);	//checks ID and password, returns user's ID
		if (ID[1] == -1) 
			login(aManufacturer);						//if user rejected logining in
		else if (accaunt_type == 0)
			client_screen1(ID[0], ID[1], aManufacturer);	//login succeed; moving to client's menu
		else if (accaunt_type == 1)
			mgr_screen(ID[0], ID[1], aManufacturer);	//login succeed; moving to manager's menu
		else if (accaunt_type == 2)
			derector_screen1(ID[0], aManufacturer);		//login succeed; moving to director's menu
		break;
	case 3: //manufacturer menu choosen
	{
		int rezult = login_scr2_factory(aManufacturer);
		if (rezult == -1) 
			login(aManufacturer);					//if user rejected logining in
		else manufacturer_screen1(aManufacturer);	//login succeed; moving to manufacturer's menu
	}
	break;
	case -1: case 4:
	try
	{
		aManufacturer.print_to_file("DB.bin");
	}
	catch (char *msg)
	{
		show_message(msg);
	}
		system("cls");
		system("color 9F");
		pos.X = 30;
		pos.Y = 10;
		SetConsoleCursorPosition(hStdOut, pos);
		cout << "Good by!\n"; 
		return;
	}
}


int infs::login_scr1(DBMS &aManufacturer)
{
	const int scr1_size = 5;
	string screen1[scr1_size] = {"Client menu", "Manager menu", "Director menu", "Manufacturer menu", "Exit"};
	int activeItem = show_menu(screen1, scr1_size);
	return activeItem;
}

int infs::login_scr2_other(DBMS &aManufacturer)
{
	const int size_salons = aManufacturer.get_size_salons();	//getting total number of salons in DBMS
	if (size_salons == 0)
	{
		show_message("No salons exist");
		login(aManufacturer);
		return -1;
	}
	string *screen2 = new string[size_salons];		//array to make a menu
	try
	{
		for (int i = 0; i < size_salons; i++)	//filling in array with salons' names
			screen2[i] = aManufacturer.get_all_salonsDB()[i].get_salon_data().get_name();
	}
	catch (char *msg)
	{
		show_message(msg);
		login(aManufacturer);
		return -1;
	}
	int activeItem = show_menu(screen2, size_salons, "Choose your salon:");
	delete[] screen2;
	return activeItem;
}

int infs::login_scr2_factory(DBMS &aManufacturer)
{
	int password = 0;
	system("cls");
	do
	{
		password = show_question<int>("Input your password (to reject logining in input -1)");
		if (password == -1)
			return -1;
	} while (aManufacturer.get_factory().get_password() != password);
	return 1;
}

int infs::login_scr3_other(DBMS &aManufacturer, int accaunt_type)
{
	system("color 9F");	//to set the console window color
	int password = 0;
	if (accaunt_type == 0)	//client accaunt
	{
		if (aManufacturer.get_size_salons() == 0 || aManufacturer.get_all_salonsDB()[ID[0]].get_size_clients() == 0)
		{
			show_message("No clients exist in choosen salon");
			return -1;
		}
		do
		{
			ID[1] = show_question<int>("Input your ID (to exit input -1):");
			if (ID[1] == -1)
				return -1;
			else if (ID[1] <= 0 || !(aManufacturer.get_all_salonsDB()[ID[0]].check_client_ID(ID[1])))
			{
				show_message("Wrong ID. Try again.");
				login_scr3_other(aManufacturer, accaunt_type);
				break;
			}
			password = show_question<int>("Input your password (to exit input -1):");
			if (password == -1)
				return -1;
		} while (aManufacturer.get_all_salonsDB()[ID[0]].get_client(ID[1]).get_password() != password);
	}
	else if (accaunt_type == 1)	//manager
	{
		if (aManufacturer.get_size_salons() == 0 || aManufacturer.get_all_salonsDB()[ID[0]].get_managers_amount() == 0)
		{
			show_message("No managers exist in choosen salon");
			return -1;
		}
		do
		{
			ID[1] = show_question<int>("Input your ID (to exit input -1):");
			if (ID[1] == -1)
				return -1;
			else if (ID[1] <= 1 || !(aManufacturer.get_all_salonsDB()[ID[0]].check_manager(ID[1])))
			{
				show_message("Wrong ID. Try again.");
				login_scr3_other(aManufacturer, accaunt_type);
				break;
			}
			password = show_question<int>("Input your password (to exit input -1):");
			if (password == -1)
				return -1;
		} while (aManufacturer.get_all_salonsDB()[ID[0]].get_personnel().get_manager_by_ID(ID[1]).get_password() != password);
	}
	else if (accaunt_type == 2)	//director
	{
		if (aManufacturer.get_size_salons() == 0)
		{
			show_message("No salons exist");
			return -1;
		}
		do
		{
			password = show_question<int>("Input your password (to exit input -1):");
			if (password == -1)
				return -1;
		} while (aManufacturer.get_all_salonsDB()[ID[0]].get_personnel().get_director().get_password() != password);
		return 1;
	}
	return ID[1];
}

void infs::mgr_screen(int index_salon, int IDmanager, DBMS &aManufacturer)
{
	const int scr1_size = 9;
	string screen1[scr1_size] = {"Show car catalog", "Test autos", "Add test-auto", "Add client", "Select client", "Show all inquiries", "Change password", "Change accaunt", "Exit" };
	int activeItem = show_menu(screen1, scr1_size);
	switch (activeItem)
	{
	case 0: //Show car catalog
		print_arr<Car, int>(aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog(), aManufacturer.get_all_salonsDB()[index_salon].get_size_catalog());
		mgr_screen(index_salon, IDmanager, aManufacturer);
		break;
	case 1: //Test autos
	{
		int test_auto_index = print_arr<test_auto, int>(aManufacturer.get_all_salonsDB()[index_salon].get_autopark(),
		aManufacturer.get_all_salonsDB()[index_salon].get_size_autopark(),
		"Input number of test-auto if you want to remove it (or 0 to turn back):") - 1;
		if (test_auto_index >= 0 && test_auto_index < aManufacturer.get_all_salonsDB()[index_salon].get_size_autopark())
		{
			int test_auto_ID = aManufacturer.get_all_salonsDB()[index_salon].get_autopark()[test_auto_index].get_carID();
			try
			{
				aManufacturer.get_all_salonsDB()[index_salon].remove_test_auto(test_auto_ID);
				show_message("Choosen test auto has been removed.");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
		}
		mgr_screen(index_salon, IDmanager, aManufacturer); 
	}
		break;
	case 2://Add test-auto
	{
		//creating a menu of cars from catalog:
		int cars_menu_size = aManufacturer.get_all_salonsDB()[index_salon].get_size_catalog();
		string *cars_menu = new string[cars_menu_size + 1];
		for (int i = 0; i < cars_menu_size; i++)
			cars_menu[i] = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[i].get_model();
		cars_menu[cars_menu_size] = "Back";
		int car_new_index = show_menu(cars_menu, cars_menu_size+1, "Select a model of the new test-auto:");
		delete[] cars_menu;
		if (car_new_index == cars_menu_size) //Back
		{
			mgr_screen(index_salon, IDmanager, aManufacturer);
			break;
		}
		int car_new_ID = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[car_new_index].get_ID();
		try
		{
			aManufacturer.get_all_salonsDB()[index_salon].add_test_auto(car_new_ID);
			show_message("Selected car has been added to salon's autopark");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		mgr_screen(index_salon, IDmanager, aManufacturer);
	}
		break;
	case 3: //Add client
	{
		string name = show_question_get_str("Name:");
		string surname = show_question_get_str("Surname:");
		string phone = show_question_get_str("Phone:");
		aManufacturer.get_all_salonsDB()[index_salon].add_client(surname, name, phone, IDmanager);
		mgr_screen(index_salon, IDmanager, aManufacturer);
	}
		break;
	case 4: //Select client
		select_client_screen(index_salon, IDmanager, aManufacturer);
	break;
	case 5: //Show all inquiries
	{
		int salon_ID = aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().get_ID();
		int inq_counter = 0;
		//counting total amount of inquiries in salon:
		for (int i = 0; i < aManufacturer.get_all_salonsDB()[index_salon].get_size_clients(); i++)
			inq_counter += aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[i].get_list_sz();
		//creating and filling in the array of all salon's inquiries:
		inquiry *All = new inquiry[inq_counter];
		for (int i = 0, k = 0; i < aManufacturer.get_all_salonsDB()[index_salon].get_size_clients(); i++)
			for (int j = 0; j < aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[i].get_list_sz(); j++, k++)
				All[k] = aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[i].get_inquiry_list()[j];
		print_arr<inquiry, int>(All, inq_counter);
		delete[] All;
		mgr_screen(index_salon, IDmanager, aManufacturer);
	}
		break;
	case 6: //Change password
	{
		int password[2];
		do
		{
			password[0] = show_question<int>("Enter new password (to exit input -1):");
			password[1] = show_question<int>("Repete new password (to exit input -1):");
		} while (password[1] != password[0]);
		aManufacturer.get_all_salonsDB()[index_salon].get_personnel().get_manager_by_ID(IDmanager).set_password(password[0]);
		show_message("Your password has been succesfully changed.");
		mgr_screen(index_salon, IDmanager, aManufacturer);
	}
	break;
	case 7: // Change accaunt
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		login(aManufacturer);
		break;
	case 8: //Exit
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		system("color 9F");
		system("cls");
		pos.X = 30;
		pos.Y = 10;
		SetConsoleCursorPosition(hStdOut, pos);
		cout << "Good by!\n";
		return;
	}
}

void infs::select_client_screen(int index_salon, int IDmanager, DBMS &aManufacturer)
{
	//creating strings array to show clients list in menu:
	int screen1_3_size = aManufacturer.get_all_salonsDB()[index_salon].get_size_clients();
	string *screen1_3 = new string[screen1_3_size];
	for (int i = 0; i < screen1_3_size; i++)	//filling array with clients' surnames and names
		screen1_3[i] = aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[i].get_surname() + static_cast<string>(" ") +
		aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[i].get_name();
	int client_index = show_menu(screen1_3, screen1_3_size, "Choose client:");
	delete[] screen1_3;
	client_actions(index_salon, IDmanager, client_index, aManufacturer);
}

void infs::client_actions(int index_salon, int IDmanager, int client_index, DBMS &aManufacturer)
{
	//creating menu of actions with choosen client:
	const int scr1_4_size = 6;
	string screen1_4[scr1_4_size] = { "Information","Change client's data", "Remove client", "Inquiries", "Back" };
	int action = show_menu(screen1_4, scr1_4_size);
	switch (action) //different types of actions with choosen client
	{
	case 0:	//Information
		print<client>(aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index]);
		client_actions(index_salon, IDmanager, client_index, aManufacturer);
		break;
	case 1: //Change client's data
		change_clients_data_screen(index_salon, IDmanager, client_index, aManufacturer);
		break;
	case 2:	//Remove client
	{
		bool remove = show_question<bool>("Are you shure you want to remove selected client? (0 - no, othervise - yes");
		if (remove)	//if client has to be removed
		{
			int client_ID = aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_client_ID();
			try
			{
				aManufacturer.get_all_salonsDB()[index_salon].remove_client(client_ID, IDmanager);
				show_message("Client has been removed.");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
			mgr_screen(index_salon, IDmanager, aManufacturer);
		}
		else //if client has not to be removed, turning back to manager's menu of actions with clients
			client_actions(index_salon, IDmanager, client_index, aManufacturer);
	}
	break;
	case 3:	//Inquiries				
		inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		break;
	case 4:	//Back
		mgr_screen(index_salon, IDmanager, aManufacturer);
		break;
	}
}

void infs::change_clients_data_screen(int index_salon, int IDmanager, int client_index, DBMS &aManufacturer)
{
	//creating menu of options to change:
	const int change_client_menu_size = 6;
	string change_client_menu[change_client_menu_size] = { "Change name", "Change surname",	"Change phone", "Change status","Back", "Exit" };
	int option = show_menu(change_client_menu, change_client_menu_size, "Choose option to change:");
	switch (option)
	{
	case 0:	//Change name
	{
		string name_new = show_question_get_str("Input new name");
		aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_name(name_new);
		show_message("Client's name was successfully changed");
		change_clients_data_screen(index_salon, IDmanager, client_index, aManufacturer);
	}
		break;
	case 1:	//Change surname
	{
		string surname_new = show_question_get_str("Input new surname");
		aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_surname(surname_new);
		show_message("Client's surname was successfully changed");
		change_clients_data_screen(index_salon, IDmanager, client_index, aManufacturer);
	}
		break;
	case 2:	//Change phone
	{
		string phone_new = show_question_get_str("Input new phone");
		aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_name(phone_new);
		show_message("Client's phone was successfully changed");
		change_clients_data_screen(index_salon, IDmanager, client_index, aManufacturer);
	}
		break;
	case 3:	//Change status
		//creating menu of status variants
	{
		const int status_menu_size = 4;
		string status_menu[status_menu_size] = { "added", "changed", "synchronyzed", "Back to previous menu" };
		int status_choosen = show_menu(status_menu, status_menu_size, "Choose new status for client:");
		switch (status_choosen)
		{
		case 0:	//added
			try
			{
				aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_status(added);
				show_message("Client's status has been successfully changed");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
			break;
		case 1:	//changed
			try
			{
				aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_status(changed);
				show_message("Client's status has been successfully changed");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
			break;
		case 2:	//synchronyzed
			try
			{
				aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_status(synchronyzed);
				show_message("Client's status has been successfully changed");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
			break;
		case 3:	//Back to previous menu
			break;
		}
		change_clients_data_screen(index_salon, IDmanager, client_index, aManufacturer);
	}
		break;
	case 4:	//Back
		client_actions(index_salon, IDmanager, client_index, aManufacturer);
		break;
	}
}

void infs::inquiries_screen(int index_salon, int IDmanager, int client_index, DBMS &aManufacturer)
{
	int client_ID = aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_client_ID();
	const int inq_menu1_sz = 4;
	string inq_menu1[inq_menu1_sz] = { "Add inquiry", "Remove inquiry", "Change inquiry", "Back"};
	int action1 = show_menu(inq_menu1, inq_menu1_sz);
	switch (action1)
	{
	case 0: //Add inquiry
	{
		//void add_inquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, int aDay, int aMonth, int aYear, inquiry_status aStatus, const string &aOption = nullptr);
		int salonID = aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().get_ID();
		int clientID = aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_client_ID();
		int cars_menu_size = aManufacturer.get_all_salonsDB()[index_salon].get_size_catalog();
		string *cars_menu = new string[cars_menu_size];
		for (int i = 0; i < cars_menu_size; i++)
			cars_menu[i] = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[i].get_model();
		int car_index = show_menu(cars_menu, cars_menu_size, "Select the model of a car:");
		delete[] cars_menu;
		int carID = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[car_index].get_ID();
		int day = show_question<int>("Input date (day)");
		int month = show_question<int>("Input date (month)");
		int year = show_question<int>("Input date (year)");
		//adding an inquiry:
		try
		{
			aManufacturer.get_all_salonsDB()[index_salon].add_inquiry(clientID, carID, IDmanager, day, month, year);
			show_message("New inquiry was succesfully created.");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
	}
	break;
	case 1: //Remove inquiry
	{
		//outputting all client's inquiries:				
		int inquiry_index = print_arr<inquiry, int>(aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_inquiry_list(),
			aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_list_sz(), "Choose number of inquiry:") - 1;
		if (inquiry_index < 0 || inquiry_index >= aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_list_sz())
		{
			show_message("A wrong inquiry number!");
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		}
		bool remove = show_question<bool>("Are you shure you want to remove selected inquiry? (0 - no, other - yes");
		if (remove)	//if inquiry has to be removed
		{
			int car_ID = aManufacturer.get_all_salonsDB()[index_salon].get_client(client_ID).get_inquiry_list()[inquiry_index].get_car_ID();
			try
			{
				aManufacturer.get_all_salonsDB()[index_salon].remove_inquiry(client_ID, car_ID, IDmanager);
				show_message("Inquiry has been removed.");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		}
		else //if inquiry has not to be removed, turning back to inquiry's menu
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
	}
		break;
	case 2: //Change inquiry
	{
		//outputting all client's inquiries:				
		int inquiry_index = print_arr<inquiry, int>(aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_inquiry_list(),
			aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_list_sz(), "Choose number of inquiry:") - 1;
		if (inquiry_index < 0 || inquiry_index >= aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_list_sz())
		{
			show_message("A wrong inquiry number!");
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		}
		int car_ID = aManufacturer.get_all_salonsDB()[index_salon].get_client(client_ID).get_inquiry_list()[inquiry_index].get_car_ID();
		//creating menu of options to change:
		const int change_options_size = 4;
		string change_options[change_options_size] = { "Change car ID", "Add option", "Remove option", "Back" };
		int option = show_menu(change_options, change_options_size);
		switch (option)
		{
		case 0:	//Change car ID
		{
			//creating a menu of cars from catalog:
			int cars_menu_size = aManufacturer.get_all_salonsDB()[index_salon].get_size_catalog();
			string *cars_menu = new string[cars_menu_size];
			for (int i = 0; i < cars_menu_size; i++)
				cars_menu[i] = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[i].get_model();
			int car_new_index = show_menu(cars_menu, cars_menu_size, "Select a new model of car:");
			int car_new_ID = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[car_new_index].get_ID();
			delete[] cars_menu;
			int car_ID_new = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[car_new_index].get_ID();
			try
			{
				aManufacturer.get_all_salonsDB()[index_salon].change_inquiry(client_ID, car_ID, car_ID_new, IDmanager);
				show_message("ID has been changed.");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		}
			break;
		case 1:	//Add option
		{
			string option_new = show_question_get_str("Input option to add to order");
			aManufacturer.get_all_salonsDB()[index_salon].get_client(client_ID).get_inquiry_list()[inquiry_index].add_option(option_new);
			show_message("Option has been added.");
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		}
			break;
		case 2:	//Remove option
		{
			string option_old = show_question_get_str("Input option to delete");
			aManufacturer.get_all_salonsDB()[index_salon].get_client(client_ID).get_inquiry_list()[inquiry_index].remove_option(option_old);
			show_message("Option has been removed.");
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		}
			break;
		case 3:	//Back
			inquiries_screen(index_salon, IDmanager, client_index, aManufacturer);
		break;
		}
	}
		break;
	case 3: //Back
		mgr_screen(index_salon, IDmanager, aManufacturer);
		break;
	}
}


void infs::derector_screen1(int index_salon, DBMS &aManufacturer)
{
	//creating basic director's menu
	const int dir_menu_size = 16;
	string dir_menu[dir_menu_size] = { "Show salon data", "Change salon data", "Show salon personnel", "Hire manager", "Fire manager",
		"Change manager's data", "Show all clients", "Show autopark", "Show car catalog", "Synchronize car catalog", "Synchronize autopark",
		"Synchronise clients and inquiries", "Synchronize all data", "Change password", "Change accaunt", "Exit" };
	int choice = show_menu(dir_menu, dir_menu_size);
	switch (choice)
	{
	case 0: //Show salon data
	{
		print<salon>(aManufacturer.get_all_salonsDB()[index_salon].get_salon_data());
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 1://Change salon data
	{
		change_salon_data_screen(index_salon, aManufacturer);
	}
	break;
	case 2://Show salon personnel
	{
		print<stuff>(aManufacturer.get_all_salonsDB()[index_salon].get_personnel());
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 3://Hire manager
	{
		string name = show_question_get_str("Input new manager's name");
		string surname = show_question_get_str("Input new manager's surname");
		string phone = show_question_get_str("Input new manager's phone");
		short day = show_question<short>("Input new manager's birth day");
		short month = show_question<short>("Input new manager's birth month (number)");
		short year = show_question<short>("Input new manager's birth year");
		aManufacturer.get_all_salonsDB()[index_salon].add_manager(surname, name, day, month, year, phone);
		show_message("Manager has been succesfully hired");
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 4://Fire manager
	{
		//creating array of managers to choose from
		int mgrs_size = aManufacturer.get_all_salonsDB()[index_salon].get_managers_amount();
		if (mgrs_size == 0)
		{
			show_message("No managers found");
			derector_screen1(index_salon, aManufacturer);
		}
		string *mgrs = new string[mgrs_size];
		for (int i = 0; i < mgrs_size; i++)
			mgrs[i] = aManufacturer.get_all_salonsDB()[index_salon].get_managers()[i].get_name() + static_cast<string>(" ") + 
			aManufacturer.get_all_salonsDB()[index_salon].get_managers()[i].get_surname();
		int choosen_mgr_index = show_menu(mgrs, mgrs_size, "Choose manager to fire");
		bool accept = show_question<bool>("Are you shure you want to fire this manager?(0 - no, other - yes)");
		if (accept)	//if choosen manager has to be fired
		{
			int choosen_mgr_ID = aManufacturer.get_all_salonsDB()[index_salon].get_managers()[choosen_mgr_index].get_employee_ID();
			aManufacturer.get_all_salonsDB()[index_salon].fire_manager(choosen_mgr_ID);
			show_message("Manager has been succesfully fired");
			derector_screen1(index_salon, aManufacturer);
		}
		else	//if choosen manager has not to be fired, turning back to director's menu
			derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 5://Change manager's data
	{
		//creating array of managers to choose from:
		int mgrs_size = aManufacturer.get_all_salonsDB()[index_salon].get_managers_amount();
		if (mgrs_size == 0)
		{
			show_message("No managers found");
			derector_screen1(index_salon, aManufacturer);
		}
		string *mgrs = new string[mgrs_size];
		for (int i = 0; i < mgrs_size; i++)
			mgrs[i] = aManufacturer.get_all_salonsDB()[index_salon].get_managers()[i].get_name() + static_cast<string>(" ") +
			aManufacturer.get_all_salonsDB()[index_salon].get_managers()[i].get_surname();
		int choosen_mgr_index = show_menu(mgrs, mgrs_size, "Choose manager to fire");
		change_managers_data_screen(index_salon, choosen_mgr_index, aManufacturer);
	}
	break;
	case 6://Show all clients
	{
		print_arr<client, int>(aManufacturer.get_all_salonsDB()[index_salon].get_clients_list(),
			aManufacturer.get_all_salonsDB()[index_salon].get_size_clients());
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 7://Show autopark
	{
		print_arr<test_auto, int>(aManufacturer.get_all_salonsDB()[index_salon].get_autopark(),
			aManufacturer.get_all_salonsDB()[index_salon].get_size_autopark());
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 8://Show car catalog
	{
		print_arr<Car, int>(aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog(),
			aManufacturer.get_all_salonsDB()[index_salon].get_size_catalog());
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 9://Synchronize car catalog
	{
		aManufacturer.synchronize_cars(aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().get_ID());
		show_message("Car catalog has been synchronized with manufacturer's base");
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 10://Synchronize autopark
	{
		aManufacturer.synchronize_test_autos(aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().get_ID());
		show_message("Salon's autopark has been synchronized with manufacturer's base");
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 11://Synchronise clients and inquiries
	{
		aManufacturer.synchronize_clients_and_inquiries(aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().get_ID());
		show_message("Cients and inquiries has been synchronized with manufacturer's base");
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 12://Synchronize all data
	{
		aManufacturer.synchronize_all(aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().get_ID());
		show_message("All salon's data has been synchronized with manufacturer's base");
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 13://Change password
	{
		int password[2];
		do
		{
			password[0] = show_question<int>("Enter new password (to exit input -1):");
			password[1] = show_question<int>("Repete new password (to exit input -1):");
		} while (password[1] != password[0]);
		aManufacturer.get_all_salonsDB()[index_salon].get_personnel().get_director().set_password(password[0]);
		show_message("Your password has been succesfully changed.");
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 14://Change accaunt
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		login(aManufacturer);
	break;
	case 15://Exit
	{
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		system("color 9F");
		system("cls");
		pos.X = 30;
		pos.Y = 10;
		SetConsoleCursorPosition(hStdOut, pos);
		cout << "Good by!\n";
		return;
	}
	}
}

void infs::change_salon_data_screen(int index_salon, DBMS &aManufacturer)
{
	//creating menu of actions (properties to change):
	const int menu_size = 6;
	string menu[menu_size] = { "Change salon name", "Change salon city", "Change salon address", "Change salon phone",  
		"Back to director's menu", "Exit" };
	int action = show_menu(menu, menu_size);
	switch (action)
	{
	case 0://Change salon name
	{
		string salon_name = show_question_get_str("Input a new salon's name");
		aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().set_name(salon_name);
		show_message("Salon has been renamed");
		change_salon_data_screen(index_salon, aManufacturer);
	}
	break;
	case 1://Change salon city
	{
		string salon_city = show_question_get_str("Input a new salon's city");
		aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().set_city(salon_city);
		show_message("Salon's city has been changed");
		change_salon_data_screen(index_salon, aManufacturer);
	}
	break;
	case 2://Change salon address
	{
		string salon_address = show_question_get_str("Input a new salon's address");
		aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().set_address(salon_address);
		show_message("Salon's address has been changed");
		change_salon_data_screen(index_salon, aManufacturer);
	}
	break;
	case 3://Change salon phone
	{
		string salon_phone = show_question_get_str("Input a new salon's phone");
		aManufacturer.get_all_salonsDB()[index_salon].get_salon_data().set_phone(salon_phone);
		show_message("Salon's phone has been changed");
		change_salon_data_screen(index_salon, aManufacturer);
	}
	break;
	case 4://Back to director's menu
	{
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 5://Exit
	{
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		system("color 9F");
		system("cls");
		pos.X = 30;
		pos.Y = 10;
		SetConsoleCursorPosition(hStdOut, pos);
		cout << "Good by!\n";
		return;
	}
	}
}

void infs::change_managers_data_screen(int index_salon, int manager_index, DBMS &aManufacturer)
{
	//creating menu of actions (properties to change):
	const int menu_size = 6;
	string menu[menu_size] = { "Change name", "Change surname",	"Change phone", "Change birth date", "Back to director's menu", "Exit" };
	int action = show_menu(menu, menu_size);
	switch (action)
	{
	case 0://Change name
	{
		string name = show_question_get_str("Input a new manager's name");
		aManufacturer.get_all_salonsDB()[index_salon].get_managers()[manager_index].set_name(name);
		show_message("Manager's name has been changed");
		change_managers_data_screen(index_salon, manager_index, aManufacturer);
	}
	break;
	case 1://Change surname
	{
		string surname = show_question_get_str("Input a new manager's surname");
		aManufacturer.get_all_salonsDB()[index_salon].get_managers()[manager_index].set_surname(surname);
		show_message("Manager's surname has been changed");
		change_managers_data_screen(index_salon, manager_index, aManufacturer);
	}
	break;
	case 2://Change phone
	{
		string phone = show_question_get_str("Input a new manager's phone");
		aManufacturer.get_all_salonsDB()[index_salon].get_managers()[manager_index].set_phone(phone);
		show_message("Manager's phone has been changed");
		change_managers_data_screen(index_salon, manager_index, aManufacturer);
	}
	break;
	case 3://Change birth date
	{
		short day = show_question<short>("Input a new manager's birth day");
		short month = show_question<short>("Input a new manager's birth month (number)");
		short year = show_question<short>("Input a new manager's birth year");
		aManufacturer.get_all_salonsDB()[index_salon].get_managers()[manager_index].set_birth_date(day, month, year);
		show_message("Manager's birth date has been changed");
		change_managers_data_screen(index_salon, manager_index, aManufacturer);
	}
	break;
	case 4://Back to director's menu
	{
		derector_screen1(index_salon, aManufacturer);
	}
	break;
	case 5://Exit
	{
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		system("color 9F");
		system("cls");
		pos.X = 30;
		pos.Y = 10;
		SetConsoleCursorPosition(hStdOut, pos);
		cout << "Good by!\n";
		return;
	}
	}
}

void infs::client_screen1(int index_salon, int clientID, DBMS &aManufacturer)
{
	int client_index = aManufacturer.get_all_salonsDB()[index_salon].get_client_index(clientID);
	//creating basic client menu
	const int menu_size = 10;
	string menu[menu_size] = { "Show personal data", "Change personal data", "Show all inquiries", "Change inquiry", "Show managers' contacts", 
		"Show salon data", "Show car catalog", "Change password", "Change accaunt", "Exit" };
	int action = show_menu(menu, menu_size);
	switch (action)
	{
	case 0://Show personal data
	{
		print<client>(aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index]);
		client_screen1(index_salon, clientID, aManufacturer);
	}
	break;
	case 1://Change personal data
	{
		//creating menu of options to change:
		const int change_client_menu_size = 5;
		string change_client_menu[change_client_menu_size] = { "Change name", "Change surname",	"Change phone", "Back", "Exit" };
		int option = show_menu(change_client_menu, change_client_menu_size, "Choose option to change:");
		switch (option)
		{
		case 0:	//Change name
		{
			string name_new = show_question_get_str("Input new name");
			aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_name(name_new);
			show_message("Client's name was successfully changed");
			client_screen1(index_salon, clientID, aManufacturer);
		}
		break;
		case 1:	//Change surname
		{
			string surname_new = show_question_get_str("Input new surname");
			aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_surname(surname_new);
			show_message("Client's surname was successfully changed");
			client_screen1(index_salon, clientID, aManufacturer);
		}
		break;
		case 2:	//Change phone
		{
			string phone_new = show_question_get_str("Input new phone");
			aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_client_name(phone_new);
			show_message("Client's phone was successfully changed");
			client_screen1(index_salon, clientID, aManufacturer);
		}
		break;
		case 3:	//Back
			client_screen1(index_salon, clientID, aManufacturer);
			break;
		case 4: //Exit
			try
			{
				aManufacturer.print_to_file("DB.bin");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
			system("color 9F");
			system("cls");
			pos.X = 30;
			pos.Y = 10;
			SetConsoleCursorPosition(hStdOut, pos);
			cout << "Good by!\n";
			return;
		}
	}
	break;
	case 2://Show all inquiries
	{
		//outputting all client's inquiries:				
		print_arr<inquiry, int>(aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_inquiry_list(),
			aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_list_sz());
		client_screen1(index_salon, clientID, aManufacturer);
	}
	break;
	case 3://Change inquiry
	{
		//outputting all client's inquiries:				
		int inquiry_index = print_arr<inquiry, int>(aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_inquiry_list(),
			aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_list_sz(), "Choose number of inquiry:") - 1;
		if (inquiry_index < 0 || inquiry_index >= aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].get_list_sz())
		{
			show_message("A wrong inquiry number!");
			client_screen1(index_salon, clientID, aManufacturer);
		}
		//creating menu of options to change:
		const int change_options_size = 4;
		string change_options[change_options_size] = { "Change car ID", "Add option", "Remove option", "Back to previous menu" };
		int option = show_menu(change_options, change_options_size);
		switch (option)
		{
		case 0:	//Change car ID
		{
			//creating a menu of cars from catalog:
			int cars_menu_size = aManufacturer.get_all_salonsDB()[index_salon].get_size_catalog();
			string *cars_menu = new string[cars_menu_size];
			for (int i = 0; i < cars_menu_size; i++)
				cars_menu[i] = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[i].get_model();
			int car_new_index = show_menu(cars_menu, cars_menu_size, "Select a new model of car:");
			delete[] cars_menu;
			int ID_new = aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog()[car_new_index].get_ID();
			aManufacturer.get_all_salonsDB()[index_salon].get_client(clientID).get_inquiry_list()[inquiry_index].set_car_ID(ID_new);
			show_message("ID has been changed.");
			client_screen1(index_salon, clientID, aManufacturer);
		}
		break;
		case 1:	//Add option
		{
			string option_new = show_question_get_str("Input option to add to order");
			aManufacturer.get_all_salonsDB()[index_salon].get_client(clientID).get_inquiry_list()[inquiry_index].add_option(option_new);
			show_message("Option has been added.");
			client_screen1(index_salon, clientID, aManufacturer);
		}
		break;
		case 2:	//Remove option
		{
			string option_old = show_question_get_str("Input option to delete");
			aManufacturer.get_all_salonsDB()[index_salon].get_client(clientID).get_inquiry_list()[inquiry_index].remove_option(option_old);
			show_message("Option has been removed.");
			client_screen1(index_salon, clientID, aManufacturer);
		}
		break;
		case 3:	//Back to previous menu
		{
			client_screen1(index_salon, clientID, aManufacturer);
		}
		break;
		}
	}
	break;
	case 4://Show managers' contacts
	{
		print_arr<manager, int>(aManufacturer.get_all_salonsDB()[index_salon].get_managers(), 
			aManufacturer.get_all_salonsDB()[index_salon].get_managers_amount(), "Fell free to ask our managers all questions about your inquiries");
		client_screen1(index_salon, clientID, aManufacturer);
	}
	break;
	case 5: //Show salon data
	{
		print<salon>(aManufacturer.get_all_salonsDB()[index_salon].get_salon_data());
		client_screen1(index_salon, clientID, aManufacturer);
	}
	break;
	case 6://Show car catalog
	{
		print_arr<Car, int>(aManufacturer.get_all_salonsDB()[index_salon].get_car_catalog(),
			aManufacturer.get_all_salonsDB()[index_salon].get_size_catalog());
		client_screen1(index_salon, clientID, aManufacturer);
	}
	break;
	case 7://Change password
	{
		int password[2];
		do
		{
			password[0] = show_question<int>("Enter new password (to exit input -1):");
			password[1] = show_question<int>("Repete new password (to exit input -1):");
		} while (password[1] != password[0]);
		aManufacturer.get_all_salonsDB()[index_salon].get_clients_list()[client_index].set_password(password[1]);
		show_message("Your password has been succesfully changed.");
		client_screen1(index_salon, clientID, aManufacturer);
	}
	break;
	case 8://Change accaunt
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		login(aManufacturer);
	break;
	case 9://Exit
	{
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		system("color 9F");
		system("cls");
		pos.X = 30;
		pos.Y = 10;
		SetConsoleCursorPosition(hStdOut, pos);
		cout << "Good by!\n";
		return;
	}
	}
}

void infs::manufacturer_screen1(DBMS &aManufacturer)
{
	const int menu_size = 14;
	string menu[menu_size] = { "Show car catalog", "Change car in catalog",	"Add new car to catalog", "Remove car from catalog", "Show all test autos",
		"Show all salons", "Add salon", "Set salon's director", "Remove salon", "Show all clients", "Inquiries",
		"Change password", "Change accaunt", "Exit" };
	int action = show_menu(menu, menu_size);
	switch (action)
	{
	case 0:	//Show car catalog
		print_arr<Car, int>(aManufacturer.get_factory().get_car_catalog(),
			aManufacturer.get_factory().get_size_catalog());
		manufacturer_screen1(aManufacturer);
		break;
	case 1:	//Change car in catalog
	{
		int car_number = print_arr<Car, int>(aManufacturer.get_factory().get_car_catalog(),
			aManufacturer.get_factory().get_size_catalog(), "Input the number of car to change");
		//checking the correctness of input:
		if (car_number <= 0 || car_number > aManufacturer.get_factory().get_size_catalog())
			show_message("A wrong car number");
		else
		{
			string model_new = show_question_get_str("Input a new model of choosen car");
			int car_ID = aManufacturer.get_factory().get_car_catalog()[car_number - 1].get_ID();
			aManufacturer.get_factory().change_car(car_ID, model_new);
			show_message("Choosen car data has been successfully changed");
		}
		manufacturer_screen1(aManufacturer);
	}
	break;
	case 2:	//Add new car to catalog
	{
		string model_new = show_question_get_str("Input a model of new car");
		aManufacturer.add_car_to_catalog(model_new);
		show_message("A new car has been successfully added to catalog");
		manufacturer_screen1(aManufacturer);
	}
	break;
	case 3:	//Remove car from catalog
	{
		int car_number = print_arr<Car, int>(aManufacturer.get_factory().get_car_catalog(),
			aManufacturer.get_factory().get_size_catalog(), "Input the number of car to remove");
		//checking the correctness of input:
		if (car_number <= 0 || car_number > aManufacturer.get_factory().get_size_catalog())
			show_message("A wrong car number");
		else
		{
			int car_ID = aManufacturer.get_factory().get_car_catalog()[car_number - 1].get_ID();
			aManufacturer.remove_car_from_catalog(car_ID);
			show_message("Choosen car has been successfully removed");
		}
		manufacturer_screen1(aManufacturer);
	}
	break;
	case 4:	//Show all test autos
		print_arr<test_auto, int>(aManufacturer.get_factory().get_autopark(), aManufacturer.get_factory().get_size_autopark());
		manufacturer_screen1(aManufacturer);
		break;
	case 5:	//Show all salons
		print_arr<salon, int>(aManufacturer.get_factory().get_all_salons(), aManufacturer.get_factory().get_size_salons());
		manufacturer_screen1(aManufacturer);
		break;
	case 6:	//Add salon
	{
		string salon_name = show_question_get_str("Input the name of a new salon");
		string salon_city = show_question_get_str("Input the city of a new salon");
		string director_surname = show_question_get_str("Input a surname of a new salon's director");
		string director_name = show_question_get_str("Input a name of a new salon's director");
		string salon_address = show_question_get_str("Input an address of a new salon");
		string salon_phone = show_question_get_str("Input a phone number of a new salon");
		aManufacturer.add_salon(salon_name, salon_city, director_surname, director_name, salon_address, salon_phone);
		show_message("A new salon has been successfully added to data base");
		manufacturer_screen1(aManufacturer);
	}
		break;
	case 7:	//Set salon's director
	{
		const int size_salons = aManufacturer.get_size_salons();	//getting total number of salons in DBMS
		if (size_salons == 0)
		{
			show_message("No salons exist");
			manufacturer_screen1(aManufacturer);
			break;
		}
		string *screen2 = new string[size_salons + 1];		//array to make a menu
		for (int i = 0; i < size_salons; i++)	//filling in array with salons' names
			screen2[i] = aManufacturer.get_factory().get_all_salons()[i].get_name();
		screen2[size_salons] = "Back";
		int salon_index = show_menu(screen2, size_salons+1, "Choose salon to set a director:");
		delete[] screen2;
		if (salon_index == size_salons) //Back
		{
			manufacturer_screen1(aManufacturer);
			break;
		}
		string director_surname = show_question_get_str("Input a surname of a new director");
		string director_name = show_question_get_str("Input a name of a new director");
		aManufacturer.get_all_salonsDB()[salon_index].set_director(director_surname, director_name);
		show_message("A new director has been set to choosen salon");
		manufacturer_screen1(aManufacturer);
	}
		break;
	case 8:	//Remove salon
	{
		const int size_salons = aManufacturer.get_size_salons();	//getting total number of salons in DBMS
		if (size_salons == 0)
		{
			show_message("No salons exist");
			manufacturer_screen1(aManufacturer);
			break;
		}
		string *screen2 = new string[size_salons + 1];		//array to make a menu
		for (int i = 0; i < size_salons; i++)	//filling in array with salons' names
			screen2[i] = aManufacturer.get_factory().get_all_salons()[i].get_name();
		screen2[size_salons] = "Back";
		int salon_index = show_menu(screen2, size_salons+1, "Choose salon to remove:");
		delete[] screen2;
		if (salon_index == size_salons) //Back
		{
			manufacturer_screen1(aManufacturer);
			break;
		}
		int salonID = aManufacturer.get_factory().get_all_salons()[salon_index].get_ID();
		aManufacturer.remove_salon(salonID);
		show_message("Salon has been removed");
		manufacturer_screen1(aManufacturer);
	}
		break;
	case 9:	//Clients
	{
		int cl_index = print_arr<client, int>(aManufacturer.get_factory().get_clients_list(), aManufacturer.get_factory().get_size_clients(),
			"To delete client input it's number (or 0 to turn back):") - 1;
		if (cl_index >= 0 || cl_index < aManufacturer.get_factory().get_size_clients())
		{
			int cl_ID = aManufacturer.get_factory().get_clients_list()[cl_index].get_client_ID();
			try
			{
				aManufacturer.get_factory().remove_client(cl_ID);
				show_message("Client has been removed");
			}
			catch (char *msg)
			{
				show_message(msg);
			}
		}
	}
		manufacturer_screen1(aManufacturer);
		break;
	case 10:	//Inquiries
	{
		int inq_counter = 0;
		//counting total amount of inquiries in factory catalog:
		for (int i = 0; i < aManufacturer.get_factory().get_size_clients(); i++)
			inq_counter += aManufacturer.get_factory().get_clients_list()[i].get_list_sz();
		//creating and filling in the array of all inquiries:
		inquiry *All = new inquiry[inq_counter];
		for (int i = 0, k = 0; i < aManufacturer.get_factory().get_size_clients(); i++)
			for (int j = 0; j < aManufacturer.get_factory().get_clients_list()[i].get_list_sz(); j++, k++)
				All[k] = aManufacturer.get_factory().get_clients_list()[i].get_inquiry_list()[j];
		int action = print_arr<inquiry, int>(All, inq_counter, "To choose inquiry input it's number (or 0 to turn back):") - 1;
		if (action >= 0 && action < inq_counter)
		{
			int clientID = All[action].get_client_ID();
			int client_index = aManufacturer.get_factory().get_client_index(clientID);
			int carID = All[action].get_car_ID();
			int inq_index = aManufacturer.get_factory().get_clients_inquiry_index(clientID, carID);
			delete[] All;
			aManufacturer.get_factory().get_clients_list()[client_index].get_inquiry_list()[inq_index];
			const int sz1 = 3;
			string ar1[sz1] = { "Change status", "Delete", "Back to menu" };
			int choise = show_menu(ar1, sz1, "Choose action with an inquiry:");
			switch (choise)
			{
			case 0: //Change status
			{
				const int sz2 = 5;
				string ar2[sz2] = { "inq_accepted", "inq_is_moving", "inq_rejected", "done", "Back to menu" };
				int status_index = show_menu(ar2, sz2, "Choose a new status of inquiry:");
				if (status_index == sz2 - 1) //"Back to menu"  
					break;
				inquiry_status status;
				switch (status_index)
				{
				case 0: status = inq_accepted; break;
				case 1: status = inq_is_moving; break;
				case 2: status = inq_rejected; break;
				case 3: status = done; break;
				}
				//changing status:
				aManufacturer.get_factory().change_inquiry_status(aManufacturer.get_factory().get_clients_list()[client_index].get_inquiry_list()[inq_index], status);
				show_message("Inquiry status has been changed.");
			}
				break;
			case 1: //Delete
			{
				bool remove = show_question<bool>("Are you shure you want to remove selected inquiry? (0 - no, othervise - yes");
				if (remove)	//if inquiry has to be removed
				{
					try
					{
						aManufacturer.get_factory().remove_inquiry(clientID, carID);
						show_message("Inquiry has been removed.");
					}
					catch (char *msg)
					{
						show_message(msg);
					}
				}
				else //if inquiry has not to be removed, turning back
					break;
			}
			break;
			case 2: //back
				break;
			}
		}
		manufacturer_screen1(aManufacturer);
	}
		break;
	case 11:	//Change password
	{
		int password[2];
		do
		{
			password[0] = show_question<int>("Enter new password (to exit input -1):");
			password[1] = show_question<int>("Repete new password (to exit input -1):");
		} while (password[1] != password[0]);
		aManufacturer.get_factory().set_password(password[1]);
		show_message("Your password has been succesfully changed.");
		manufacturer_screen1(aManufacturer);
	}
		break;
	case 12:	//Change accaunt
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		login(aManufacturer);
		break;
	case 13:	//Exit
		try
		{
			aManufacturer.print_to_file("DB.bin");
		}
		catch (char *msg)
		{
			show_message(msg);
		}
		system("color 9F");
		system("cls");
		pos.X = 30;
		pos.Y = 10;
		SetConsoleCursorPosition(hStdOut, pos);
		cout << "Good by!\n";
		return;
	}
}
