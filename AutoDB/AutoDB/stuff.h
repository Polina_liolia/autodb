#pragma once
#include "director.h"
#include "manager.h"

/*�����, ����������� �������� ��������� ������. ��������� ��� �������, ��� ���������� ����� ���� �����, �� �������� ����*/
class stuff
{
private:
	director direk; //��������� ������ director 
	int size_mgrs; //����������� ���������� 
	int counter;
	manager	*mgrs; //������ ����������� ������ manager (��� ���������, ���������� � ������) 

public:
	stuff(); //default constructor 
	stuff(director aDir); //overloaded constructor
	stuff(string director_surname, string director_name, short director_bDay = 0, short director_bMonth = 0, short director_bYear = 0, string director_phone = "");
	stuff(const stuff &aStuff); //copy constructor 
	~stuff(); //destructor 

public:
	/*set ��������� ���������� ����� ��� �������� ��� ������������ ��������*/
	void set_director(director aDir);
	void set_director(int aSalonID, string aSurname, string aName, short aDay = 0, short aMonth = 0, short aYear = 0, string aPhone = "");
	void add_manager(manager aMgr);
	void add_manager(int aSalonID, string aSurname, string aName, short aDay = 0, short aMonth = 0, short aYear = 0, string aPhone = nullptr);
	void fire_manager(int aEmployeeID); //���������� ��������� (�������� ��� �� �������) �� ��� ID 
	stuff operator=(stuff aStuff);
	void fire_director(char aChint, int aEmployeeID = 1);
	director get_director()const;
	int get_size_mgrs()const;
	manager *get_managers()const;
	manager get_manager(int index)const;
	manager get_manager_by_ID(int ID)const;
	friend class DBMS;
	friend ostream& operator << (ostream& os, stuff &aStuff);

	void print_to_file(ofstream &F)const;
	void read_from_file(ifstream &F);
};

