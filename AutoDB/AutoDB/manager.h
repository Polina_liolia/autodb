#pragma once
#include "employee.h"
#include "client.h"

class manager : public employee
{
public:
	manager();	//default constructor ������������� type = mgr;
	manager(int aSalonID, int aemployeeID, string aSurname, string aName, short aDay = 0, short aMonth = 0, short aYear = 0, string aPhone = "", position aType = mgr, int aPassword = 1111);	//overloaded constructor ������������� type = dir;
	manager(const manager &amanager);	//copy constructor ������������� ������������� type = mgr;

public:
	void add_client(string aSurname, string aName, salonDB aSalon, string aPhone ="");
	void edit_clients_surname(int clientID, string aSurname, salonDB aSalon);
	void edit_clients_name(int clientID, string aName, salonDB aSalon);
	void edit_clients_phone(int clientID, string aPhone, salonDB aSalon);
	void edit_clients_status(int clientID, client_status aSt, salonDB aSalon);
	void remove_client(int clientID, salonDB aSalon); //�������� ������� �� ��� ID
	void change_clients_status(int clientID, client_status aStatus, salonDB aSalon);	//�������� ������ �������
	void add_inquiry(int clientID, int carID, salonDB aSalon, short aDay, short aMonth, short aYear);	//�������� ������� ������ 
	void remove_inquiry(int clientID, int carID, salonDB aSalon);	//������� ������ �� ��������� ����������
	void change_inquiry(int clientID, int carID_old, int carID_new, salonDB aSalon);	//�������� ���������� � �������
	void add_inquiry_option(int clientID, int carID, string aOption, salonDB aSalon);	//�������� ����� � ������ �������
	void remove_inquiry_option(int clientID, int carID, string aOption, salonDB aSalon);	//������� ����� �� ������� �������
};