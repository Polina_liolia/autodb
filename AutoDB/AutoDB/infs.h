#pragma once
#include <iostream>
#include <windows.h>
#include <conio.h>
#include "DBMS.h"
using namespace std;
class infs  
{	
private:
	int ID[2];
	//properties to manage console output:
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	int activeColor = FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN;
	int defaultColor = BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_RED;
	COORD pos;
public:
	infs();
	~infs() {};
	void show_message(string message);
	int show_menu(string *arr, int sz, string msg="");

	template<typename T>
	T show_question(string question);
	string show_question_get_str(string question);

	template<typename K>
	void print(K object);

	template<typename L, typename M>
	M print_arr(L *object, int size, string msg="");

	void login(DBMS &aManufacturer);
	int login_scr1(DBMS &aManufacturer);
	int login_scr2_factory(DBMS &aManufacturer);
	int login_scr2_other(DBMS &aManufacturer);
	int login_scr3_other(DBMS &aManufacturer, int accaunt_type);
	void mgr_screen(int INsalon, int INmamager, DBMS &aManufacturer);
	void select_client_screen(int index_salon, int IDmanager, DBMS &aManufacturer);
	void client_actions(int index_salon, int IDmanager, int client_index, DBMS &aManufacturer);
	void change_clients_data_screen(int index_salon, int IDmanager, int client_index, DBMS &aManufacturer);
	void inquiries_screen(int index_salon, int IDmanager, int client_index, DBMS &aManufacturer);
	void derector_screen1(int INsalon, DBMS &aManufacturer);
	void change_salon_data_screen(int INsalon, DBMS &aManufacturer);
	void change_managers_data_screen(int index_salon, int manager_index, DBMS &aManufacturer);
	void client_screen1(int INsalon, int INclient, DBMS &aManufacturer);
	void manufacturer_screen1(DBMS &aManufacturer);
};

// template method to ask user some question; returns answer (T-type)
template<typename T>
inline T infs::show_question(string question)
{
	system("cls");
	system("color 9F");	//to set the console window color
	pos.X = 10;
	pos.Y = 3;
	SetConsoleCursorPosition(hStdOut, pos);
	cout << question << "\n";
	pos.X = 10;
	pos.Y++;
	SetConsoleCursorPosition(hStdOut, pos);
	T answer;
	cin >> answer;
	cin.clear();
	system("cls");
	return answer;
}

//universal method to print some object; K - type of an object
template<typename K>
inline void infs::print(K object)
{
	system("cls");
	system("color 9F");	//to set the console window color
	pos.X = 0;
	pos.Y = 3;
	SetConsoleCursorPosition(hStdOut, pos);
	cout << object << "\n";
	pos.X = 0;
	pos.Y = 1;
	SetConsoleCursorPosition(hStdOut, pos);
	system("pause");
	system("cls");
}

//universal method to print some objects array; L - type of an object
template<typename L, typename M>
inline M infs::print_arr(L * object, int size, string msg)
{
	system("cls");
	system("color 9F");	//to set the console window color
	pos.X = 0;
	pos.Y = 4;
	SetConsoleCursorPosition(hStdOut, pos);
	for (int i = 0; i < size; i++, pos.Y++)
		cout << i+1 << ". " << object[i] << "\n";
	M choice;
	pos.X = 0;
	pos.Y = 0;
	SetConsoleCursorPosition(hStdOut, pos);
	if (msg == "")
		cout << "Press any key and Enter to return.\n";
	else
		cout << msg << "\n";
	pos.X = 0;	
	pos.Y = 1;
	SetConsoleCursorPosition(hStdOut, pos);
	cin >> choice;	
	cin.clear();	
	system("pause");
	return choice;
}
