#include "test_auto.h"

test_auto::test_auto()	//default constructor
{
	ID[0] = 0;
	ID[1] = 0;
}

test_auto::test_auto(int salonID, const Car &aCar) /*overloaded constructor (takes data from instance of class Car from the catalog, to aviod 
											 creating test-cars not from catalog:*/
{
	ID[0] = salonID;
	ID[1] = aCar.get_ID();
	model = aCar.get_model();
}

test_auto::test_auto(const test_auto &aTest_auto)	//copy constructor
{
	ID[0] = aTest_auto.ID[0];
	ID[1] = aTest_auto.ID[1];
	model = aTest_auto.model;
}

test_auto::~test_auto() {}							//destructor
													
/*
to set or change salonID
takes meaning of salon ID
returns nothing
*/
void test_auto::set_salonID(int salonID)
{
	ID[0] = salonID;
}

/*
sets or changes car ID and model
takes an instance of class Car
returns nothing
*/
void test_auto::set_test_auto(const Car &aCar)
{
	ID[1] = aCar.get_ID();
	model = aCar.get_model();
}

/*
takes nothing
returns salon ID (first part of test-auto ID)
*/
int test_auto::get_salonID()const
{
	return ID[0];
}

/*
takes nothing
returns car ID (second part of test-auto ID)
*/
int test_auto::get_carID()const
{
	return ID[1];
}

/*
takes nothing
returns car model
*/
string test_auto::get_car_model()const
{
	return model;
}

/*overloaded operator =
takes an instance of class test_auto
*/
test_auto test_auto::operator=(const test_auto &aTest_auto)
{
	ID[0] = aTest_auto.ID[0];
	ID[1] = aTest_auto.ID[1];
	model = aTest_auto.model;
	return *this;
}

/*
int ID [2];		//ID ������ � ID ���� (�� ��������)
string model;
*/
void test_auto::print_to_file(ofstream & F) const
{
	if (!F)
		throw "Car: file was not opened, can't write";
	int buf = ID[0];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing salon ID
	buf = ID[1];
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//writing car ID
	string tmp = model;
	buf = tmp.size();
	F.write(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	F.write(reinterpret_cast<char*>(&tmp[0]), buf);			//model
}

void test_auto::read_from_file(ifstream & F)
{
	if (!F)
		throw "Car: file was not opened, can't read";
	F.read(reinterpret_cast<char*>(&ID[0]), sizeof(int));	//reading salon ID
	F.read(reinterpret_cast<char*>(&ID[1]), sizeof(int));	//reading car ID
	int buf;
	F.read(reinterpret_cast<char*>(&buf), sizeof(int));	//string size
	model.resize(buf);
	F.read(reinterpret_cast<char*>(&model[0]), buf);	//model
}



